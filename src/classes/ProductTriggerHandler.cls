public class ProductTriggerHandler {
	
	public static Id skuValueAddedRecordTypeId = Product2Service.recordTypeByName(Constants.RT_PRODUCT2_VALUE_ADDED);
	public static Id skuYarnRecordTypeId = Product2Service.recordTypeByName(Constants.RT_PRODUCT2_YARN);
	public static Id skuTwistedYarnRecordTypeId = Product2Service.recordTypeByName(Constants.RT_PRODUCT2_TWISTED_YARN);
	public static Id skuFabricRecordTypeId = Product2Service.recordTypeByName(Constants.RT_PRODUCT2_FABRIC);
    	
	public static void onAfterInsert(List<Product2> newList)
	{
		List<Product2> valueAddedSkus = new List<Product2>();
		List<Product2> yarnSkus = new List<Product2>();

		for(Product2 sku : newList)
		{
			if(sku.RecordTypeId == skuValueAddedRecordTypeId)
			{
				valueAddedSkus.add(sku);
			}
			if(sku.RecordTypeId == skuYarnRecordTypeId || sku.RecordTypeId == skuTwistedYarnRecordTypeId)
			{
				yarnSkus.add(sku);
			}
		}

		if(valueAddedSkus.size() > 0)
		{
			//create composition for value added skus
			CompositionService.createValueAddComposition(valueAddedSkus);
		}
		if(yarnSkus.size() > 0)
		{
			//Creation of Yarn Dyeing and Twisting Process
			createYarnSkuProcesses(yarnSkus);
			//create composition for value added skus
			CompositionService.createYarnComposition(yarnSkus);
		}

	}

    public static void onBeforeUpdate (List<Product2> newList, Map<Id, Product2> oldMap) {
                    System.debug('Before update Trigger');

        List<Product2> newListCopy = new List<Product2> ();
        Map<Id, Product2> oldMapCopy = new Map<Id, Product2> ();
        
        for (Product2 productObj : newList) {
            if (productObj.RecordTypeId == skuYarnRecordTypeId) {
                newListCopy.add (productObj);
                oldMapCopy.put(productObj.Id, oldMap.get (productObj.Id));
            }   
        }
        
        if (newListCopy.size() > 0 && oldMapCopy.size() > 0 ) {
            updateProductData (newListCopy, oldMapCopy);
        }
        
    }
    
	public static void onBeforeInsert(List<Product2> newList)
	{
      
        List<Product2> yarnSkus = new List<Product2>();
        List<Product2> fabricSkus = new List<Product2>();
        
        /*
         * Decleration for List to hold the product based on Pattern Type;
         * */
        List<Product2> skuNewList = new List<Product2> ();
        List<Product2> skuExistingList = new List<Product2> ();
        /*
         * Decleration Ends
         * */
        
       	for(Product2 p:newList)
        {
            
        	p.IsActive = true;
          if(p.RecordTypeId == skuYarnRecordTypeId || p.RecordTypeId == skuTwistedYarnRecordTypeId)
        	     yarnSkus.add(p);

            if(p.RecordTypeId == skuFabricRecordTypeId) {
        	     fabricSkus.add(p);
                
             	/*
                 * Check for the pattern type and add it to subsequent list
                 * */
                if (p.Pattern_Type__c != null && p.Pattern_Type__c == Constants.PRODUCT_PATTERN_TYPE_NEW) {
                    skuNewList.add (p);
                }
                
                if (p.Pattern_Type__c != null && p.Pattern_Type__c == Constants.PRODUCT_PATTERN_TYPE_EXISTING) {
                    skuExistingList.add (p);
                }
                /*
                 * Product adding to list based on pattern type ends here.
                 * */   
            }
		    }

       if(yarnSkus.size()>0)
          creatsKuYanName(yarnSkus);

        if(fabricSkus.size()>0)
          creatsKuFabricName(fabricSkus);
        
        
        /*
         * Update the Costing master
         * */
        if (skuNewList.size() > 0) {
            updateCostingMaster (skuNewList);
        }
        /*
         * End of Updating costing master
         * */
        
        
        /*
         * Check the costingMaster__c for the particular pattern 
         * if the pattern exists then save the record
         * otherwise throw an error to user saying cannot create a record
         * */
        if (skuExistingList.size() > 0) {
            checkForPattern (skuExistingList);
        }
        /*
         * End
         * */
  
	}   
    
	public static void onAfterUpdate(List<Product2> newList, Map<Id, Product2> oldMap)
	{
		List<Product2> skuList = new List<Product2>();
		Set<Id> skuIds = new Set<Id>();
		
		for(Product2 sku : newList)
		{
			if(sku.RecordTypeId == skuValueAddedRecordTypeId || sku.RecordTypeId == skuFabricRecordTypeId)
			{
				Product2 oldValue = oldMap.get(sku.Id);

				if(sku.Standard_FOB_Cost__c != oldValue.Standard_FOB_Cost__c || sku.Standard_CIF_Cost__c != oldValue.Standard_CIF_Cost__c 
					|| sku.Custom_FOB_Cost__c != oldValue.Custom_FOB_Cost__c || sku.Custom_CIF_Cost__c != oldValue.Custom_CIF_Cost__c 
					|| sku.Express_FOB_Cost__c != oldValue.Express_FOB_Cost__c || sku.Express_CIF_Cost__c != oldValue.Express_CIF_Cost__c)
				{
					skuList.add(sku);
					skuIds.add(sku.Id);
				}
			}
		}
		
		if(skuList.size() > 0)
		{
			System.debug('Creating Price Book Entry');
			createPriceBookEntry(skuList, skuIds);
		}
	}

     /*
      * Method to check for the given pattern
      * */
      public static void checkForPattern (List<Product2> checkList) {
         Set<Id> costingMasterId = new Set<Id>();
         
      	 for (Product2 productObj : checkList ) {
             if (String.isBlank(productObj.Pattern__c)) {
                 productObj.addError ('Enter the value for the pattern');
                 return;
             }
             if (productObj.Weave_Type__c != null) {
             	costingMasterId.add (productObj.Weave_Type__c);
             }
         }
         
          if (costingMasterId.size() > 0) { 
              Map<Id, CostingMaster__c> costingDetail = CostingMasterService.getCostingMasterDetails (costingMasterId);
              if (costingDetail != null) {
                  for (Product2 p : checkList) {
                      if (p.Weave_Type__c != null) {
                        CostingMaster__c costingMaster = costingDetail.get(p.Weave_Type__c);
                      	String costingPattern = costingMaster.Code__c + costingMaster.Last_series_number__c;
                          if (costingPattern.equalsIgnoreCase(p.Pattern__c)) {
                              p.Pattern__c = p.Pattern__c.touppercase ();
                          } else {
                              p.addError ('Existing pattern ' +  p.Pattern__c.touppercase () +' not found try with different pattern');
                          }
                      }
                  }
              }
          }
          
      }
    
    /*
     * Method to update the costingMaster__c's LastSeriesNumber and pattern field of SKU
     * */
    public static void updateCostingMaster (List<Product2> updateList) {

       	Set<Id> costingMasterId = new Set<Id>();
        
        for (Product2 p : updateList) {
            if (p.Weave_Type__c != null) {
                costingMasterId.add (p.Weave_Type__c);
            }
        }
        
        List<CostingMaster__c> costingListToUpdate = new List<CostingMaster__c> ();
        		
        if (costingMasterId.size() > 0) {
            
            Map<Id, CostingMaster__c> costingDetail = CostingMasterService.getCostingMasterDetails (costingMasterId);
            
            if (costingDetail != null) {
                for (Product2 p : updateList) {
                    if (p.Weave_Type__c != null) {
                        CostingMaster__c costingMaster = costingDetail.get(p.Weave_Type__c);
                        if (costingMaster.Last_series_number__c != null) {
                            p.Pattern__c = costingMaster.Code__c + (++ costingMaster.Last_series_number__c);
                        } else {
                            p.Pattern__c = costingMaster.Code__c + 1;
                            costingMaster.Last_series_number__c = 1;
                        }
                        costingListToUpdate.add (costingMaster);  
                    }
                }                
            }
        }
        update costingListToUpdate;
    }
    
     public static void updateProductData (List<Product2> newList, Map<Id, Product2> oldMap) {
        
        Set<Id> qmId = new Set<Id>();
        Set<Id> dmId = new Set<Id>(); 
        Map<id,Quality_Master__c> qmMap = new Map<Id,Quality_Master__c>();
		Map<Id,Denier_Master__c> dmMap = new Map<Id,Denier_Master__c>();
        
             
        for (Product2 p : newList) {
            dmId.add (p.Denier__c);
            qmId.add (p.Quality_Name__c);
        }
         
         for (Product2 p : oldMap.values ()) {
            dmId.add (p.Denier__c);
            qmId.add (p.Quality_Name__c);
         }
         
		/*
		 * Retrieve Quality Master and Denier Master
		 * */
        qmMap = QualityMasterService.qualityMasterMap(qmId);
		dmMap = DenierMasterService.denierMasterMap(dmId);
        
         
         for (Product2 p : newList) {
            
             if(p.RecordTypeId == skuYarnRecordTypeId) {
                                 
                if (p.Quality_Name__c != oldMap.get(p.Id).Quality_Name__c) {
                   p.ProductCode = qmMap.get(p.Quality_Name__c).Quality_Code__c;
                   p.Name = qmMap.get(p.Quality_Name__c).Quality_Code__c;
                } else {
                   p.ProductCode = qmMap.get(oldMap.get(p.Id).Quality_Name__c).Quality_Code__c;
                   p.Name =  qmMap.get(oldMap.get(p.Id).Quality_Name__c).Quality_Code__c;
                }
                 
                if (p.Denier__c != oldMap.get(p.Id).Denier__c) {
                 	p.ProductCode += '-' + dmMap.get(p.Denier__c).Name;
                    p.Name += '-' + dmMap.get(p.Denier__c).Name;
                } else {
                    p.ProductCode += '-' + dmMap.get(oldMap.get(p.Id).Denier__c).Name;
                    p.Name += '-' + dmMap.get(oldMap.get(p.Id).Denier__c).Name;
                }

                if (p.Ply__c != oldMap.get(p.Id).Ply__c) {
                    p.ProductCode += '-' +  p.Ply__c;
                    p.Name += '-' + p.Ply__c;
                } else {
                    p.ProductCode += '-' +  oldMap.get(p.Id).Ply__c;
                    p.Name += '-' + oldMap.get(p.Id).Ply__c;
                }
                 
                if (p.Twist_Type__c != oldMap.get(p.Id).Twist_Type__c) {
                     
                    if (p.Twist_Type__c != null) {
                         p.ProductCode += '-' + (p.Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : p.Twist_Type__c); 
                         p.Name += '-' + (p.Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : p.Twist_Type__c); 
                    }    
                } else {
                    if (oldMap.get(p.Id).Twist_Type__c != null) {
                         p.ProductCode += '-' + (oldMap.get(p.Id).Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : oldMap.get(p.Id).Twist_Type__c); 
                         p.Name += '-' + (oldMap.get(p.Id).Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : oldMap.get(p.Id).Twist_Type__c); 
                    }
                }
                 
                if (p.TPM__c != oldMap.get(p.Id).TPM__c) {
                     if (p.TPM__c != null) {
                         p.ProductCode += '-' + p.TPM__c;
                         p.Name += '-' + p.TPM__c;
                     }
                 } else {
                     if (oldMap.get(p.Id).TPM__c != null) {
                         p.ProductCode += '-' + oldMap.get(p.Id).TPM__c;
                         p.Name += '-' + oldMap.get(p.Id).TPM__c;
                     }
                 } 
             }
         }
    }
	public static void creatsKuYanName(List<Product2> newList)    
	{
		Set<Id> qmId = new Set<Id>();
		Set<Id> baseSkuQmId = new Set<Id>();
		Set<Id> dmId = new Set<Id>(); 
		Set<Id> baseSkuIds = new Set<Id>();
		Map<Id,Product2> baseSkuMap = new Map<Id,Product2>();
		Map<id,Quality_Master__c> qmMap = new Map<Id,Quality_Master__c>();
		Map<Id,Denier_Master__c> dmMap = new Map<Id,Denier_Master__c>();
		Map<id,Quality_Master__c> baseSkuQmMap = new Map<Id,Quality_Master__c>();


		for(Product2 p:newList)
		{
			dmId.add(p.Denier__c);
			qmId.add(p.Quality_Name__c);
			if(p.RecordTypeId == skuTwistedYarnRecordTypeId)
			{
				baseSkuIds.add(p.Base_Fabric__c);
			}
		}
		
		qmMap = QualityMasterService.qualityMasterMap(qmId);
		dmMap = DenierMasterService.denierMasterMap(dmId);
		
		baseSkuMap = Product2Service.getAllProductByIds(baseSkuIds);
		
		for(Product2 p : baseSkuMap.values())
		{
			baseSkuQmId.add(p.Quality_Name__c);
		}

		baseSkuQmMap = QualityMasterService.qualityMasterMap(baseSkuQmId);
		
		for(Product2 p:newList)
		{
           	
			// Creating yan Name and Yan Alias
          	if(p.RecordTypeId == skuYarnRecordTypeId)
          	{
          		Quality_Master__c qm = qmMap.get(p.Quality_Name__c);
           		Denier_Master__c dm = dmMap.get(p.Denier__c);

          		p.ProductCode = qm.Quality_Code__c + '-' + dm.Name + '-' + p.Ply__c;
          		p.Name = qm.Quality_Code__c + '-' + dm.Name + '-' + p.Ply__c;
                
                if (p.Twist_Type__c != null) {
                    p.ProductCode += '-' + (p.Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : p.Twist_Type__c); 
                    p.Name += '-' + (p.Twist_Type__c.equalsIgnoreCase(Constants.PRODUCT_TWIST_TYPE_UNKNOWN) ? Constants.PRODUCT_TWIST_TYPE_UNKNOWN_TWI : p.Twist_Type__c); 
                }
                
                if (p.TPM__c != null) {
                    p.ProductCode += '-' + p.TPM__c;
                    p.Name += '-' + p.TPM__c;
                }

          	}
                       
          	
          	////For Twisted Yarn
          	//if(p.RecordTypeId == skuTwistedYarnRecordTypeId)
          	//{
          	//	Quality_Master__c qm = baseSkuQmMap.get(baseSkuMap.get(p.Base_Fabric__c).Quality_Name__c);
           //		Denier_Master__c dm = dmMap.get(p.Denier__c);
          	//	p.Quality_Name__c = qm.Id;

          	//	p.ProductCode = qm.Quality_Code__c + '-' + dm.Name + '-' + p.Ply__c + '-' + p.Twist_Type__c;
          	//	p.Name = qm.Quality_Code__c + '-' + dm.Name + '-' + p.Ply__c + '-' + p.Twist_Type__c;
          		
          	//}

		}
	}
	public static void creatsKuFabricName(List<Product2> newList)
	{
		for(Product2 p:newList)
		{
			String cat='';
			if(p.Family == Constants.PRODUCT_FAMILY_JACQUARD)
			{
				cat = Constants.PRODUCT_FAMILY_JACQUARD_CODE;
		    }
		    else if(p.Family == Constants.PRODUCT_FAMILY_DOBBY)
			{
				cat = Constants.PRODUCT_FAMILY_DOBBY_CODE;
		    }
		    else if(p.Family == Constants.PRODUCT_FAMILY_EMBROIDERY)
			{
				cat = Constants.PRODUCT_FAMILY_EMBROIDERY_CODE;
		    }

          // Creating fabreic Name and fabreic Alias

			p.ProductCode = cat + '-' + (String.isBlank(p.Trial_Number__c) ? p.ColourName__c  : ('-' + p.ColourName__c));
            p.Name = cat + '-' + (String.isBlank(p.Trial_Number__c) ? p.ColourName__c : ('-' + p.ColourName__c));
        }
	}

	public static void createPriceBookEntry(List<Product2> skuList, Set<Id> skuIds)
	{
		Pricebook2 stdPriceBook = Product2Service.getStandardPriceBook();
		Map<Id,PricebookEntry> skuPbeMap = new Map<Id,PricebookEntry>();
		List<PricebookEntry> pbeListToUpsert = new List<PricebookEntry>();
		
		if(skuIds.size() > 0)
		{
			skuPbeMap = Product2Service.getPricebookEntriesMapBySkuIds(skuIds);
		}

		for(Product2 sku : skuList)
		{
			PricebookEntry pbe = new PricebookEntry();
			
			if(sku.Id != null && skuPbeMap.containsKey(sku.Id))
			{
				pbe = skuPbeMap.get(sku.Id);
			}
      else
      {
        pbe.Pricebook2Id = stdPriceBook.Id;
        pbe.Product2Id = sku.Id;
        pbe.Unitprice = sku.Standard_FOB_Cost__c;
        pbe.Isactive = true;
      }
			
			  
		    pbe.Standard_FOB_Cost__c = sku.Standard_FOB_Cost__c;
		    pbe.Standard_CIF_Cost__c = sku.Standard_CIF_Cost__c;
		    pbe.Express_FOB_Cost__c = sku.Express_FOB_Cost__c;
		    pbe.Express_CIF_Cost__c = sku.Express_CIF_Cost__c;
		    pbe.Custom_FOB_Cost__c = sku.Custom_FOB_Cost__c;
		    pbe.Custom_CIF_Cost__c = sku.Custom_CIF_Cost__c;
		    pbeListToUpsert.add(pbe);
		}

		if(pbeListToUpsert.size() > 0)
		{
			upsert pbeListToUpsert;
		}
		
	}

	public static void createYarnSkuProcesses(List<Product2> skuList)
	{
		List<SKU_Process__c> skuProcessList = new List<SKU_Process__c>();
		List<Process__c> dyeingProcess = ProcessService.getProcessListByProcessName(Constants.PROCESS_NAME_DYEING);
		List<Process__c> twistingProcess = ProcessService.getProcessListByProcessName(Constants.PROCESS_NAME_TWISTING);
		for(Product2 sku : skuList)
		{
			if(dyeingProcess.size() > 0)	
			{
				Sku_Process__c dyeingSkuProcess = new Sku_Process__c();
				dyeingSkuProcess.SKU__c = sku.Id;
				dyeingSkuProcess.hasIO__c = false;
				dyeingSkuProcess.Process__c = dyeingProcess[0].Id;
				dyeingSkuProcess.State__c = dyeingProcess[0].Yarn_State__c;
				skuProcessList.add(dyeingSkuProcess);
			}

			if(sku.Yarn_Type__c == Constants.PRODUCT_YARN_TYPE_TWISTED && twistingProcess.size() > 0)
			{
				Sku_Process__c twistingSkuProcess = new Sku_Process__c();
				twistingSkuProcess.SKU__c = sku.Id;
				twistingSkuProcess.hasIO__c = false;
				twistingSkuProcess.Process__c = twistingProcess[0].Id;
				twistingSkuProcess.State__c = twistingProcess[0].Yarn_State__c;
				skuProcessList.add(twistingSkuProcess);
			}
		}

		if(skuProcessList.size() > 0)
		{
			insert skuProcessList;
		}
	}
}