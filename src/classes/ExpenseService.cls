public class ExpenseService {
	public ExpenseService() {
		
	}

	public static List<AggregateResult> getSumOfExpenseByCampaign (Set<Id> campaignIds) {
		List<AggregateResult> res = new List<AggregateResult> ();

		for (AggregateResult aggRes : [Select SUM(Amount__c) total, 
        								Campaign_Name__c campaignId  
        								from Expense__c 
        								where Campaign_Name__c in : campaignIds
        								AND Approval_Status__c = : Constants.EXPENSE_APPROVAL_STATUS_APPROVED
        								group by Campaign_Name__c]) {
        		res.add (aggRes);
        }

        return res;
	}
}