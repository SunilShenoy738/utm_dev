public class OrderPageExtension {
    
    private final Order o{get;set;}
    private final ID cusId{get;set;}
    public List<OrderItem> oitmList{get;set;}
    
    public OrderPageExtension(ApexPages.StandardController stdController) {
        this.o = (Order)stdController.getRecord();
        System.debug('order id ' + o.Id);
        this.cusID = o.AccountId;
        System.debug('account id ' + o.AccountId);
        
        oitmList = [Select Id , OrderItemNumber, Product2.Description, Supplier__r.Name, Width_in_Inches__c, Total__c, UnitPrice, TotalPrice, EndDate
                   From OrderItem where Order.id =: o.Id];
        
        System.debug('oitem list:' + oitmList );       
    }

}