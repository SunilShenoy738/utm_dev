public with sharing class FebricController {
    @AuraEnabled
    public static FabricWrapper getAllFabric(string productId)
    {
        FabricWrapper wrapper = new FabricWrapper();
       /* List<Bill_of_Material__c> results= [SELECT Id,Colour_Group__r.Name,Cost_Per_Meter__c,Costing_Wt_Mtr__c,
        Cramming_Requirement__c,Final_Costing_Weight_After_Crimpage__c,Final_Weight_After_Crimpage__c,PPC_per_Color__c,
        Total_Ends__c,Yarn__r.Name,RecordType.Name FROM Bill_of_Material__c where Fabric__c = :productId];*/

List<Bill_of_Material__c> bom = [SELECT Id,Warp_Master__c,Warp_SKU__c,Yarn__c,RecordType.Name,
Colour_Group__c,Cramming_Requirement__c,Degum_Loss_Percentage__c,Denier__c,Final_Costing_Weight_After_Crimpage__c,
Final_Weight_After_Crimpage__c,Percentage_Wasted__c,Picks_RPT__c,PPC_per_Color__c,Total_Ends__c,Dent__c,
Total_RM_Cost_Kg__c,Yarn_State__c,BeamPosition__c FROM Bill_of_Material__c WHERE Fabric__c = :productId];
      
        List<WarpSku> lstWeft= new List<WarpSku>();
        List<WarpSku> lstWarp;

        if(bom != null)
        {
            for (Bill_of_Material__c b : bom) 
            {
                if(b.RecordType.Name.toUpperCase()=='WARP') //Warp
                {
                    lstWarp= new List<WarpSku>();
                    lstWarp= getWarpDetail(b.Warp_Master__c);
                    wrapper.WarpName= lstWarp[0].WarpName;

                }
                else //Weft
                {
                  List<Product2> wftSkus=  getWeftDetail(b.Yarn__c);
                      for(Product2 p : wftSkus)
                      {
    //Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, Total_Physical_Weight__c,
    //PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, 

                        WarpSku wf= new WarpSku();
                        wf.ProductName=p.Name;
                        wf.ProductId=p.Id;
                        wf.Quality=p.Quality_Name__r.Name;
                        wf.ColourGroup=b.Colour_Group__c;
                        wf.Ply=p.Ply__c;
                        wf.YarnState=b.Yarn_State__c;
                        wf.PercentageWasted=b.Percentage_Wasted__c;
                        wf.Dinner= b.Denier__c;
                        wf.Noofends=b.Total_Ends__c;
                        wf.TotalPhysicalWeight=p.Total_Physical_Weight__c;
                        wf.CostingWeight=0;
                        wf.TotalWeightAfterCrimpage=b.Final_Weight_After_Crimpage__c;
                        wf.TotalCostAfterCrimpage=b.Final_Costing_Weight_After_Crimpage__c;
                        wf.TotalRMCostKg=b.Total_RM_Cost_Kg__c;
                        wf.DegumLoss= b.Degum_Loss_Percentage__c;
                        wf.IsWarp= false;
                        wf.Cramming=b.Cramming_Requirement__c;
                        wf.PPCPerCol= b.PPC_per_Color__c;
                        wf.Picks= b.Picks_RPT__c;
                        wf.Dent= b.Dent__c;
                        wf.RecordTypeName= p.RecordType.Name;
                        wf.SelectBeamPosition=b.BeamPosition__c;
                        lstWeft.add(wf);
                      }
                }
            }      
       
            for(Bill_of_Material__c bill : bom)
            {

                    for(WarpSku s : lstWarp)
                    {
                            system.debug(bill.Warp_Master__c + '***' + s.Id + '***' + bill.Warp_SKU__c + '***' + s.ProductId);
                            if(bill.Warp_Master__c == s.Id && bill.Yarn__c == s.ProductId)
                            {
                                s.PercentageWasted=bill.Percentage_Wasted__c;
                                s.Dinner= bill.Denier__c;
                                s.ColourGroup=bill.Colour_Group__c;
                                s.TotalPhysicalWeight= 0;//w.Total_Physical_Weight__c;
                                s.CostingWeight=0;//w.Costing_Weight__c;
                                s.TotalWeightAfterCrimpage=0;
                                s.TotalCostAfterCrimpage=0;
                                s.TotalRMCostKg=bill.Total_RM_Cost_Kg__c;
                                s.DegumLoss=bill.Degum_Loss_Percentage__c;
                                s.Dent= bill.Dent__c;
                                s.SelectBeamPosition=bill.BeamPosition__c;
                            }
                    }
            }   
        }
        
        system.debug(lstWarp);
        
            //Warp_Master__c,Warp_SKU__c,Yarn__c

          Product2 p= [SELECT  Id, Name,Actual_Height__c,Calculated_Height__c,Actual_Width__c,Warp_Wastage_in_Percentage__c,
          Weft_Wastage_in_Cms__c,Final_PPC__c,RecordType.Name,PPC__c,Reed_Width__c,Design_Source__c,Finished_Width__c,
          Studio_Reference_Name__c,EPI__c,Horizontal_RTP_cm__c,Vertical_RPT_cm__c,Development_Ref__c,Weave_Type__c,
          Reed__c,Size_Hooks_Repeat__c,Hooks_Shafts__c, Beams__c,Warping__c,Woven_PPC__c,
          Size_Picks_Repeat__c,Remarks__c,Physical_Fabric_Weight__c,Loom__c
          FROM Product2 where id = :productId];

          wrapper.ProductId=productId;
          wrapper.PPC= p.PPC__c;
          wrapper.FinalPPC= p.Final_PPC__c;
          wrapper.WeftWastageCms= p.Weft_Wastage_in_Cms__c;
          wrapper.WarpWastagePercentage= p.Warp_Wastage_in_Percentage__c;
          wrapper.ActualWidth= p.Actual_Width__c;
          wrapper.CalculatedHeight= p.Calculated_Height__c;
          wrapper.ActualHeight= p.Actual_Height__c;
          wrapper.FabricName= p.Name;
          wrapper.DesignStudioName=p.Studio_Reference_Name__c;
          wrapper.DesignSource=getDesignSource(); 
          wrapper.SelectedDesignSource=p.Design_Source__c;
          wrapper.DevelopmentRef=p.Development_Ref__c;
          wrapper.WeaveType= [Select Id,Name From CostingMaster__c];
          wrapper.SelectWeave='';
          //wrapper.ReedWidth=p.Reed_Width__c;
          wrapper.ReedWidthCms=p.Reed_Width__c;
          wrapper.Loom='';
          wrapper.HooksShafts=p.Hooks_Shafts__c;
          wrapper.EPI=p.EPI__c;
          wrapper.WeftWestage=0;
          wrapper.ActualHorizontalRepeat=p.Horizontal_RTP_cm__c;
          wrapper.TotalFabricWeight=0;
          wrapper.CalculatedVerticalRepeat=0;
          wrapper.FinishedWidth=p.Finished_Width__c;
          wrapper.ActualVerticalRepeat=p.Vertical_RPT_cm__c;
          wrapper.Remarks=p.Remarks__c;
          wrapper.StateOfYarn=getStateofYarn();
          wrapper.WeftList=lstWeft;
          wrapper.WarpList=lstWarp;
          wrapper.ColourList= getColorMaster();
          wrapper.Reed = getReed();
          wrapper.Beams=getBeams();
          wrapper.Warping=getWarping();
          wrapper.Loom= getLoom();
          wrapper.BeamPosition=getBeamPosition();
          wrapper.SelectedWeaveType= p.Weave_Type__c;
          wrapper.SelectedReed=p.Reed__c;
          wrapper.HooksRepeat= p.Size_Hooks_Repeat__c;
          wrapper.SelectedBeams= p.Beams__c;
          wrapper.SelectedWarping= p.Warping__c;
          wrapper.SelectedLoom= p.Loom__c;
          wrapper.WovenPPC=p.Woven_PPC__c;
          wrapper.PicksRepeat=p.Size_Picks_Repeat__c;
          wrapper.PhysicalFabricWeight=p.Physical_Fabric_Weight__c;
         // system.debug(wrapper);
          return wrapper;
    }

    public static string getBeamPosition(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Bill_of_Material__c.BeamPosition__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }

    public static string getReed(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Product2.Reed__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }
    public static string getWarping(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Product2.Warping__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }
 public static string getLoom(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Product2.Loom__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }

    public static string getBeams(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Product2.Beams__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }
    
    public static string getDesignSource(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Product2.Design_Source__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       string res=JSON.serialize(ple);
       return res;
    }

    @AuraEnabled
    public static List<Product2> getWeftDetail(string weftId){

        List<Product2> product= [SELECT  Id, Name,RecordType.Name, Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, Total_Physical_Weight__c,
         PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, Quality_Name__r.Name
         FROM Product2 where id = :weftId];
        if(product[0].RecordType.Name=='Grindles')
        {
            List<Composition__c> lstComp= [SELECT Yarn__c FROM Composition__c where SKU__c= :weftId];
            List<Id> pIds= new List<Id>();
            for(Composition__c c : lstComp){pIds.add(c.Yarn__c);}
            List<Product2> lstProducts= [SELECT  Id, Name,RecordType.Name, Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, 
            Total_Physical_Weight__c, PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, 
            Quality_Name__r.Name FROM Product2 where id IN :pIds];
            return lstProducts;
        }
        else
        {
            return product;
        }

    }

     @AuraEnabled
    public static List<WarpSku> searchWeft(string weftId){
        List<WarpSku> lstWeft= new List<WarpSku>();
        List<Product2> product= [SELECT  Id, Name,RecordType.Name, Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, 
        Total_Physical_Weight__c,
         PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, Quality_Name__r.Name
         FROM Product2 where id = :weftId];
         List<Product2> wftSkus=  new List<Product2>();
        if(product[0].RecordType.Name=='Grindles')
        {
            List<Composition__c> lstComp= [SELECT Yarn__c FROM Composition__c where SKU__c= :weftId];
            List<Id> pIds= new List<Id>();
            for(Composition__c c : lstComp){pIds.add(c.Yarn__c);}
            List<Product2> lstProducts= [SELECT  Id, Name,RecordType.Name, Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, 
            Total_Physical_Weight__c, PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, 
            Quality_Name__r.Name FROM Product2 where id IN :pIds];
            wftSkus= lstProducts;
        }
        else {  wftSkus= product;  } 


                      for(Product2 p : wftSkus)
                      {
    
                        WarpSku wf= new WarpSku();
                        wf.ProductName=p.Name;
                        wf.ProductId=p.Id;
                        wf.Quality=p.Quality_Name__r.Name;
                        wf.ColourGroup=p.Colour_Name__r.Name;
                        wf.Ply=p.Ply__c;
                        wf.YarnState=p.Yarn_State__c;
                        wf.PercentageWasted=0;
                        wf.Dinner= p.Denier__r.Dinner__c;
                        wf.Noofends=0;
                        wf.TotalPhysicalWeight=p.Total_Physical_Weight__c;
                        wf.CostingWeight=0;
                        wf.TotalWeightAfterCrimpage=0;
                        wf.TotalCostAfterCrimpage=0;
                        wf.TotalRMCostKg=0;
                        wf.DegumLoss= p.Degum_Percentage__c;
                        wf.IsWarp= false;
                        wf.Cramming=false;
                        wf.PPCPerCol= 0;
                        wf.Picks= p.Size_Picks_Repeat__c;
                        wf.Dent=0;
                        wf.SelectBeamPosition='';
                        lstWeft.add(wf);
                      }

                    return lstWeft;

    }


     @AuraEnabled
    public static List<Color_Master__c> getColorMaster(){
        return [SELECT  Id, Name FROM Color_Master__c];
    }

    @AuraEnabled
    public static List<WarpSku> getWarpDetail(string warpId){
        //Reed_Width__c,EPI__c
     List<Warp_SKU__c> warpLst= [SELECT Id,Name,Yarn_State__c,Warp_Master__r.Name, Yarn__r.Name,Yarn__r.Ply__c, 
     No_of_ends__c, Beam__r.Name, Warp_Master__c,Yarn__r.Quality_Name__r.Name,Yarn__r.Denier__r.Dinner__c,
     Yarn__r.Degum_Percentage__c,Yarn__r.Colour_Name__r.Name,Yarn__r.Cost_Per_Kg__c  
     ,Warp_Master__r.Reed_Width__c, Warp_Master__r.EPI__c,Percentage_Wasted__c,
     Yarn__r.RecordType.Name, Yarn__r.Calculated_Denier__c
     FROM Warp_SKU__c WHERE Warp_Master__r.Id = :warpId];

     List<WarpSku> lstWarps= new List<WarpSku>();
     for(Warp_SKU__c w : warpLst)
     {
        WarpSku ws= new WarpSku();
        ws.Id= w.Warp_Master__r.Id;
        ws.WarpSkuId= w.Id;
        ws.WarpName= w.Warp_Master__r.Name;
        ws.ProductName= w.Yarn__r.Name;
        ws.ProductId= w.Yarn__c;
        ws.Quality=w.Yarn__r.Quality_Name__r.Name;
        ws.Ply=w.Yarn__r.Ply__c;
        ws.YarnState=w.Yarn_State__c;
        ws.Noofends=w.No_of_ends__c;
        ws.PercentageWasted=0;//w.Percentage_Wasted__c;
        ws.Dinner= 0;//w.Dinner__c;
        ws.ColourGroup=''; //w.Colour_Group__c;
        ws.TotalPhysicalWeight= 0;//w.Total_Physical_Weight__c;
        ws.CostingWeight=0;//w.Costing_Weight__c;
        ws.TotalWeightAfterCrimpage=0;
        ws.TotalCostAfterCrimpage=0;
        ws.TotalRMCostKg=0;
        ws.DegumLoss=0;
        ws.IsWarp= true;
        lstWarps.add(ws);
     }
     system.debug('WARPS:');
     system.debug(lstWarps);
    //Yarn__r.Denier__r.Dinner__c ,Yarn__r.Colour_Name__r.Name,Yarn__r.Total_Physical_Weight__C, Yarn__r.PPC__c, Yarn__r.Size_Picks_Repeat__c, Yarn__r.Cost_Per_Kg__c, Yarn__r.Degum_Percentage__c, Yarn__r.Yarn_State__c, Yarn__r.Quality_Name__r.Name
     return lstWarps;
    }
    
     @AuraEnabled
    public static List<Bill_of_Material__c> getBomCount(string fabricId){
List<Bill_of_Material__c> results= [SELECT Id,Colour_Group__r.Name,Cost_Per_Meter__c,Costing_Wt_Mtr__c,Cramming_Requirement__c,
Final_Costing_Weight_After_Crimpage__c,Final_Weight_After_Crimpage__c,PPC_per_Color__c,Total_Ends__c,Yarn__r.Name,RecordType.Name

          FROM Bill_of_Material__c where Fabric__c = :fabricId];
      return results;
        
    }

     @AuraEnabled
    public static Product2 getFabricDetails(string fabricId){
        
       Product2 product= [SELECT  Id, Name,Actual_Height__c,Calculated_Height__c,Actual_Width__c,Warp_Wastage_in_Percentage__c,Weft_Wastage_in_Cms__c,Final_PPC__c,RecordType.Name,PPC__c,Reed_Width__c  FROM Product2 where id = :fabricId];
        return product;
    }

    public static string getStateofYarn(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Bill_of_Material__c.Yarn_State__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        string res=JSON.serialize(ple);
        return res;
    }

     @AuraEnabled
    public static List<CostingMaster__c> getWeaveType(){
        //Commented by Kunal -  Weave Type field was a picklist - it is changed to Lookup to CostingMaster object
        //
        //Schema.DescribeFieldResult fieldResult = Product2.Weave_Type__c.getDescribe();
        //List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //string res=JSON.serialize(ple);
        //return res;
        //
        //Commented by Kunal
      return [Select Id,Name From CostingMaster__c];
    }

    @AuraEnabled
    public static String SaveBillOfMatterial(string bom)
    {
        system.debug(bom);
        List<RecordType> lstRecType= [SELECT Id,Name FROM RecordType WHERE SobjectType='Bill_of_Material__c'];
        Id WarpRecType;
        Id WeftRecType;
        if(lstRecType[0].Name.toUpperCase()=='WARP')
        {
            WarpRecType=lstRecType[0].Id;
            WeftRecType=lstRecType[1].Id;
        }
        else
        {
            WarpRecType=lstRecType[1].Id;
            WeftRecType=lstRecType[0].Id;
        }


        FabricWrapper obj = (FabricWrapper)JSON.deserialize(bom, FabricWrapper.class);
        List<WarpSku> lstWarp=obj.WarpList;
        List<WarpSku> lstWeft= obj.WeftList;
        Product2 prod= new Product2();
        prod.Id= obj.ProductId;
        prod.PPC__c= obj.PPC;
        prod.Final_PPC__c=obj.FinalPPC;
        prod.Weft_Wastage_in_Cms__c=obj.WeftWastageCms;

          prod.Warp_Wastage_in_Percentage__c= obj.WarpWastagePercentage ;
          prod.Actual_Width__c=obj.ActualWidth;
          prod.Calculated_Height__c=obj.CalculatedHeight;
          prod.Physical_Fabric_Weight__c= obj.PhysicalFabricWeight;
          prod.Actual_Height__c=obj.ActualHeight;
          prod.Studio_Reference_Name__c=obj.DesignStudioName;
          prod.Design_Source__c=obj.SelectedDesignSource;
          prod.EPI__c=obj.EPI;
          prod.Reed_Width__c =obj.ReedWidthCms;
          prod.Horizontal_RTP_cm__c=obj.ActualHorizontalRepeat;
          prod.Finished_Width__c=obj.FinishedWidth;
          prod.Vertical_RPT_cm__c=obj.ActualVerticalRepeat;
          prod.Development_Ref__c = obj.DevelopmentRef;
          prod.Weave_Type__c=obj.SelectedWeaveType;
          prod.Reed__c = obj.SelectedReed;
          prod.Size_Hooks_Repeat__c=obj.HooksRepeat;
          prod.Hooks_Shafts__c= obj.HooksShafts;
          prod.Beams__c=obj.SelectedBeams;
          prod.Warping__c= obj.SelectedWarping;
          prod.Loom__c = obj.SelectedLoom;
          prod.Woven_PPC__c=obj.WovenPPC;
          prod.Size_Picks_Repeat__c=obj.PicksRepeat;
          prod.Remarks__c= obj.Remarks;

          List<Bill_of_Material__c> bill = new List<Bill_of_Material__c>();
          for(WarpSku w : lstWarp)
          {
            Bill_of_Material__c b = new Bill_of_Material__c();
            b.RecordTypeId=WarpRecType;
            b.BeamPosition__c= w.SelectBeamPosition;
            b.Colour_Group__c= w.ColourGroup;
            b.Costing_Weight__c=w.CostingWeight;
            b.Degum_Loss_Percentage__c= w.DegumLoss;
            b.Denier__c= w.Dinner;
            b.Dent__c   = w.Dent;
            b.Fabric__c= obj.ProductId;
            b.Final_Costing_Weight_After_Crimpage__c    = w.TotalCostAfterCrimpage;
            b.Final_Weight_After_Crimpage__c= w.TotalWeightAfterCrimpage;
            b.Percentage_Wasted__c= w.PercentageWasted;
            b.Total_Ends__c= w.Noofends;
            b.Total_RM_Cost_Kg__c=w.TotalRMCostKg;
            b.Warp_Master__c=w.Id;
            b.Warp_SKU__c= w.WarpSkuId;
            b.Weight__c=w.TotalPhysicalWeight;
            b.Yarn_State__c=w.SelectedYarnStates;
            b.Yarn__c=w.ProductId;
            bill.add(b);
          }
          for(WarpSku w : lstWeft)
          {
            Bill_of_Material__c b = new Bill_of_Material__c();
             b.RecordTypeId=WeftRecType;
             b.Colour_Group__c= w.ColourGroup;
             b.Costing_Weight__c=w.CostingWeight;
             b.Cramming_Requirement__c= w.Cramming;
             b.Degum_Loss_Percentage__c= w.DegumLoss;
             b.Denier__c= w.Dinner;
             b.Fabric__c= obj.ProductId;
             b.Final_Costing_Weight_After_Crimpage__c    = w.TotalCostAfterCrimpage;
             b.Final_Weight_After_Crimpage__c= w.TotalWeightAfterCrimpage;
             b.Percentage_Wasted__c= w.PercentageWasted;
             b.Picks_RPT__c=w.Picks;
             b.PPC_per_Color__c= w.PPCPerCol;
             b.Total_RM_Cost_Kg__c=w.TotalRMCostKg;
             b.Weight__c=w.TotalPhysicalWeight;
             b.Yarn_State__c=w.SelectedYarnStates;
             b.Yarn__c=w.ProductId;

              bill.add(b);
            
          }
          List<Bill_of_Material__c> bomToDelete = [SELECT Id FROM Bill_of_Material__c WHERE Fabric__c = : obj.ProductId];
         delete bomToDelete;

          insert bill;
          update prod;
           /*obj.SelectWeave='';
          obj.Loom='';
          obj.HooksShafts=0;
          obj.WeftWestage=0;
          obj.PicksRepeat=0;
          obj.TotalFabricWeight=0;
          obj.CalculatedVerticalRepeat=0;
          obj.Remarks='';
          */
         
        return 'success';
    }

    @AuraEnabled
    public static String saveBOM(string lstWarp,string lstWeft, Product2 product) {

        List<RecordType> lstRecType= [SELECT Id,Name FROM RecordType WHERE SobjectType='Bill_of_Material__c'];
        Id WarpRecType;
        Id WeftRecType;
        if(lstRecType[0].Name.toUpperCase()=='WARP')
        {
            WarpRecType=lstRecType[0].Id;
            WeftRecType=lstRecType[1].Id;
        }
        else
        {
            WarpRecType=lstRecType[1].Id;
            WeftRecType=lstRecType[0].Id;
        }

        List<Bill_of_Material__c> objWarp = (List<Bill_of_Material__c>)JSON.deserialize(lstWarp, List<Bill_of_Material__c>.class);
        List<Bill_of_Material__c> objWeft = (List<Bill_of_Material__c>)JSON.deserialize(lstWeft, List<Bill_of_Material__c>.class);
        
        for(Bill_of_Material__c w : objWarp)
        {
            w.RecordTypeId=WarpRecType;
        }
        for(Bill_of_Material__c w : objWeft)
        {
            w.RecordTypeId=WeftRecType;
        }
        system.debug('WARP and WEFT');
        system.debug(objWarp);
        system.debug(objWeft);
        insert objWarp;
        System.debug('Socting Weight: '+objWeft);
        insert objWeft;
        
        
        //added by Kunal
        //Start
        List<Bill_of_Material__c> bomList = new List<Bill_of_Material__c>();
        bomList.addAll(objWeft);
        bomList.addAll(objWarp);
        System.debug('createFabricCompositionAfterAddingBom');
        CompositionService.createFabricCompositionAfterAddingBom(bomList);
        //End
        
        update product;
        
        return 'success';
    }

    @AuraEnabled
    public static List<SKU_Process__c> getRMCost(List<String> SKUs){
        
    List<SKU_Process__c> skuList= [SELECT SKU__c,Yarn_State__c,Cost_Per_Kg__c FROM SKU_Process__c WHERE SKU__c IN :SKUs];
     return skuList;
    }

@AuraEnabled
    public static string deleteBomItems(string fabricId){
        List<Bill_of_Material__c> lstBom= [SELECT Id FROM Bill_of_Material__c Where Fabric__c = :fabricId];
        delete lstBom;
     return 'success';
    }
   
    
   
}