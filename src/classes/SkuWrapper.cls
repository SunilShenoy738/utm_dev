public class SkuWrapper {
	public String Id{get;set;}
    public String SelectedCost{get;set;}
    public Decimal Rate{get;set;}
    public Decimal Freight{get;set;}
    public Decimal Adjustment{get;set;}
    public Decimal NetProfit{get;set;}
    public Decimal Total{get;set;}
    public Integer Qty{get;set;}
    public Boolean CustomerProduct{get;set;}
    public String Comments{get;set;}
    public String SelectedRate{get;set;}
    public String SelectedUOM{get;set;}
    public Decimal Width{get;set;}
    public Decimal AgentPercentage{get;set;}
    public Decimal AgentAmount{get;set;}
    public Decimal RatePerYard{get;set;}

}