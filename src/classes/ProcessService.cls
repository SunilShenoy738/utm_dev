public class ProcessService {
    
    public static Process__c getProcessById(Id processId)
    {
        return [Select Id, Name, Process_Code__c, Yarn_State__c From Process__c Where Id =: processId];
    }

    public static Map<Id,Process__c> getProcessMapByIds(Set<Id> processIds)
    {
        Map<Id, Process__c> processMap = new Map<Id, Process__c>([Select Id, Name, Process_Code__c, Yarn_State__c From Process__c Where Id =: processIds]);
        return processMap;
    }

    public static List<Process__c> getProcessListByProcessType(String processType)
    {
    	return [Select Id, Name, Process_Code__c, Process_Type__c, Yarn_State__c, Time_per_Process__c, (Select Id, Name, Process_Code__c, Process_Type__c, Yarn_State__c, Parent_Process__c,  Time_per_Process__c From Processes__r), (Select Id, Name, Vendor__c, Vendor__r.Name, Cost_Per_Kg__c, Cost_Per_Metre__c From Vendor_Procresses__r) From Process__c Where Process_Type__c =:processType And Parent_Process__c = null];
    }

    public static List<Process__c> getProcessListByProcessName(String processName)
    {
        return [Select Id, Name, Process_Code__c, Process_Type__c, Yarn_State__c, (Select Id, Name, Process_Code__c, Process_Type__c, Yarn_State__c, Parent_Process__c From Processes__r), (Select Id, Name, Vendor__c, Vendor__r.Name, Cost_Per_Kg__c, Cost_Per_Metre__c From Vendor_Procresses__r) From Process__c Where Name =:processName];
    }
    
}