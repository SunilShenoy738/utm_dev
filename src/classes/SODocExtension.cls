public class SODocExtension {
    
    private final Order o{get;set;}
    private final ID cusId{get;set;}
    public List<OrderItem> oitmList{get;set;}
    public Map<String, String> accSKUMap {get;set;}
    public Set<Id> skuIds = new Set<Id>();
    
    public SODocExtension(ApexPages.StandardController stdController) {
        this.o = (Order)stdController.getRecord();
        System.debug('order id ' + o.Id);
        this.cusID = o.AccountId;
        accSKUMap = new Map<String, String>();
        System.debug('account id ' + o.AccountId);
        
        oitmList = [Select Id , OrderItemNumber, Product2Id, Product2.Name, Product2.Description, Product2.ColourName__c, Product2.HS_Code__r.Name, Supplier__r.Name, Width_in_Inches__c, Total__c, UnitPrice, TotalPrice, EndDate
                   From OrderItem where Order.id =: o.Id];

        System.debug('oitem list:' + oitmList );  

        for(OrderItem oli : oitmList) 
        {
            skuIds.add(oli.Product2Id);
        }

        System.debug('skuIds:' + skuIds );  

        for(Account_SKU__c accSku : [Select Id, Colour_Description__c, Account__c, SKU__r.Name, SKU__c From Account_SKU__c Where Account__c =: o.AccountId And SKU__c =:skuIds]) {
            if(accSku.Colour_Description__c == null)
            {
                accSKUMap.put(accSku.SKU__r.Name, '');
            }
            else
            {
                accSKUMap.put(accSku.SKU__r.Name, accSku.Colour_Description__c);   
            }
            
        }
        System.debug('accSKUMap :' + accSKUMap );  
             
    }

}