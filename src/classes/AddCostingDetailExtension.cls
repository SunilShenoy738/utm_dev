public class AddCostingDetailExtension
{
    public Product2 fabricRec {get;set;}
    public Decimal totalExpressCost {get;set;}
    public Decimal totalStandardCost {get;set;}
    public Decimal totalCustomCost {get;set;}
    public Decimal totalRMCost {get;set;}
    public Decimal totalFinishRate {get;set;}
    public Product2 stdController {get;set;}
    public Decimal expressCIFCost {get;set;}
    public Decimal standardCIFCost {get;set;}
    public Decimal customCIFCost {get;set;}
    public Decimal expressFOBCost {get;set;}
    public Decimal standardFOBCost {get;set;}
    public Decimal customFOBCost {get;set;}
    public Id weaveTypeId;
    public CostingMaster__c costingMaster {get;set;}
    
    public List<Bill_Of_Material__c> bomList {get;set;}
    public List<SKU_Process__c> skuProcessList {get;set;}
    
    public AddCostingDetailExtension(ApexPages.StandardController controller) {
        stdController = (Product2)controller.getRecord();
        fabricRec = new Product2();
        fabricRec = (Product2)controller.getRecord();
        weaveTypeId = Product2Service.getProductById(stdController.Id).Weave_Type__c;
        totalRMCost = 0;
        totalFinishRate = 0;
        
        expressCIFCost = 0;
        standardCIFCost = 0;
        customCIFCost = 0;
        expressFOBCost = 0;
        standardFOBCost = 0;
        customFOBCost = 0;
        System.debug('Std Weav Type: '+weaveTypeId);
        costingMaster = CostingMasterService.getCostingMasterDetailsById(weaveTypeId); 
        if(costingMaster == null)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Warning!!! Weave Costing Category not selected for '+stdController.Name+'.')); 
            return;
        }
        
        getBillOfMaterialsCost();
        getSKUProcessCost();
        calculateTotalCost();
    }
    
    public void updateCostingDetail()
    {
        fabricRec.Total_RM_Cost_Including_Dyeing__c = totalRMCost;
        fabricRec.Total_Finishing_Cost__c = totalFinishRate;
        fabricRec.Last_Costing_Done__c = Date.today();
        update fabricRec;
        System.debug('updateCostingDetail fabricRec: '+fabricRec);
    }
    
    public void calculateTotalCost()
    {
        
        fabricRec.Express_Conversion_Cost__c = costingMaster.Express_Conversion_Cost__c;
        fabricRec.Bulk_Conversion_Cost__c = costingMaster.Standard_Conversion_Cost__c;
        //fabricRec.Custom_Conversion_Cost__c = costingMaster.Custom_Conversion_Cost__c;

        if(fabricRec.Express_Conversion_Cost__c == null)
            fabricRec.Express_Conversion_Cost__c = 0;
        if(fabricRec.Bulk_Conversion_Cost__c == null)
            fabricRec.Bulk_Conversion_Cost__c = 0;
        if(fabricRec.Custom_Conversion_Cost__c == null)
            fabricRec.Custom_Conversion_Cost__c = 0;
        if(fabricRec.Express_Depreciation__c == null)
            fabricRec.Express_Depreciation__c = 0;
        if(fabricRec.Express_Loan__c == null)
            fabricRec.Express_Loan__c = 0;
        if(fabricRec.Express_PPC_Surcharge__c == null)
            fabricRec.Express_PPC_Surcharge__c = 0;
        if(fabricRec.Express_Freight__c == null)
            fabricRec.Express_Freight__c = 0; 
        if(fabricRec.Standard_Depreciation__c == null)
            fabricRec.Standard_Depreciation__c = 0;
        if(fabricRec.Standard_Loan__c == null)
            fabricRec.Standard_Loan__c = 0;
        if(fabricRec.Standard_PPC_Surcharge__c == null)
            fabricRec.Standard_PPC_Surcharge__c = 0;
        if(fabricRec.Standard_Freight__c == null)
            fabricRec.Standard_Freight__c = 0; 
        if(fabricRec.Custom_Depreciation__c == null)
            fabricRec.Custom_Depreciation__c = 0;
        if(fabricRec.Custom_Loan__c == null)
            fabricRec.Custom_Loan__c = 0;
        if(fabricRec.Custom_PPC_Surcharge__c == null)
            fabricRec.Custom_PPC_Surcharge__c = 0;
        if(fabricRec.Custom_Freight__c == null)
            fabricRec.Custom_Freight__c = 0;   
        if(costingMaster.Freight__c == null)
            costingMaster.Freight__c = 0;      
           
        
        expressFOBCost = totalRMCost + totalFinishRate + fabricRec.Express_Conversion_Cost__c + fabricRec.Express_Depreciation__c + fabricRec.Express_Loan__c + fabricRec.Express_PPC_Surcharge__c;
        expressCIFCost = costingMaster.Freight__c + expressFOBCost + fabricRec.Express_Freight__c;
        
        standardFOBCost = totalRMCost + totalFinishRate + fabricRec.Bulk_Conversion_Cost__c + fabricRec.Standard_Depreciation__c + fabricRec.Standard_Loan__c + fabricRec.Standard_PPC_Surcharge__c;
        standardCIFCost = costingMaster.Freight__c + standardFOBCost + fabricRec.Standard_Freight__c;
        
        customFOBCost = totalRMCost + totalFinishRate + fabricRec.Custom_Conversion_Cost__c + fabricRec.Custom_Depreciation__c + fabricRec.Custom_Loan__c + fabricRec.Custom_PPC_Surcharge__c;
        customCIFCost = costingMaster.Freight__c + customFOBCost + fabricRec.Custom_Freight__c;
        
    }
    
    public void getBillOfMaterialsCost()
    {
        bomList = BillOfMaterialService.getBomBySKUId(fabricRec.Id); //[Select Id,Name,Yarn__c,Yarn__r.Name,Total_RM_Cost_Kg__c,Costing_Wt_Mtr__c,Cost_Per_Meter__c From Bill_Of_Material__c Where Fabric__c =:fabricRec.Id];
        System.debug('bomList: '+bomList);
        for(Bill_Of_Material__c bom : bomList)
        {
            totalRMCost += bom.Cost_Per_Meter__c;
        }
    }
    
    public void getSKUProcessCost()
    {
        skuProcessList = SKUProcessService.getSKUProcessBySkuIdWithoutIO(fabricRec.Id); //[Select Id, Cost_Per_Kg__c, Process__c, Vendor__c, Vendor__r.Name, SKU__c, Process__r.Name From SKU_Process__c Where SKU__c =: fabricRec.Id];
        for(SKU_Process__c skuprocess : skuProcessList)
        {
            if( skuProcess.Cost_Per_Kg__c == null)
            {
                 skuProcess.Cost_Per_Kg__c = 0;
            }
            if( skuProcess.Cost_per_Mtr__c == null)
            {
                 skuProcess.Cost_per_Mtr__c = 0;
            }
            totalFinishRate += skuProcess.Cost_per_Mtr__c;
        }
    }
    
    public PageReference cancel()
    {
       PageReference myVFPage = new PageReference('/apex/AddCostingDetail');
       myVFPage.setRedirect(true);
       myVFPage.getParameters().put('Id', fabricRec.Id);
       return myVFPage;
    }
    

    
}