public class DenierMasterService
{
	public static Map<id,Denier_Master__c> denierMasterMap(Set<Id> dm)
	{
		Map<id,Denier_Master__c> dmMap= new Map<id,Denier_Master__c>([Select Id,name from Denier_Master__c where Id IN : dm]);
        return dmMap;
	}
}