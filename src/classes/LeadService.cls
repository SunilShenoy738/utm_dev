public class LeadService
{
    public static LeadStatus getLeadConvertedStatus()
    {
        return [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    }
}