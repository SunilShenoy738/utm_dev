public class CloneQuote {
    
    public Quote stdController {get;set;}

    public CloneQuote(ApexPages.StandardController controller) {
        stdController = (Quote)controller.getRecord();
    }

    public PageReference cloneQuote() {
        Quote currentQuote = QuoteService.getQuoteAndQuoteLineItemsByQuoteId(stdController.Id);
        
        Quote newQuote = currentQuote.clone(false, true);
        newQuote.Name = currentQuote.Opportunity.Name;
        newQuote.Status = Constants.QUOTE_STATUS_DRAFT;
        try {
            insert newQuote;
            
            List<QuoteLineItem> newQlis = new List<QuoteLineItem>();
        
            for(QuoteLineItem qli : newQuote.QuoteLineItems) {
                QuoteLineItem newQli = qli.clone(false, true);
                newQli.QuoteId = newQuote.Id;
                newQli.Agent_Commission_Percentage__c = qli.Agent_Commission_Percentage__c;
                newQli.Agent_Commission_in_Rs__c = qli.Agent_Commission_in_Rs__c;
                newQli.Cost_Type__c = qli.Cost_Type__c;
                newQli.Freight__c = qli.Freight__c;
                newQli.Net_Profit__c = qli.Net_Profit__c; 
                newQli.Rate__c = qli.Rate__c;
                newQli.Rate_Type__c = qli.Rate_Type__c;
                newQli.Total__c = qli.Total__c;
                newQlis.add(newQli);
            }
            
            try {
                insert newQlis;
                
                
            } catch(Exception e) {
                System.debug('QLI Insert Error Message: ' + e.getMessage() + ' LineNumber: ' + e.getLineNumber() + ' Exception Type: ' + e.getTypeName() + ' Cause: ' + e.getCause());
            }
            
        } catch (Exception e){
            System.debug('Quote Insert Error Message: ' + e.getMessage() + ' LineNumber: ' + e.getLineNumber() + ' Exception Type: ' + e.getTypeName() + ' Cause: ' + e.getCause());
        }
        
       PageReference myVFPage = new PageReference('/'+stdController.Id);
       myVFPage.setRedirect(true);
       myVFPage.getParameters().put('Id', stdController.Id);
       return myVFPage;
    }
}