public class OrderTriggerHandler 
{
    public static void onBeforeInsert(List<Order> newList)
    {
        setStandardPriceBookId(newList);
    }
    public static void onAfterInsert(List<Order> newList)
    {
        String recid=Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.RT_ORDER_SALES_ORDER).getRecordTypeId();

        List<Order> orderList=new List<Order>();

        for(Order od:newList)
        {
            if(od.RecordTypeId==recid)
            orderList.add(od);
        }

        if(orderList.size()>0)
        {
            createOrderlineItem(orderList);
        }
    }

    public static void setStandardPriceBookId(List<Order> newList)
    {
        PriceBook2 standardPriceBook = Product2Service.getStandardPriceBook();
        for(Order ord : newList)
        {
            ord.PriceBook2Id = standardPriceBook.Id;
        }
    }
        
    
    public static void createOrderlineItem(List<Order> newList)
    {
        List<OrderItem> orderIt=new List<OrderItem>();

        Set<Id> oppLt=new Set<Id>();

        for(order ord: newList)
        {
           oppLt.add(ord.OpportunityId);
        }


        List<OpportunityLineItem> opprLineItemList=OpportunityLineItemService.getOpportunityLineItemByOpportunityId(oppLt);
        
        Map<id,List<OpportunityLineItem>> oppLineItem=new Map<id,list<OpportunityLineItem>>();

        
        for(OpportunityLineItem opp:opprLineItemList)
        {
           List<OpportunityLineItem> ol=new List<OpportunityLineItem>();

            if(oppLineItem.containsKey(opp.OpportunityId))
            {
               ol=oppLineItem.get(opp.OpportunityId);
               ol.add(opp);
            }
            else 
            {
                ol.add(opp);
                
            }
           
            oppLineItem.put(opp.OpportunityId, ol);
     
        }

        if(oppLineItem.size()>0)
        {       
            for(order ord: newList)
            {
               List<OpportunityLineItem> opplineList=oppLineItem.get(ord.OpportunityId);

               for(OpportunityLineItem oplt:opplineList)
               {
                       OrderItem ot = new OrderItem();
                       ot.OrderId=ord.id;
                       ot.PricebookEntryId=oplt.PricebookEntryId; 
                       ot.Quantity=oplt.Quantity;
                       ot.ServiceDate = oplt.ServiceDate;
                       ot.UnitPrice=oplt.UnitPrice;
                       ot.Adjustment__c=oplt.Adjustment__c;
                       ot.Agent_Commission_in_Rs__c=oplt.Agent_Commission_in_Rs__c;
                       ot.Agent_Commission_Percentage__c=oplt.Agent_Commission_Percentage__c;
                       ot.Cost_Type__c=oplt.Cost_Type__c;
                       ot.Freight__c=oplt.Freight__c;
                       ot.Net_Profit__c=oplt.Net_Profit__c;
                       ot.Rate__c=oplt.Rate__c;
                       ot.Rate_Type__c=oplt.Rate_Type__c;
                       ot.Total__c=oplt.Total__c;             
                       OrderIt.add(ot);
                }   
            }

        }
        if (orderIt.size()>0)
        {
            try
            {
             insert orderIt;
            }
            catch(Exception e)
            {
                System.debug('OrderItem insertion error Error message '+e);
                OrderIt[0].addError(e.getMessage());
            }
        }
    }

}