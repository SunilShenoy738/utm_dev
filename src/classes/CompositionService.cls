public class CompositionService {
	
	public static Id recordTypeByName(String rtName) {
        return Schema.SObjectType.Composition__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
    }

	public static Map<Id, List<Composition__c>> getCompositionBySkuIds(Set<Id> skuIds)
	{
		Map<Id, List<Composition__c>> compMap = new Map<Id, List<Composition__c>>();

		for(Product2 sku : [Select Id, (Select Id, Composition_Percentage__c, Constituents__c, SKU__c From Compositions__r) From Product2 Where Id =: skuIds])
		{
			if(sku.Compositions__r != null && sku.Compositions__r.size() > 0)
			{
				compMap.put(sku.Id,sku.Compositions__r);
			}
		}

		return compMap;
	}

	public static Map<Id, List<Composition__c>> getCompositionByQualityIds(Set<Id> qualityIds)
	{
		Map<Id, List<Composition__c>> compMap = new Map<Id, List<Composition__c>>();

		for(Quality_Master__c quality : [Select Id, (Select Id, Composition_Percentage__c, Constituents__c, Quality_Master__c From Compositions__r) From Quality_Master__c Where Id =: qualityIds])
		{
			if(quality.Compositions__r != null && quality.Compositions__r.size() > 0)
			{
				compMap.put(quality.Id,quality.Compositions__r);
			}
		}

		return compMap;
	}

	public static Map<String, String> getTotalCompositionPercentageByQualityIds(Set<Id> qualityIds)
	{
		Map<String, String> compMap = new Map<String, String>();

		for(AggregateResult ar : [Select Quality_Master__c, SUM(Composition_Percentage__c)  From Composition__c Where Quality_Master__c =: qualityIds Group By Quality_Master__c])
		{
			compMap.put(String.valueOf(ar.get('Quality_Master__c')),String.valueOf(ar.get('expr0')));
		}

		return compMap;
	}


	public static void createFabricCompositionAfterAddingBom(List<Bill_of_Material__c> bomList)
	{
		System.debug('Composition createFabricCompositionAfterAddingBom');
		Decimal fabricTotalWt = 0;
		Id fabricId = bomList[0].Fabric__c;
		Set<Id> yarnIds = new Set<Id>();
		Map<String, Decimal> yarnMap = new Map<String, Decimal>();
		Map<Id, List<Composition__c>> skuCompositionMap = new Map<Id, List<Composition__c>>();
		List<Composition__c> comList = new List<Composition__c>();
		Map<String, Decimal> constituentMap = new Map<String, Decimal>();
		Id bomWeftRecordTypeId = BillOfMaterialService.recordTypeByName(Constants.RT_BOM_WEFT);
		Id bomWarpRecordTypeId = BillOfMaterialService.recordTypeByName(Constants.RT_BOM_WARP);
		Id compFabricRecordTypeId = recordTypeByName(Constants.RT_COMPOSITION_SKU);

		for(Bill_of_Material__c bom : bomList)
		{
			Decimal yarnWt = 0;

			if(bom.RecordTypeId == bomWeftRecordTypeId)
			{
				if(bom.Weight__c == null)
				{
					bom.Weight__c = 0;
				}
				fabricTotalWt += bom.Weight__c;
				yarnWt += bom.Weight__c;
			}
			if(bom.RecordTypeId == bomWarpRecordTypeId)
			{
				if(bom.Final_Weight_After_Crimpage__c == null)
				{
					bom.Final_Weight_After_Crimpage__c = 0;
				}
				fabricTotalWt += bom.Final_Weight_After_Crimpage__c;
				yarnWt += bom.Final_Weight_After_Crimpage__c;
			}

			yarnMap.put(bom.Id+'-'+bom.Yarn__c,yarnWt);
			yarnIds.add(bom.Yarn__c);
		}

		if(yarnMap.size() > 0)
		{
			skuCompositionMap = getCompositionBySkuIds(yarnIds);

			for(Bill_of_Material__c bom : bomList)
			{
				if(skuCompositionMap.containsKey(bom.Yarn__c))
				{
					for(Composition__c com : skuCompositionMap.get(bom.Yarn__c))
					{
						Decimal num = 0;
						if(constituentMap.containsKey(com.Constituents__c))
						{
							num = constituentMap.get(com.Constituents__c);
						}
						num += (com.Composition_Percentage__c/100) * yarnMap.get(bom.Id+'-'+bom.Yarn__c)/fabricTotalWt * 100;
						constituentMap.put(com.Constituents__c,num);
					}
				}
			}

			for(String constituent : constituentMap.keySet())
			{
				Composition__c com = new Composition__c(SKU__c = fabricId, Constituents__c = constituent);
				com.Composition_Percentage__c = constituentMap.get(constituent);
				com.Quality_Master__c = null;
				com.RecordTypeId = compFabricRecordTypeId;
				comList.add(com);
			}

			if(comList.size() > 0)
			{
				insert comList;
			}
		}
	}

	public static void createValueAddComposition(List<Product2> skuList)
	{
		Map<Id,Id> valueAddIdFabricIdMap = new Map<Id,Id>();
		Map<Id, List<Composition__c>> fabricCompositionMap = new Map<Id, List<Composition__c>>();
		Set<Id> fabricIds = new Set<Id>();
		List<Composition__c> compList = new List<Composition__c>();
		Id compSKURecordTypeId = recordTypeByName(Constants.RT_COMPOSITION_SKU);
		
		for(Product2 sku : skuList)
		{
			valueAddIdFabricIdMap.put(sku.Id,sku.Base_Fabric__c);
			fabricIds.add(sku.Base_Fabric__c);
		}	
		
		if(valueAddIdFabricIdMap.size() > 0)
		{
			fabricCompositionMap = getCompositionBySkuIds(fabricIds);

			for(Id valueAddedId : valueAddIdFabricIdMap.keySet())
			{
				if(fabricCompositionMap.containsKey(valueAddIdFabricIdMap.get(valueAddedId)))
				{
					for(Composition__c comp : fabricCompositionMap.get(valueAddIdFabricIdMap.get(valueAddedId)))
					{
						Composition__c newComp = comp;
						newComp.Id = null;
						newComp.Quality_Master__c = null;
						newComp.SKU__c = valueAddedId;
						newComp.RecordTypeId = compSKURecordTypeId;
						compList.add(newComp);
					}
				}
			}

			if(compList.size() > 0)
			{
				insert compList;
			}
		}

	}

	public static void createYarnComposition(List<Product2> skuList)
	{
		Map<Id,Id> yarnIdQualityIdMap = new Map<Id,Id>();
		Map<Id, List<Composition__c>> qualityCompositionMap = new Map<Id, List<Composition__c>>();
		Set<Id> qualityIds = new Set<Id>();
		List<Composition__c> compList = new List<Composition__c>();
		Id compSKURecordTypeId = recordTypeByName(Constants.RT_COMPOSITION_SKU);
		
		for(Product2 sku : skuList)
		{
			yarnIdQualityIdMap.put(sku.Id,sku.Quality_Name__c);
			qualityIds.add(sku.Quality_Name__c);
		}	
		
		if(yarnIdQualityIdMap.size() > 0)
		{
			qualityCompositionMap = getCompositionByQualityIds(qualityIds);

			for(Id yarnId : yarnIdQualityIdMap.keySet())
			{
				if(qualityCompositionMap.containsKey(yarnIdQualityIdMap.get(yarnId)))
				{
					for(Composition__c comp : qualityCompositionMap.get(yarnIdQualityIdMap.get(yarnId)))
					{
						Composition__c newComp = comp;
						newComp.Id = null;
						newComp.Quality_Master__c = null;
						newComp.SKU__c = yarnId;
						newComp.RecordTypeId = compSKURecordTypeId;
						compList.add(newComp);
					}
				}
				else
				{
					for(Product2 sku : skuList)
					{
						if(sku.Id == yarnId)
						{
							sku.addError('No compositions defined for selected quality in Quality Master, please define the composition first.');
							System.debug('Error for yarn, no quality defined');
						}
					}
				}
			}

			if(compList.size() > 0)
			{
				insert compList;
			}
		}

	}
}