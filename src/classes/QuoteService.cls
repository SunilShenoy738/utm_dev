public class QuoteService {
    
    public static List<Quote> getQuotesByOpportunityIds(Set<Id> oppIds)
    {
        return [Select Id, OpportunityId, Status from Quote where OpportunityId =:oppIds];
    }

    public static List<Quote> getQuotesAndLineItemsByOpportunityId(Id oppId) {
        return [Select Id, Name, Status, PriceBook2Id, TotalPrice, OpportunityId, Opportunity.Name, Account.Name, 
                AccountId, QuoteNumber, GrandTotal, 
                (Select Id, UnitPrice, Quantity, PriceBookEntryId, Product2Id, Agent_Commission_Percentage__c, Agent_Commission_in_Rs__c, 
                Cost_Type__c, Freight__c, Net_Profit__c, Rate_Type__c, Rate__c, Total__c
                from QuoteLineItems) 
                from Quote where OpportunityId =: oppId Order By LastModifiedDate Desc];
    }

    public static Quote getQuoteAndQuoteLineItemsByQuoteId(Id quoteId) {
        return [Select Id, Name, Status, PriceBook2Id, TotalPrice, OpportunityId, Opportunity.Name, Account.Name, 
                AccountId, QuoteNumber, GrandTotal,
                (Select Id, UnitPrice, Quantity, PriceBookEntryId, Product2Id, Agent_Commission_Percentage__c, Agent_Commission_in_Rs__c, 
                Cost_Type__c, Freight__c, Net_Profit__c, Rate__c, Rate_Type__c, Total__c
                from QuoteLineItems) 
                from Quote where Id =: quoteId];
    }

    public static Map<Id, Quote> getQuotesMap (Set<Id> ids) {
        Map<Id, Quote> res = new Map <Id, Quote> ();
        for (Quote obj : [Select Id,  Minimum_Quantity_Per_Shipment__c from Quote where Id in : ids]) {
          res.put (obj.Id, obj);
        }
        return res;
    }
}