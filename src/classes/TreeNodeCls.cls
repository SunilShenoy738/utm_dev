public class TreeNodeCls {
   @AuraEnabled public static UserRole[] getUserRoles() {
        return [SELECT Name, ParentRoleId,Id FROM UserRole];
    }
}