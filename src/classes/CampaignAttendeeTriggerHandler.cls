public class CampaignAttendeeTriggerHandler {
   
    public static void checkForDuplicate(List<Campaign_Attendee__c> newList){
        
        Map<Id, Campaign_Attendee__c> caMap = new Map<Id, Campaign_Attendee__c>();
        
        for(Campaign_Attendee__c caToAdd : [Select Id, User__c, Campaign__c from Campaign_Attendee__c]) {
            caMap.put(caToAdd.Id, caToAdd);            
        }
        System.debug('map for user**' + caMap);
        
        for(Campaign_Attendee__c ca : newList){
            for(Campaign_Attendee__c i : caMap.values()){
                 if(ca.User__c == i.User__c && ca.Campaign__c == i.Campaign__c){
                     ca.User__c.addError('You cannot add duplicate User to the same Campaign.');
                 }
            }           
        }
    }
}