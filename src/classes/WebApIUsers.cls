public class WebApIUsers{

public static HttpResponse GetUsers() {
    Http http = new Http();
    HttpRequest request = new HttpRequest();
    request.setEndpoint('http://mysqltestweb.azurewebsites.net/api/users');
    request.setMethod('GET');
    HttpResponse response = http.send(request);
    // If the request is successful, parse the JSON response.
    if (response.getStatusCode() == 200) {
    system.debug(response.getBody());
        /*
        // Deserializes the JSON string into collections of primitive data types.
        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        // Cast the values in the 'tickets' key as a list
        List<Object> tickets = (List<Object>) results.get('tickets');
        System.debug('Received the following tickets:');
        for (Object ticket: tickets) {
            System.debug(ticket);
        }
        */
    }
    return response;
}

}