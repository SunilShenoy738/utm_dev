// Created By Baibahv
public class OpportunityLineItemService
{
public  static List<OpportunityLineItem> getOpportunityLineItemByOpportunityId(Set<id> Oppid)
{
    List<OpportunityLineItem> ItemList=new List<OpportunityLineItem>();

    ItemList=[SELECT Discount,ListPrice,Name,OpportunityId,ServiceDate,PricebookEntryId,Product2Id,ProductCode,Quantity,TotalPrice,UnitPrice,Adjustment__c,Agent_Commission_in_Rs__c,Agent_Commission_Percentage__c,Cost_Type__c,Freight__c,Net_Profit__c,Rate__c,Rate_Type__c,Total__c FROM OpportunityLineItem where OpportunityId in:oppid];
  
    return ItemList;  

}
 //Added by baibhav
public  static Map<id,list<OpportunityLineItem>> getMapOpportunityLineItemByOpportunityId(Set<id> Oppid)
{
    List<Opportunity> oppList = [Select id,name  from Opportunity where id IN:Oppid];

    Map<id,list<OpportunityLineItem>> opplinMap = new Map<id,list<OpportunityLineItem>>();
    for(Opportunity op: oppList)
    {

    List<OpportunityLineItem> ItemList=new List<OpportunityLineItem>();
    
    ItemList=[SELECT Discount,ListPrice,Name,OpportunityId,PricebookEntryId,Product2Id,ProductCode,Quantity,TotalPrice,UnitPrice,Adjustment__c,Agent_Commission_in_Rs__c,Agent_Commission_Percentage__c,Cost_Type__c,Freight__c,Net_Profit__c,Rate__c,Rate_Type__c,Total__c FROM OpportunityLineItem where OpportunityId =:op.id];
  
    opplinMap.put(op.id,ItemList);
    }
    return opplinMap;  

}
}