public class OrderItemProcessWrapper {
    @AuraEnabled
    public string JobId{get;set;}

    @AuraEnabled
    public string OrderItemId{get;set;}
    
    @AuraEnabled
    public string OrderNumber{get;set;}

     @AuraEnabled
    public string OrderId{get;set;}
    
    @AuraEnabled
    public string UnitOfMeasurement{get;set;}
    
    @AuraEnabled
    public string ProductId{get;set;}
    
     @AuraEnabled
    public INTEGER Quantity{get;set;}
    
    @AuraEnabled
    public string SKU{get;set;}
    
    @AuraEnabled
    public string Remarks{get;set;}
    
    @AuraEnabled
    public string CustomerSKUProcess{get;set;}
    
    @AuraEnabled
    public string SKUMasterProcess{get;set;}
    
    @AuraEnabled
    public INTEGER ProcessNumber{get;set;}
    
    @AuraEnabled
    public Boolean IsSelected{get;set;}
    
    @AuraEnabled
    public DATE DeliveryDate{get;set;}
    
    @AuraEnabled
    public string RemarksNew{get;set;}
    
    @AuraEnabled
    public INTEGER AssignQty{get;set;}
    
    @AuraEnabled
    public Decimal Rate{get;set;}
    
    @AuraEnabled
    public List<SKUSupplier> Supplier{get;set;}
    
    @AuraEnabled
    public string SelectedSupplier{get;set;}
    
    @AuraEnabled
    public string JobOption{get;set;}
    
    @AuraEnabled
    public string Input{get;set;}
    
    @AuraEnabled
    public string Output{get;set;}
    
    @AuraEnabled
    public string MillRemarks{get;set; }
       
    @AuraEnabled
    public string JobNumber{get;set;}

    @AuraEnabled
    public string Status{get;set;}

    @AuraEnabled
    public Date DueDate{get;set;}

}