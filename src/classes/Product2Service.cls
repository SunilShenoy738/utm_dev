public class Product2Service {

	public static Product2 getProductById(Id skuId)
	{
		return [Select Id, Name, RecordType.Name, Family, Base_Fabric__c, Base_Fabric__r.Name, Base_Fabric__r.Family, Weave_Type__c From Product2 Where Id =: skuId ];
	}

	public static Id recordTypeByName(String rtName) {
        return Schema.SObjectType.Product2.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
    }

	public static Map<Id,Product2> getAllProductByIds(Set<Id> skuIds)
	{
		Map<Id,Product2> skuMap = new Map<Id,Product2>([Select Id, Name, RecordType.Name, Quality_Name__c From Product2 Where Id =: skuIds]);
		return skuMap;
	}
	
	public static Pricebook2 getStandardPriceBook()
	{
		return [select Id, Name from PriceBook2 where isStandard = true limit 1];
	}

	public static Map<Id,PricebookEntry> getPricebookEntriesMapBySkuIds(Set<Id> skuIds)
	{
		Map<Id,PricebookEntry> pbeMap = new Map<Id,PricebookEntry>();
		Pricebook2 stdPricebook = getStandardPriceBook();

		for(PricebookEntry pbe : [Select Id, Product2Id From PricebookEntry Where Pricebook2Id =:stdPricebook.Id and isActive = true and Product2Id =: skuIds])
		{
			pbeMap.put(pbe.Product2Id, pbe);
		}
		return pbeMap;
	}
}