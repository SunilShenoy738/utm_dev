public class ErrorLogService {
    public static void insertError(Exception e) {
        Error_Log__c eLog = new Error_Log__c();
        eLog.Line_Number__c = String.valueOf(e.getLineNumber());
        eLog.Exception_Type__c = e.getTypeName().substring(0, e.getTypeName().length()-1);
        eLog.Message__c = e.getMessage().substring(0, e.getMessage().length()-1);
        eLog.Stack_Trace__c = e.getStackTraceString().substring(0, e.getStackTraceString().length()-1);
        
        insert eLog;
    }
}