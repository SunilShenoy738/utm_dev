public class GRNCreationController {
    public GRN__c newGRN {get;set;}
    public List<Purchase_Order__c> supplierPOsinVF {get;set;}
    public List<Purchase_Order__c> supplierPOs {get;set;}
    public Map<Id, List<GRN_Item__c>> poIdGrnSkuMap {get;set;}
    public Map<Id, Purchase_Order_Item__c> orderItemMap {get;set;}
    public Integer noOfOrderItems {get;set;}
    public Integer noOfPOs {get;set;}
    public Boolean invtUnitAssigned {get;set;}
    public Id rawMatRecordTypeId {get; private set;}
    public Id fabricRecordTypeId {get; private set;}
    public Id valueAddedRecordTypeId {get; private set;}
    public Integer batchNum;
    public Id selectedPOItem {get;set;}
    public Map<Id, List<Batch__c>> poBatchMap {get;set;}
    public Decimal totalQty {get;set;}

    public GRNCreationController(ApexPages.StandardController controller) {
        newGRN = new GRN__c();
        supplierPOsinVF = new List<Purchase_Order__c>();
        
        //Account invtUnit = AccountService.getInventoryUnitByName(UserService.getUserById(UserInfo.getUserId()).Inventory_Unit__c);
        //if(invtUnit == null) {
          //  invtUnitAssigned = false;
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Inventory Unit is not assigned, please contact your administrator.'));
        //} else {
          //  invtUnitAssigned = true;
           // newGRN.Inventory_Unit__c = invtUnit.Id;
        //}
        
        fabricRecordTypeId = Product2Service.recordTypeByName(Constants.RT_PRODUCT2_FABRIC);
        valueAddedRecordTypeId =  Product2Service.recordTypeByName(Constants.RT_PRODUCT2_VALUE_ADDED);


        
    }
    
    public void getAllSupplierPOs() {
        System.debug('Test Message' + newGRN.Supplier_Vendor__c);
        System.debug('getAllSupplierPOs method entry');
        poIdGrnSkuMap = new Map<Id, List<GRN_Item__c>>();
        orderItemMap = new Map<Id, Purchase_Order_Item__c>();
        supplierPOsinVF.clear();
        if(newGRN.Supplier_Vendor__c != null) {
            supplierPOs = PurchaseOrderService.getPOsWithLineItemsByAccountId(newGRN.Supplier_Vendor__c);

            System.debug('SupplierPos: '+supplierPOs);
            noOfPOs = supplierPOs.size();
            
            if(supplierPOs.size() > 0) {
                GRN_Item__c grnSKU;
                for(Purchase_Order__c po : supplierPOs) {
                    System.debug('po line items: '+po.Purchase_Order_Items__r);
                    System.debug('po line items size: '+po.Purchase_Order_Items__r.size());
                    if(po.Purchase_Order_Items__r.size() > 0) {
                        supplierPOsinVF.add(po);
                        for(Purchase_Order_Item__c oi : po.Purchase_Order_Items__r) {
                            orderItemMap.put(oi.Id, oi);
                            
                            grnSKU = new GRN_Item__c();
                            grnSKU.Order_Item__c = oi.Id;
                            grnSKU.Order__c = po.Id;
                            grnSKU.SKU__c = oi.SKU__c;
                            if(grnSKU.SKU__c != null) {
                                grnSKU.SKU__r = oi.SKU__r;
                               grnSKU.SKU__r.RecordTypeId = oi.SKU__r.RecordTypeId;   
                            }
                            grnSKU.Unit_Of_Measurement__c = oi.Unit_Of_Measurement__c;
                            grnSKU.Ordered_Quantity__c = oi.Quantity__c;
                            grnSKU.Yarn_State__c = oi.Yarn_State__c;

                            if(poIdGrnSkuMap.containsKey(po.Id)) {
                                poIdGrnSkuMap.get(po.Id).add(grnSKU);
                            } else {
                                poIdGrnSkuMap.put(po.Id, new List<GRN_Item__c> {grnSKU});    
                            }
                            
                        }
                    }    
                }
                
                if(!poIdGrnSkuMap.isEmpty()) {
                    noOfOrderItems = 0;
                    for(List<GRN_Item__c> gSkuList : poIdGrnSkuMap.values()) {
                        noOfOrderItems += gSkuList.size();
                    }
                }
            }
            System.debug('poIdGrnSkuMap: '+poIdGrnSkuMap);
        }
        else{
            supplierPOs.clear();
            supplierPOsinVF.clear();
        }
    }

    public void showBatchList() {

        System.debug('selectedPOItem: '+selectedPOItem);
        System.debug('poBatchMap: '+poBatchMap);
        if(poBatchMap == null || !poBatchMap.containsKey(selectedPOItem) || poBatchMap.get(selectedPOItem).size() == 0)
        {
            poBatchMap = new Map<Id, List<Batch__c>>();
            System.debug('batch creation');
            batchNum = 1;
            List<Batch__c> batchList = new List<Batch__c>();
            Batch__c batch = new Batch__c(Name=orderItemMap.get(selectedPOItem).Name+'\\'+batchNum,Unit_Of_Measurement__c=orderItemMap.get(selectedPOItem).Unit_Of_Measurement__c);
            batchList.add(batch);
            poBatchMap.put(selectedPOItem, batchList);
            totalQty = 0;
        }
    }

    public void addBatch() {
    	if(poBatchMap.containsKey(selectedPOItem))
    	{
    		if(poBatchMap.get(selectedPOItem).size() > 0) 
    		{
				batchNum = poBatchMap.get(selectedPOItem).size() + 1;
	    		Batch__c batch = new Batch__c(Name=orderItemMap.get(selectedPOItem).Name+'\\'+batchNum,Unit_Of_Measurement__c=poBatchMap.get(selectedPOItem)[0].Unit_Of_Measurement__c);
	            batch.Quantity__c = poBatchMap.get(selectedPOItem)[0].Quantity__c;
                poBatchMap.get(selectedPOItem).add(batch);
                calculateBatchTotal();
	        }
	        else{
	        	showBatchList();
	        }
    	}
    	else{
    		System.debug('error: addBatch: poBatchMap ');
    	}
    }

    public void removeBatch() {
    	if(poBatchMap.containsKey(selectedPOItem))
    	{
    		Integer batchSize = poBatchMap.get(selectedPOItem).size() - 1;
    		poBatchMap.get(selectedPOItem).remove(batchSize);
            calculateBatchTotal();
    	}
    	else{
    		System.debug('error: removeBatch: poBatchMap ');
    	}
    }

    public void calculateBatchTotal() {
        totalQty = 0;
        System.debug('Data :' + poIdGrnSkuMap);
        if(poBatchMap.containsKey(selectedPOItem))
        {
            for(Batch__c bt : poBatchMap.get(selectedPOItem)) 
            {
                if(bt.Quantity__c == null)
                {
                    bt.Quantity__c = 0;
                }
                totalQty += bt.Quantity__c;
            }
            for(Id poId : poIdGrnSkuMap.keySet()) 
            {                
                for(GRN_Item__c gi : poIdGrnSkuMap.get(poId))
                {
                    if(gi.Order_Item__c == selectedPOItem)
                    {
                        gi.Received_Quantity__c = totalQty;
                    }
                }
            }
           
        }
        else{
            totalQty = 0;
        }
    }


    public PageReference saveGRN() {
        System.debug('saveGRN');
        try {
            if (newGRN.Location__c == null) {
                newGRN.Location__c.addError ('Location is required');
                return null;
                
            }
            List<Purchase_Order_Item__c> orderItemsToUpdate = new List<Purchase_Order_Item__c>();
            insert newGRN; 

            List<GRN_Item__c> grnSkusToInsert = new List<GRN_Item__c>();
            List<Batch__c> batchesToInsert = new List<Batch__c>();

            for(List<GRN_Item__c> gSkuList : poIdGrnSkuMap.values()) {
                for(GRN_Item__c gSku : gSkuList) {
                    if(gSku.SKU__c != null && gSku.SKU__r != null && gSku.Received_Quantity__c != null) {
                        gSku.GRN__c = newGRN.Id;
                        grnSkusToInsert.add(gSku); 
                        
                        Purchase_Order_Item__c orderItem = orderItemMap.get(gSku.Order_Item__c);
                        System.debug('orderItem: '+orderItem);
                        if(orderItem != null) {
                        	System.debug('gSku.Received_Quantity__c: '+gSku.Received_Quantity__c);

                            if(gSku.Received_Quantity__c != null) {
                            	System.debug('orderItem.Received_Quantity__c: '+orderItem.Received_Quantity__c);
                                if(orderItem.Received_Quantity__c == null) {
                                    orderItem.Received_Quantity__c = gSku.Received_Quantity__c;
                                } else {
                                    orderItem.Received_Quantity__c += gSku.Received_Quantity__c;
                                }    
                            }

                            if(gSku.Complete__c == true || gSku.Received_Quantity__c >= orderItem.Quantity__c || orderItem.Received_Quantity__c >= orderItem.Quantity__c)
                            {
                            	orderItem.Status__c = Constants.ORDER_ITEM_STATUS_COMPLETED;
                            }
                            orderItemsToUpdate.add(orderItem);
                        }
                    }
                }
            }
            
            try {
                
                insert grnSkusToInsert;

                System.debug('grnSkusToInsert: '+grnSkusToInsert);
                
                for(GRN_Item__c gi : grnSkusToInsert)
                {
                	System.debug('gi: '+gi);
                	if(poBatchMap!= null && poBatchMap.containsKey(gi.Order_Item__c))
                	{
                		for(Batch__c bt : poBatchMap.get(gi.Order_Item__c))
                		{
                			if(bt.Quantity__c != null && bt.Quantity__c != 0)
                			{
                				bt.GRN_Item__c = gi.Id;
                				batchesToInsert.add(bt);
                			}
                		}
                	}
                }

                if(batchesToInsert.size() > 0)
                {
                	insert batchesToInsert;
                }
                
                System.debug('update orderItemsToUpdate: '+orderItemsToUpdate);
                update orderItemsToUpdate;
                
                PageReference pg = new PageReference('/'+newGRN.Id);
                return pg;
            } catch(Exception e) {
                ErrorLogService.insertError(e);
            }
        } catch(Exception e) {
            ErrorLogService.insertError(e);
        }
        return null;
    }
}