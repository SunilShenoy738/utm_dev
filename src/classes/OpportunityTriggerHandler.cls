public class OpportunityTriggerHandler 
{
	 public static void onAfterInsert(List<Opportunity> newList) 
     {
        List<Opportunity> oppList = new List<Opportunity>();
        for(Opportunity opp : newList) {
            if(opp.StageName == Constants.OPPORTUNTIY_STAGE_CLOSED_WON ) {
                oppList.add(opp);
            }
        }
        
        if(oppList.size() > 0) {
            createOrder(oppList);
        }
    }
    
    public static void onAfterUpdate(List<Opportunity> newList, Map<Id,Opportunity> oldMap) 
    {
        List<Opportunity> oppList = new List<Opportunity>();
        Set<Id> oppIds = new set<Id>();
        for(Opportunity opp : newList) {
            if(oldMap.get(opp.Id).StageName != Constants.OPPORTUNTIY_STAGE_CLOSED_WON && opp.StageName == Constants.OPPORTUNTIY_STAGE_CLOSED_WON) {
                oppList.add(opp);
                oppIds.add(opp.id);
            }
        } 
        
        if(oppList.size() > 0) {
            createOrder(oppList);
        }
              
    }
    
    public static void onBeforeInsert(List<Opportunity> newList)
    {
        setStandardPriceBookId(newList);
    }
    
    public static void setStandardPriceBookId(List<Opportunity> newList)
    {
        PriceBook2 standardPriceBook = Product2Service.getStandardPriceBook();
        for(Opportunity opp : newList)
        {
            opp.PriceBook2Id = standardPriceBook.Id;
        }
    }
    
    public static void createOrder(List<Opportunity> oppList) {
        List<Order> orderInsertList = new List<Order>();
       
        String recid=Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.RT_ORDER_SALES_ORDER).getRecordTypeId();
        
        for(Opportunity opp : oppList) 
        {
            Order neworder = new Order();
            newOrder.OpportunityId = opp.Id;
            newOrder.Status = Constants.ORDER_STATUS_DRAFT;
            newOrder.AccountId = opp.AccountId;
            newOrder.EffectiveDate = System.today();
            newOrder.QuoteId = opp.SyncedQuoteId;
            newOrder.Pricebook2Id = opp.Pricebook2Id;

            // Order Recordtypeid as Sales Order
           
            newOrder.RecordTypeId = recid;
            newOrder.CurrencyISOCode = opp.CurrencyISOCode;
            orderInsertList.add(newOrder);       
        }
        
        if(orderInsertList.size() > 0) {
            try {        
                insert orderInsertList;
                
                if(Test.isRunningTest()) {
                    integer c = 1/0;
                }    
            } catch(Exception e) {
                System.debug('Order insertion error Error message '+e);
                orderInsertList[0].addError(e.getMessage());
            }
        }
    }





}