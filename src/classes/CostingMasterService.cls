public class CostingMasterService {
	
	public static CostingMaster__c getCostingMasterDetailsById(Id weaveType)
	{
		List<CostingMaster__c> cmList = [Select Id, Name, Express_Conversion_Cost__c, Standard_Conversion_Cost__c, Freight__c From CostingMaster__c Where Id =:weaveType];
		if(cmList.size() > 0)
		{
			return cmList[0];
		}
		else
			return null;
		return null;
	}
    
    /*
     * Method to return the details of the costingMaster__c Data
     * */
    public static Map<Id, CostingMaster__c> getCostingMasterDetails (Set<Id> costingMasterIds) {
        Map<Id, CostingMaster__c> res = new Map<Id, CostingMaster__c> ([SELECT Id, Name, Code__c, Last_series_number__c  from CostingMaster__c where id in : costingMasterIds]);
        return res;
    }
}