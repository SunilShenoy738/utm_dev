public class PODocExtension {
    
    private final Purchase_Order__c po{get;set;}
    private final ID supId{get;set;}
    public List<Purchase_Order_Item__c> poitmList{get;set;}
    public List<WrapperPurchaseOrderItems> wrapPoitmList{get;set;}
    
    public PODocExtension(ApexPages.StandardController stdController) {
        this.po = (Purchase_Order__c)stdController.getRecord();
        System.debug('po record***' + po);
        this.supId = po.Supplier__c;
        System.debug('supID***' + supId);
        
        poitmList =  [Select Id, Name, SKU__r.Description, Quantity__c, SKU__r.QuantityUnitOfMeasure, SKU__r.Cost_Per_Kg__c
                      from Purchase_Order_Item__c where Purchase_Order__r.id =: po.id];
        System.debug('po item list' + poitmList);
        
        wrapPoitmList = new List<WrapperPurchaseOrderItems>();
        for(Purchase_Order_Item__c potm : poitmList ) {                                                
                                                
            WrapperPurchaseOrderItems wrap = new WrapperPurchaseOrderItems();
            wrap.poitm = potm;
            System.debug('potm.Quantity__c: '+ potm.Quantity__c);
            System.debug('potm.SKU__r.Cost_Per_Kg__c: '+ potm.SKU__r.Cost_Per_Kg__c);
            try
            {
                wrap.lineTotal = (potm.Quantity__c * potm.SKU__r.Cost_Per_Kg__c);
            }
            catch(NullPointerException ex) {
                potm.SKU__r.Cost_Per_Kg__c = 1;
                wrap.lineTotal = (potm.Quantity__c * potm.SKU__r.Cost_Per_Kg__c);
            }                
            System.debug('lineTotal '+  wrap.lineTotal);
            System.debug('wrap obj: ' + wrap);
            wrapPoitmList.add(wrap);  
            System.debug('wrap obj: ' + wrap);
        }
        System.debug('wraplist' + wrapPoitmList);
        
    }
    
    public class WrapperPurchaseOrderItems {
    
        public Purchase_Order_Item__c poitm{get;set;}
        public Decimal lineTotal {get;set;}

    }
    
  /*  public Account getVendor() {
        System.debug('CompID***' + compId);
        Account ven = [Select id, name, recordtype.name, 
                       BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
                       Billing_Phone__c, Email__c 
                      from Account where recordtype.name = 'Vendor' limit 1];
        return ven;
    } */
    
     public Account getCompany() {
        Account comp = [Select id, name, recordtype.name, 
                       ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                       Shipping_Phone__c, Email__c 
                      from Account where recordtype.name = 'Company' limit 1];
         System.debug('comp record : '+comp);
        return comp;
    }
    
  /*    public Account getSupplier() {
        Account sup = [Select id, name, recordtype.name, 
                       ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
                       Shipping_Phone__c, Email__c 
                      from Account where recordtype.name = 'Supplier' AND Id =: po.Supplier__c limit 1];
        supId = sup.Id; 
        return sup;
    } 
    
    public Decimal getLineTotal() {
        Purchase_Order_Item__c poitm = [Select Id, Quantity__c, SKU__r.Cost_Per_Kg__c 
                                        from Purchase_Order_Item__c limit 1];
        Decimal quantity = poitm.Quantity__c;
        Decimal unitPrice = poitm.SKU__r.Cost_Per_Kg__c;
        
        Decimal lineTotal = quantity * unitPrice;
        
        return lineTotal;
       

    } */

}