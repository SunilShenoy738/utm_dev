public class OpportunityLineItemTriggerHandler {
    
    public static void onBeforeInsert(List<OpportunityLineItem> newList)
    {
        //Sync quote line item  custom fields to Opp line item custom fields
        syncQuoteLineItemToOppLineItem(newList);
    }

    public static void syncQuoteLineItemToOppLineItem(List<OpportunityLineItem> oliList) {
        
        Set<Id> oppIdSet = new Set<Id>();
        Map<String,QuoteLineItem> qliMap = new Map<String,QuoteLineItem>();
        List<QuoteLineItem> qliList = new List<QuoteLineItem>();
        
        for(OpportunityLineitem oli : oliList) {
            oppIdSet.add(oli.OpportunityId);
        }
        
        qliList = QuoteLineItemService.getQliFromOpportunityIds(oppIdSet);
        System.debug('qliList: '+qliList);
        
        if(qliList.size() > 0) {
            for(QuoteLineItem qli : qliList) {
                qliMap.put(qli.Quote.OpportunityId+':'+qli.Product2Id+':'+qli.Quantity+':'+qli.UnitPrice, qli);
            }
            
            for(OpportunityLineItem oli : oliList) {
                if(qliMap.containsKey(oli.OpportunityId+':'+oli.Product2Id+':'+oli.Quantity+':'+oli.UnitPrice)){
                    QuoteLineItem qli = qliMap.get(oli.OpportunityId+':'+oli.Product2Id+':'+oli.Quantity+':'+oli.UnitPrice);
                    oli.Agent_Commission_Percentage__c = qli.Agent_Commission_Percentage__c;
                    oli.Agent_Commission_in_Rs__c = qli.Agent_Commission_in_Rs__c;
                    oli.Cost_Type__c = qli.Cost_Type__c;
                    oli.Freight__c = qli.Freight__c;
                    oli.Net_Profit__c = qli.Net_Profit__c;
                    oli.Rate__c = qli.Rate__c;
                    oli.Rate_Type__c = qli.Rate_Type__c;
                    oli.Total__c = qli.Total__c;              
                }
            }
        }
    }
}