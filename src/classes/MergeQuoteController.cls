public class MergeQuoteController {
    public List<Quote> relatedQuotes {get;set;}
    public List<QuoteWrapper> relatedQuotesWrap {get; set;}
    public Id oppId {get;set;}
    public Quote mergedQuote {get;set;}
    public Id priceBookId {get;set;}
    public String quoteName {get;set;}
    public MergeQuoteController() {
        oppId = ApexPages.currentPage().getParameters().get('oppId');
        
        if(oppId == null) {
            return;
        }
        
        relatedQuotes = QuoteService.getQuotesAndLineItemsByOpportunityId(oppId);
        
        if(relatedQuotes.size() > 0) {
            relatedQuotesWrap = new List<QuoteWrapper>();
            QuoteWrapper qw;
            for(Quote qt : relatedQuotes) {
                if(priceBookId == null) {
                    priceBookId = qt.PriceBook2Id;
                }
                if(quoteName == null) {
                    quoteName = qt.Opportunity.Name;
                }
                qw = new QuoteWrapper(qt);
                relatedQuotesWrap.add(qw);
            }
        }
    }
    
    public PageReference mergeQuotes() {
        mergedQuote = new Quote();
        mergedQuote.Status = Constants.QUOTE_STATUS_DRAFT;
        mergedQuote.OpportunityId = oppId;
        mergedQuote.Name = quoteName;
        mergedQuote.Pricebook2Id = priceBookId;
        insert mergedQuote;
        
        Map<Id, QuoteLineItem> skuIdToQliMap = new Map<Id, QuoteLineItem>();
        for(QuoteWrapper qw : relatedQuotesWrap) {
            if(qw.isSelected) {
                for(QuoteLineItem qli : qw.quo.QuoteLineItems) {
                    if(!skuIdToQliMap.containsKey(qli.Product2Id)) {
                        skuIdToQliMap.put(qli.Product2Id, qli);
                    }
                }
            }
        }

        List<QuoteLineItem> relatesQLIs = new List<QuoteLineItem>();
        QuoteLineItem relatesQLI;
        for(QuoteLineItem qli : skuIdToQliMap.values()) {
            relatesQLI = qli.clone(false, true);
            relatesQLI.QuoteId = mergedQuote.Id;
            relatesQLIs.add(relatesQLI);
        }
        
        insert relatesQLIs;
        
        PageReference pg = new PageReference('/'+mergedQuote.Id);
        return pg;
    }
    
    public PageReference cancelMerge() {
        PageReference pg = new PageReference('/'+oppId);
        return pg;
    }
    
    public class QuoteWrapper {
        public Quote quo {get;set;}
        public Boolean isSelected {get;set;}
        
        public QuoteWrapper(Quote quo) {
            this.quo = quo;
            isSelected = false;
        }
    }
}