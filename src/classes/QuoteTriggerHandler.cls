public class QuoteTriggerHandler {
	
	//method to perform operations after update of Quote reocrds
    public static void onAfterUpdate(List<Quote> newList, Map<Id,Quote> oldMap) 
    {
       Map<Id, Id> quoteMap = new Map<Id, Id>();
       for(Quote currentQuote : newList)
       {
           if(currentQuote.status == Constants.QUOTE_STATUS_ACCEPTED && oldMap.get(currentQuote.id).status != Constants.QUOTE_STATUS_ACCEPTED) 
           {
               quoteMap.put(currentQuote.Id, currentQuote.OpportunityId);
           }
        }
        
        if(quoteMap.size() > 0)
        {
            syncQuote(quoteMap);
        }
    }
    
    public static void onBeforeInsert(List<Quote> newList)
    {
        setStandardPriceBookId(newList);
        setPaymentTerms (newList);
    }
    
    public static void setPaymentTerms (List<Quote> newList) {
        Set<Id> accountIdSet = new Set<Id> ();
        Set<Id> opportunityIdSet = new Set<Id> ();
        Map<Id, Account> accountPaymentTerms = new Map<Id, Account> ();
        Map<Id, Id> oppAccount = new Map<Id, Id> ();
        
        for (Quote qObj : newList) {
            if (qObj.OpportunityId != null) {
                opportunityIdSet.add(qObj.OpportunityId);
            }
        }

        if (opportunityIdSet.size() > 0) {
            oppAccount = OpportunityService.getOpportunityAccountId (opportunityIdSet);     
            for (Id idData : oppAccount.values ()) {
                    accountIdSet.add (idData);
            }
                 
                 
            if (accountIdSet.size() > 0) {
                accountPaymentTerms = AccountService.getAccountMapByIds (accountIdSet);
                if (accountPaymentTerms != null && accountPaymentTerms.size() > 0) {
                    for (Quote qObj : newList) {
                        if (oppAccount.containsKey(qObj.OpportunityId)) {
                            Account accObj = accountPaymentTerms.get(oppAccount.get(qObj.OpportunityId));
                            qObj.Advance__c = accObj.Advance__c;
                            qObj.Payment_Term__c = accObj.Payment_Term__c;
                            qObj.Number_of_days_after_Airway_Bill_date__c = accObj.Number_of_days_after_Airway_Bill_date__c;
                            qObj.Number_of_days_after_shipment_date__c = accObj.Number_of_days_after_shipment_date__c;
                            qObj.Days_after_first_payment_post_shipment__c = accObj.Days_after_first_payment_post_shipment__c;
                            qObj.Days_after_second_payment_post_shipment__c = accObj.Days_after_second_payment_post_shipment__c;
                            qObj.Days_after_goods_receipt__c = accObj.Days_after_goods_receipt__c;
                            qObj.Before_Shipment__c = accObj.Before_Shipment__c;
                            qObj.Percentage_first_payment__c = accObj.Percentage_first_payment__c;
                            qObj.Percentage_second_payment__c = accObj.Percentage_second_payment__c;
                            qObj.Minimum_Quantity_Per_Shipment__c = accObj.Minimum_Quantity_Per_Shipment__c;
                            qObj.Courier_Account_Type__c = accObj.Courier_Account_Type__c;
                            qObj.Courier_Account_Number__c = accObj.Courier_Account_Number__c;
                        } else {
                            qObj.addError ('Account doesn\'t exist !');
                        }                    
                    }    
                }            
            }
        }
    }
    
    public static void setStandardPriceBookId(List<Quote> newList)
    {
        PriceBook2 standardPriceBook = Product2Service.getStandardPriceBook();
        for(Quote quo : newList)
        {
            quo.PriceBook2Id = standardPriceBook.Id;
        }
    }
    
    //method to sync Quote with Opportunity
    //future method is used because syncQuote cannot be done in same transaction
    @future
    public static void syncQuote(Map<Id, Id> quoteMap) {
        System.debug('future method');
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        List<Quote> quotesToReject = new List<Quote>();
       
        try {
            //Use below code if there is any custom field syncing 
            //List<Quote> quoteLst=[select id,Contract_Nature__c,Payment_Term__c,Deposit_If_Payment_Term_is_AP_or_DP__c,Reference_Date_If_DA__c,Days_from_Reference_Date_If_DA__c from quote where id in:quoteMap.keyset()];
            //Map<Id, Quote> idToQuoteMap = new Map<Id, Quote>();
            
            //for(Quote qt: quoteLst) {
              //  idToQuoteMap.put(qt.id,qt);
            //}
            
            for(Id currentQuote : quoteMap.keyset()) {
                Opportunity opp = new Opportunity();
                opp.Id = quoteMap.get(currentQuote);
                //Sample code below for custom field mapping dfor sync
                //opp.Contract_Nature__c=idToQuoteMap.get(currentQuote).Contract_Nature__c;
                opp.SyncedQuoteId = currentQuote;
                oppMap.put(opp.Id, opp);
            }
            
            
            if(oppMap.size() > 0) {
                update oppMap.values();

                /*
                 * Update the accepted quote Line item Id to the Account_SKU's
                 * Quote_Line_Item_ID__c field.
                 * */
                Map<id,list<QuoteLineItem>> quoteLineMap = 
                            QuoteLineItemService.getMapQuoteLineItemByQuoteId(quoteMap.keyset());
                List<Account_SKU__c> updateAccSKUList = new List<Account_SKU__c> ();
                Map<Id, Account_SKU__c> accountSKUMap = new Map<Id, Account_SKU__c> ();

                Set<Id> productIdSet = new Set<Id> ();
                for (list<QuoteLineItem> quoteLineItem : quoteLineMap.values()) {
                    for(QuoteLineItem item : quoteLineItem) {
                        productIdSet.add (item.Product2Id);
                    }
                }

                /*
                 *
                 * */
                accountSKUMap = AccountSKUService.getAccountSKUByProductId (productIdSet);


                for(Id currentQuote : quoteMap.keyset()) {
                    if(quoteLineMap.containsKey(currentQuote)) {
                        list<QuoteLineItem> quoteLineItems = quoteLineMap.get(currentQuote);
                        if (quoteLineItems.size () > 0) {
                            for (QuoteLineItem qlItem : quoteLineItems) {
                                if (!qlItem.Dont_Sync__c && accountSKUMap.containsKey(qlItem.Product2Id)) {
                                    updateAccSKUList.add (
                                    new Account_SKU__c (
                                        Id =  accountSKUMap.get(qlItem.Product2Id).Id, 
                                        Quote_Line_Item_ID__c = qlItem.Id,
                                        Minimum_Quantity_Per_Shipment__c = qlItem.Minimum_Quantity_Per_Shipment__c
                                        )
                                    );
                                }
                            }
                        }
                    }
                }

                if (updateAccSKUList.size() > 0) {
                    System.debug('updateAccSKUList : ' + updateAccSKUList);
                    update updateAccSKUList;
                }

            }


            
            //[Select Id,Status from Quote where OpportunityId IN :quoteMap.values() AND Id NOT IN :quoteMap.keySet()]
            
            for(Quote quote : QuoteService.getQuotesByOpportunityIds(oppMap.keySet())) 
            {
                if(!quoteMap.containsKey(quote.Id))
                {
                	quote.status = Constants.QUOTE_STATUS_REJECTED;
                	quotesToReject.add(quote);
                }
            }

            if(quotesToReject.size() > 0) {
                update quotesToReject;
            }
        } catch(Exception e) {
            System.debug('Exception found in QuoteTriggerhandler class: '+e.getMessage()+' '+e.getLineNumber());
            ErrorLogService.insertError(e);
        }
    }
}