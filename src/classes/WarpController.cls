public with sharing class WarpController {

    @AuraEnabled
    public static List<Warp_SKU__c> getWrapTemplate() {
       List<Warp_SKU__c>lst= new List<Warp_SKU__c>();
       return lst;
    }

 @AuraEnabled
    public static List<Product2> getYarnDetail(string yarnId){

        List<Product2> product= [SELECT  Id, Name,RecordType.Name FROM Product2 where id = :yarnId];
        if(product[0].RecordType.Name=='Grindles')
        {
            List<Composition__c> lstComp= [SELECT Yarn__c FROM Composition__c where SKU__c= :yarnId];
            List<Id> pIds= new List<Id>();
            for(Composition__c c : lstComp){pIds.add(c.Yarn__c);}
            List<Product2> lstProducts= [SELECT  Id, Name,RecordType.Name FROM Product2 where id IN :pIds];
            return lstProducts;
        }
        else
        {
            return product;
        }

    }


    @AuraEnabled
    public static string getStateofWarp(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Warp_SKU__c.Yarn_State__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        string res=JSON.serialize(ple);
        return res;
    }
    
    @AuraEnabled
    public static String saveWarp(string wrapSkuList, string wrapName, string epi, string reedWidth) {
        Warp_Master__c wm = new Warp_Master__c();
        wm.Name=wrapName;
        wm.EPI__c=Integer.valueOf(epi);
        wm.Reed_Width__c= Decimal.valueOf(reedWidth);
        //wm.Percentage_Wasted__c= Decimal.valueOf(per);
        insert wm;
         List<Warp_SKU__c> obj = (List<Warp_SKU__c>)JSON.deserialize(wrapSkuList, List<Warp_SKU__c>.class);
        for(Warp_SKU__c w : obj){
            w.Warp_Master__c=wm.Id;
        }
        insert obj;
     // List<Warp_SKU__c> customObject = (List<Warp_SKU__c>)JSON.deserialize(wrapSkuList, Sobject.class);
       system.debug('ss');
        system.debug(obj);
        return wm.Id;
    }
    

     @AuraEnabled
    public static List<Product2> getYarnList(){
        List<Product2> lstYarn= new List<Product2>();
        lstYarn= [SELECT  Id,Name,ProductCode,RecordType.Name FROM Product2 WHERE RecordType.Name ='Yarn' ];
        return lstYarn;
    }
    @AuraEnabled
    public static List<Asset> getAssetList(){
        List<Asset> lstAsset= new List<Asset>();
        lstAsset= [SELECT Id, Name FROM Asset ];
        return lstAsset;
    }

}