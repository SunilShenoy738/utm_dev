public class SKUProcessService {
    
    public static List<SKU_Process__c> getSKUProcessBySkuId(Id skuId)
    {
        return [Select Id, Name, Cost_Per_Kg__c, Cost_per_Mtr__c, Input_Stock_SKU__c, Shrinkage_Percentage__c, Input_Stock_SKU__r.Name, Output_Stock_SKU__c, Output_Stock_SKU__r.Name, SKU__c, SKU__r.RecordType.Name, Fabric_Width__c, Fabric_Wt_Mtr__c, Step__c, Process__c, Vendor__c, Vendor_Process__c, Process__r.Name, HasIO__c, Job_Work__c, Time_per_Process__c, Special_Comments__c, (Select Id, Name, Cost_Per_Kg__c, Cost_per_Mtr__c, Input_Stock_SKU__c, Shrinkage_Percentage__c, Input_Stock_SKU__r.Name, Output_Stock_SKU__c, Output_Stock_SKU__r.Name, SKU__c, Fabric_Width__c, Fabric_Wt_Mtr__c, Step__c, Process__c, Vendor__c, Vendor_Process__c, Process__r.Name, Time_per_Process__c, Special_Comments__c From Parent_SKU_Processes__r Order By Step__c) From SKU_Process__c Where SKU__c =: skuId and Parent_SKU_Process__c = null and hasIO__c = true];
    }

    public static List<SKU_Process__c> getSKUProcessBySkuIdWithoutIO(Id skuId)
    {
    	return [Select Id, Name, Cost_Per_Kg__c, Cost_per_Mtr__c, Input_Stock_SKU__c, Shrinkage_Percentage__c, Input_Stock_SKU__r.Name, Output_Stock_SKU__c, Output_Stock_SKU__r.Name, SKU__c, SKU__r.RecordType.Name, Fabric_Width__c, Fabric_Wt_Mtr__c, Step__c, Process__c, Vendor__c, Vendor_Process__c, Process__r.Name, HasIO__c, Job_Work__c, Time_per_Process__c, Special_Comments__c From SKU_Process__c Where SKU__c =: skuId and Parent_SKU_Process__c = null and hasIO__c = false];
    }
}