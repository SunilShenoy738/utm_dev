public class ExpenseTriggerHandler {
	   
    public static void onAfterUpdate (Map<Id, Expense__c> oldMap, List<Expense__c> newList) {
        Set<Id> campaignIdSet = new Set<Id> ();
        List<Campaign> updateList = new List<Campaign> ();

        for (Expense__c expObj : newList) {
            if (expObj.Campaign_Name__c != null) {
                campaignIdSet.add (expObj.Campaign_Name__c);
            }
        } 

        if (campaignIdSet.size() > 0) {
        	for (AggregateResult res : ExpenseService.getSumOfExpenseByCampaign (campaignIdSet)) {
        		updateList.add (new Campaign (Id = (Id)res.get ('campaignId'), 
        										ActualCost = (Decimal) res.get ('total')
        									)
        						);
        	}
        	if (updateList.size() > 0) {
        		update updateList;
        	}
        }
    }
}