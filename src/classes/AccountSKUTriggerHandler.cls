public class AccountSKUTriggerHandler {
	
	public static void onBeforeInsert(List<Account_SKU__c> newList)
	{
		autoPopulateFieldsFromAccount(newList);
	}

	public static void autoPopulateFieldsFromAccount(List<Account_SKU__c> accSKUList)
	{
		Set<Id> accIds = new Set<Id>();
		Map<Id,Account> accMap = new Map<Id,Account>();

		for(Account_SKU__c accSKU : accSKUList)
		{
			accIds.add(accSKU.Account__c);
		}

		accMap = AccountService.getAccountMapByIds(accIds);

		for(Account_SKU__c accSKU : accSKUList)
		{
			//populate Special Instructions from Account is not specified in Account SKU
			if(accSKU == null)
			{
				accSKU.Special_Instructions__c = accMap.get(accSKU.Account__c).Special_Instructions__c;
			}
		}
	}
}