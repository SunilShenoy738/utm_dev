public class CompositionTriggerHandler {
	
	public static void onAfterInsert(List<Composition__c> newList)
	{
		List<Composition__c> qualityCompList = new List<Composition__c>();
		Id compQualityRecordTypeId = CompositionService.recordTypeByName(Constants.RT_COMPOSITION_QUALITY);
		
		for(Composition__c comp : newList)
		{
			if(comp.Composition_Percentage__c > 100)
			{
				comp.addError('Composition Percentage cannot be greater than 100');
			}
			if(comp.Quality_Master__c != null && comp.RecordTypeId != compQualityRecordTypeId)
			{
				comp.addError('Please select the composition record type as Quality.');
			}
			else if(comp.Quality_Master__c != null && comp.RecordTypeId == compQualityRecordTypeId)
			{
				qualityCompList.add(comp);
			}
		}

		if(qualityCompList.size() > 0)
		{
			checkOverallCompositionPercentage(qualityCompList);
		}
	} 

	public static void onAfterUpdate(List<Composition__c> newList, Map<Id, Composition__c> oldMap)
	{
		List<Composition__c> compList = new List<Composition__c>();

		for(Composition__c comp : newList)
		{
			if(comp.Composition_Percentage__c == null)
				comp.Composition_Percentage__c = 0;
				
			if(oldMap.get(comp.Id).Composition_Percentage__c == null)
				oldMap.get(comp.Id).Composition_Percentage__c = 0; 
			
			if(comp.Composition_Percentage__c != oldMap.get(comp.Id).Composition_Percentage__c)
			{
				compList.add(comp);
			}
		}

		if(compList.size() > 0)
		{
			checkOverallCompositionPercentage(compList);
		}

	}

	public static void onAfterDelete(List<Composition__c> oldList)
	{
		checkOverallCompositionPercentage(oldList);
	}

	public static void checkOverallCompositionPercentage(List<Composition__c> compList)
	{
		Set<Id> qualityIds = new Set<Id>();
		Map<String, String> qualityCompMap = new Map<String, String>();
		
		for(Composition__c comp : compList)
		{
			qualityIds.add(comp.Quality_Master__c);
		}
		qualityCompMap = CompositionService.getTotalCompositionPercentageByQualityIds(qualityIds);

		for(Composition__c comp : compList)
		{
			if(qualityCompMap.containsKey(comp.Quality_Master__c) && Decimal.valueOf(qualityCompMap.get(comp.Quality_Master__c)) > 100)
			{
				comp.addError('Overall composition percentage is more than 100%');
			}
		}

		updateCompleteCheckboxOnQualityMaster(qualityCompMap);
	}

	public static void updateCompleteCheckboxOnQualityMaster(Map<String, String> qualityCompMap)
	{
		List<Quality_Master__c> updateQualityList = new List<Quality_Master__c>();

		for(String quality : qualityCompMap.keySet())
		{
			Quality_Master__c qm = new Quality_Master__c();
			qm.Id = quality;
			if(qualityCompMap.containsKey(quality))
			{
				qm.Complete__c = (Integer.valueOf(qualityCompMap.get(quality))==100?true:false);
			}
			else
			{
				qm.Complete__c = false;
			}
			updateQualityList.add(qm);
		}

		if(updateQualityList.size() > 0)
		{
			update updateQualityList;
		}
	}
}