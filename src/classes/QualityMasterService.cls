public class QualityMasterService
{
	public static Map<id,Quality_Master__c> qualityMasterMap(Set<Id> qm)
	{
       Map<id,Quality_Master__c> qmMap= new Map<id,Quality_Master__c>([Select Id,Quality_Code__c from Quality_Master__c where Id IN : qm]);
       return qmMap;
	}
}