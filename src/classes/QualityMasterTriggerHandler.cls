public class QualityMasterTriggerHandler {
	
	public static void onBeforeInsert(List<Quality_Master__c> newList)
	{
		for(Quality_Master__c qm : newList)
		{
			if(qm.Complete__c == true)
			{
				qm.addError('Please add compositions before marking it complete.');
			}
		}
	}

	public static void onBeforeUpdate(List<Quality_Master__c> newList, Map<Id, Quality_Master__c> oldMap)
	{
		List<Quality_Master__c> qmList = new List<Quality_Master__c>();
		Map<Id, Quality_Master__c> qmMap = new Map<Id, Quality_Master__c>();

		for(Quality_Master__c qm : newList)
		{
			if(qm.Complete__c != oldMap.get(qm.Id).Complete__c && qm.Complete__c == true)
			{
				qmList.add(qm);
				qmMap.put(qm.Id, qm);
			}
		}

		if(qmList.size() > 0)
		{
			checkQualityMasterForComplete(qmList, qmMap);
		}
	}

	public static void checkQualityMasterForComplete(List<Quality_Master__c> qmList, Map<Id, Quality_Master__c> qmMap)
	{
		Map<String, String> qualityCompMap = CompositionService.getTotalCompositionPercentageByQualityIds(qmMap.keySet());

		for(Quality_Master__c qm : qmList)
		{
			if(!qualityCompMap.containsKey(qm.Id))
			{
				qm.addError('Please add compositions before marking it complete.');
			}
			else if(Integer.valueOf(qualityCompMap.get(qm.Id)) != 100)
			{
				qm.addError('Overall composition % should be 100%.');
			}
		}
	}
}