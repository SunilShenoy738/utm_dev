public class LeadTriggerHandler
{
    public static void onAfterInsert(List<Lead> newList)
    {
        List<Lead> leadsToConvert = new List<Lead>();
        for(Lead l : newList)
        {
            if(l.Status == Constants.LEAD_CONVERTED_STATUS)
            {
                leadsToConvert.add(l);
            }
        }
        
        if(leadsToConvert.size() > 0)
        {    
            convertLead(leadsToConvert);
        }
    }
    
    public static void onAfterUpdate(List<Lead> newList, Map<Id,Lead> oldMap)
    {
         List<Lead> leadsToConvert = new List<Lead>();
         
         for(Lead l : newList)
         {
             if(l.Status != oldMap.get(l.Id).Status && l.Status == Constants.LEAD_CONVERTED_STATUS)
             {
                 leadsToConvert.add(l);
             }
         }   
         if(leadsToConvert.size() > 0)
         {
              convertLead(leadsToConvert);   
         }
    }
    
    
    public static void convertLead(List<Lead> newList)
    {
        List<Database.LeadConvert> lcList = new List<Database.LeadConvert>();
        LeadStatus convertStatus = LeadService.getLeadConvertedStatus();
        
        for(Lead l : newList)
        {
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.Id);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lcList.add(lc);
        }
        
        if(lcList.size() > 0)
        {
            List<Database.LeadConvertResult> lcr = Database.convertLead(lcList);
        }
    }
}