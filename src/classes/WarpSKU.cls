public class WarpSKU
{

@AuraEnabled
public string Id{get;set;}

@AuraEnabled
public string WarpSkuId{get;set;}

@AuraEnabled
public string WarpName{get;set;}

@AuraEnabled
public string ProductName{get;set;}


@AuraEnabled
public string ProductId{get;set;}

@AuraEnabled
public string Quality{get;set;}

@AuraEnabled
public string ColourGroup{get;set;}

@AuraEnabled
public string Ply{get;set;}

@AuraEnabled
public string YarnState{get;set;}

@AuraEnabled
public Decimal PercentageWasted{get;set;}


@AuraEnabled
public Decimal Dinner{get;set;}

@AuraEnabled
public Decimal Noofends{get;set;}

@AuraEnabled
public Decimal TotalPhysicalWeight{get;set;}

@AuraEnabled
public Decimal CostingWeight{get;set;}

@AuraEnabled
public Decimal TotalWeightAfterCrimpage{get;set;}

@AuraEnabled
public Decimal TotalCostAfterCrimpage{get;set;}

@AuraEnabled
public Decimal TotalRMCostKg{get;set;}

@AuraEnabled
public Decimal DegumLoss{get;set;}

@AuraEnabled
public Boolean IsWarp{get;set;}

@AuraEnabled
public Boolean Cramming{get;set;}

@AuraEnabled
public Decimal PPCPerCol{get;set;}

@AuraEnabled
public Decimal Picks{get;set;}

@AuraEnabled
public Decimal Dent{get;set;}

@AuraEnabled
public String SelectBeamPosition{get;set;}

@AuraEnabled
public String SelectedYarnStates{get;set;}

@AuraEnabled
public string RecordTypeName{get;set;}

    
}