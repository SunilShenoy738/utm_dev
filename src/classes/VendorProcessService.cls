public class VendorProcessService {
	
	public static List<Vendor_Process__c> getVendorProcessByVendorIdsAndProcessIds(Set<Id> vendorIds, Set<Id> processIds)
	{
		return [Select Id, Vendor__c, Vendor__r.Name, Process__c, Process__r.Name, Cost_Per_Kg__c, Cost_Per_Metre__c From Vendor_Process__c Where Vendor__c =:vendorIds And Process__c =: processIds];
	}

	public static Vendor_Process__c getVendorProcessByVendorIdAndProcessId(Id vendorId, Id processId)
	{
		Vendor_Process__c vp = [Select Id, Vendor__c, Vendor__r.Name, Process__c, Process__r.Name, Cost_Per_Kg__c, Cost_Per_Metre__c From Vendor_Process__c Where Vendor__c =:vendorId And Process__c =: processId];
		return vp;
	}

	public static Map<Id, List<Vendor_Process__c>> getVendorProcessMapByProcessIds(Set<Id> processIds)
	{
		Map<Id, List<Vendor_Process__c>> vpMap = new Map<Id, List<Vendor_Process__c>>();
		for(Vendor_Process__c vp : [Select Id, Name, Vendor__c, Vendor__r.Name, Process__c, Process__r.Name, Cost_Per_Kg__c, Cost_Per_Metre__c From Vendor_Process__c Where Process__c = :processIds])
		{
			List<Vendor_Process__c> vpList = new List<Vendor_Process__c>();
			if(vpMap.containsKey(vp.Process__c))
			{
				vpList = vpMap.get(vp.Process__c);
			}
			vpList.add(vp);
			vpMap.put(vp.Process__c,vpList);
		}
		return vpMap;
	}
	
}