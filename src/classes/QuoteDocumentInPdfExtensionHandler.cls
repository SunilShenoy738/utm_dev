public with sharing class QuoteDocumentInPdfExtensionHandler {

	private final Quote myQuote;
    public List<QuoteLineItem> listOfQuoteLineItems{get;set;}
    public List<Composition__c> listOfComposition{get;set;}
    public List<Composition__c> relatedCompositionList;
    public Map<Id,List<Composition__c>> mapOfLineItemToComposition{get;set;}
    
    public QuoteDocumentInPdfExtensionHandler(ApexPages.StandardController stdController) {

        listOfComposition=new List<Composition__c>();
        listOfQuoteLineItems=new List<QuoteLineItem>();
        relatedCompositionList=new List<Composition__c>();
        mapOfLineItemToComposition=new Map<Id,List<Composition__c>>();
        Set<Id> lineItemProduct2Id=new Set<Id>();

        myQuote = (Quote)stdController.getRecord();
        listOfQuoteLineItems=[SELECT Discount,Id,Product2.name,Product2.Actual_Width__c,Product2Id,Quantity,Subtotal,Total__c,UnitPrice FROM QuoteLineItem where QuoteId=:myQuote.id];
        for(QuoteLineItem qlitem:listOfQuoteLineItems)
        {
            lineItemProduct2Id.add(qlitem.Product2Id);
        }

        listOfComposition=[SELECT  Id,Composition_Percentage__c,SKU__c,Constituents__c from Composition__c where SKU__c in: lineItemProduct2Id];
        for(QuoteLineItem qline:listOfQuoteLineItems)
        {
            Id temp=null;
            relatedCompositionList = new List<Composition__c>();

            for(Composition__c comp:listOfComposition)
            {
                if(comp.SKU__c==qline.Product2Id)
                {
                        temp=qline.id;
                        relatedCompositionList.add(comp);
                }
            }
            if(temp!=null)
                mapOfLineItemToComposition.put(temp,relatedCompositionList);

        }
    }

   
}