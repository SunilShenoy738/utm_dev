public class FabricWrapper {
	@AuraEnabled
	public string FabricName{get;set;}
	@AuraEnabled
	public string WarpName{get;set;}
	 @AuraEnabled
	public string 	ProductId			{get;set;}
	 @AuraEnabled
    public string 	DesignStudioName	{get;set;}
     @AuraEnabled
    public string 	DesignSource		{get;set;}
     @AuraEnabled
    public string 	SelectedDesignSource		{get;set;}
     @AuraEnabled
    public string 	DevelopmentRef		{get;set;}
     @AuraEnabled
    public List<CostingMaster__c> WeaveType{get;set;}
     @AuraEnabled
    public string SelectWeave {get;set;}
     @AuraEnabled
    public String Reed{get;set;}
     @AuraEnabled
    public Decimal ReedWidthCms{get;set;}
     @AuraEnabled
    public string Loom{get;set;}

    @AuraEnabled
    public string SelectedLoom{get;set;}
    
     @AuraEnabled
        public Decimal HooksShafts{get;set;}
    @AuraEnabled
    public Decimal HooksRepeat{get;set;}
    @AuraEnabled
    public Decimal EPI{get;set;}
     @AuraEnabled
    public Decimal WeftWestage{get;set;}
     @AuraEnabled
    public Decimal PicksRepeat{get;set;}
     @AuraEnabled
    public Decimal ActualHorizontalRepeat{get;set;}
     @AuraEnabled
    public Decimal TotalFabricWeight{get;set;}
     @AuraEnabled
    public Decimal CalculatedVerticalRepeat{get;set;}
     @AuraEnabled
    public Decimal FinishedWidth{get;set;}
     @AuraEnabled
    public Decimal ActualVerticalRepeat{get;set;}
     @AuraEnabled
    public string Remarks{get;set;}
     @AuraEnabled
    public string StateOfYarn{get;set;}
     @AuraEnabled
    public List<WarpSKU> WeftList{get;set;}
    
     @AuraEnabled
    public List<WarpSKU> WarpList{get;set;}

    @AuraEnabled
    public List<Color_Master__c> ColourList{get;set;}

    @AuraEnabled
    public Decimal ActualHeight{get;set;}

    @AuraEnabled
    public Decimal CalculatedHeight{get;set;}

    @AuraEnabled
    public Decimal ActualWidth{get;set;}

    @AuraEnabled
    public Decimal WarpWastagePercentage{get;set;}

    @AuraEnabled
    public Decimal WeftWastageCms{get;set;}

    @AuraEnabled
    public Decimal FinalPPC {get;set;}

    @AuraEnabled
    public Decimal PPC {get;set;}

     @AuraEnabled
    public Decimal CrimpagePercentage {get;set;}

    @AuraEnabled
    public String Beams{get;set;}

    @AuraEnabled
    public String Warping{get;set;}

    @AuraEnabled
    public String SelectedWarping{get;set;}

    @AuraEnabled
    public String SelectedBeams{get;set;}

    @AuraEnabled
    public String SelectedReed{get;set;}

    @AuraEnabled
    public String BeamPosition{get;set;}

    @AuraEnabled
    public String SelectedWeaveType{get;set;}

    @AuraEnabled
    public Decimal WovenPPC{get;set;}

    @AuraEnabled
    public Decimal PhysicalFabricWeight{get;set;}


}