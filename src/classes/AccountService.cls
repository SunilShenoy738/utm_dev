public with sharing class AccountService {
	
	public static Map<Id,Account> getAccountMapByIds(Set<Id> accIds)
	{
		//Map<Id,Account> accMap = new Map<Id,Account>([Select Id, Name, Special_Instructions__c, RecordTypeId, RecordType.Name From Account Where Id =:accIds]);
		//return accMap;

		Map<Id, Account> resMap = new Map<Id, Account> ();
        for (Account accObj : [Select Id, 
        					   Name,
        					   Special_Instructions__c,
        					   RecordTypeId,
        					   RecordType.Name,
                               Minimum_Quantity_Per_Shipment__c,
                               Advance__c, 
                               Payment_Term__c, 
                               Number_of_days_after_Airway_Bill_date__c, 
                               Number_of_days_after_shipment_date__c, 
                               Days_after_first_payment_post_shipment__c, 
                               Days_after_second_payment_post_shipment__c,  
                               Days_after_goods_receipt__c, 
                               Before_Shipment__c, 
                               Percentage_first_payment__c, 
                               Percentage_second_payment__c,
                               Courier_Account_Number__c,
                               Courier_Account_Type__c 
                               from 
                               Account
                               WHERE Id in : accIds]) {
            resMap.put (accObj.Id, accObj);
        }
        return resMap;
	}
}