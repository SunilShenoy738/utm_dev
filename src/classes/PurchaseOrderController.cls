public with sharing class PurchaseOrderController {
 
  @AuraEnabled
    public static Order getOrderDetails(string orderId){
        return [SELECT Id,Name,OrderNumber,Status, TotalAmount, Account.Name FROM Order WHERE Id = :orderId];
    }

     @AuraEnabled
    public static string getGrades(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Purchase_Order_Item__c.Grade__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        string res=JSON.serialize(ple);
        return res;
    }

@AuraEnabled
   public static List<OrderItem> getOrderSKU(string orderId){
    List<RecordType> lstRecType= [SELECT Id,Name FROM RecordType WHERE SobjectType='Product2'];
      List<String> recTypes= new List<String>();

      for(RecordType r : lstRecType)
      {
        if(r.Name =='Prchased Fabric') // && r.Name !='Fabric'
        {
          recTypes.add(r.Id);
        }
      }

      List<Id> produxtsIds= new List<Id>();
      List<OrderItem> lstOI= [SELECT Product2Id, Product2.Name,Product2.RecordType.Name  FROM OrderItem WHERE Product2.RecordTypeId IN : recTypes];
     return lstOI;

   
   }

    @AuraEnabled
    public static string getStateofSKU(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Purchase_Order_Item__c.Yarn_State__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        string res=JSON.serialize(ple);
        return res;
    }

    @AuraEnabled
    public static string getUOM(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Purchase_Order_Item__c.Unit_Of_Measurement__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        string res=JSON.serialize(ple);
        return res;
    }

    


    @AuraEnabled
    public static List<Profile> getAllDepartment(){
        List<Profile> lstProf = [Select Id, Name from Profile ];
        return lstProf;
    }

    @AuraEnabled
    public static List<PurchaseOrderWrapper> getReqPO(string poNumber, string supplierNm, string department){
     /*  List<Purchase_Order_Item__c> po= [SELECT SKU__r.Name,Purchase_Order__r.Supplier__r.Name,Purchase_Order__r.Supplier__c ,
       Purchase_Order__r.Name,SKU__c,Rate__c,
       Yarn_State__c,Unit_Of_Measurement__c,Quantity__c,Remarks__c FROM Purchase_Order_Item__c
       ORDER BY Purchase_Order__r.Supplier__r.Name];*/
       string status='Requested';
       string query='SELECT SKU__r.Name,Purchase_Order__r.Supplier__r.Name,Purchase_Order__r.Supplier__c ,Purchase_Order__r.Name,Purchase_Order__r.OrderNo__c,SKU__c,Rate__c, Yarn_State__c,Unit_Of_Measurement__c,Quantity__c,Remarks__c, Grade__c,Colour_Name__c FROM Purchase_Order_Item__c WHERE Purchase_Order__r.Status__c= :status AND  Purchase_Order__r.IsActive__c = true';
       if(poNumber != '')
       {
            query = query + ' AND Purchase_Order__r.Name =: poNumber ' ;
        }
        if(supplierNm != '')
        {
            query = query + ' AND Purchase_Order__r.Supplier__c =:supplierNm '  ;
        }
        system.debug('query');
        system.debug(query);
      List<Purchase_Order_Item__c> lstPo= Database.query(query);
      system.debug(lstPo);
       return TransforData(lstPo);
      
    }
     @AuraEnabled
    public static List<PurchaseOrderWrapper> TransforData(List<Purchase_Order_Item__c> po)
    {
        system.debug('F1');
        system.debug(po);
       List<PurchaseOrderWrapper> pow= new List<PurchaseOrderWrapper>();
       PurchaseOrderWrapper pw;
       Set<String> suppliers= new Set<String>();
       for(Purchase_Order_Item__c poic : po)
        {
            suppliers.add(poic.Purchase_Order__r.Supplier__c);
        }

        
        //IsSupplier
       for(String s : suppliers)
       {
       
        pw = new PurchaseOrderWrapper();
        pw.Supplier= s;
        pw.SupplierName= GetSupplierName(po,s);
        pw.IsSupplier= true;

        pow.add(pw);

        for(Purchase_Order_Item__c poic : po)
        {
            if(poic.Purchase_Order__r.Supplier__c == s)
            {

                    pw = new PurchaseOrderWrapper();
                     pw.IsSelected= false;
                     pw.Id=poic.id;
                     pw.IsSupplier= false;
                     pw.Quantity= poic.Quantity__c;
                     pw.Rate= poic.Rate__c;
                     pw.SKU= poic.SKU__c;
                     pw.SKUName= poic.SKU__r.Name;
                     pw.SupplierName= poic.Purchase_Order__r.Supplier__r.Name;
                     pw.Supplier= poic.Purchase_Order__r.Supplier__c;
                     pw.UOM= poic.Unit_Of_Measurement__c;
                     pw.YarnState= poic.Yarn_State__c;
                     pw.PoId=poic.Purchase_Order__c;
                     pw.PoNo=poic.Purchase_Order__r.OrderNo__c;
                     pw.Remarks= poic.Remarks__c;
                     pw.SelectedGrade= poic.Grade__c;
                     pw.ColourName=  poic.Colour_Name__c;

                     pow.add(pw);

            }
        }
    }
    system.debug('F2');
    system.debug(pow);

       return pow;
    }

 
    private static string GetSupplierName(List<Purchase_Order_Item__c> lst, string supId)
    {
        string supplierName='';
        for(Purchase_Order_Item__c l : lst)
        {
            if(supId== l.Purchase_Order__r.Supplier__c)
            {
                SupplierName= l.Purchase_Order__r.Supplier__r.Name;
            }
        }
        return SupplierName;
    }
    

     @AuraEnabled
    public static List<Account> getSupplierList(){
        List<Account> lstSupp= new List<Account>();
        lstSupp= [SELECT Id, Name FROM Account WHERE RecordType.Name='Supplier' ];
        return lstSupp;
    }

    @AuraEnabled
    public static List<Product2> getYarnDetail(string yarnId){

        List<Product2> product= [SELECT  Id, Name,RecordType.Name FROM Product2 where id = :yarnId];
        if(product[0].RecordType.Name=='Grindles')
        {
            List<Composition__c> lstComp= [SELECT Yarn__c FROM Composition__c where SKU__c= :yarnId];
            List<Id> pIds= new List<Id>();
            for(Composition__c c : lstComp){pIds.add(c.Yarn__c);}
            List<Product2> lstProducts= [SELECT  Id, Name,RecordType.Name FROM Product2 where id IN :pIds];
            return lstProducts;
        }
        else
        {
            return product;
        }

    }
      @AuraEnabled
    public static List<Account_SKU__c> getSupplierSKU(string yarnId, string SupplierId){

        List<Account_SKU__c> acc= [SELECT  Rate__c,SKU__c,Account__c,Dying_Rate__c, Heating_Setting_Rate__c, 
        Kora_Dying_Rate__c, Twisting_Rate__c 
        FROM Account_SKU__c 
        where SKU__c = :yarnId AND Account__c = :SupplierId ];

      return acc;
       

    }
     @AuraEnabled
    public static string savePurchaseOrder(string purchaseData){
        List<PurchaseOrderWrapper> items = (List<PurchaseOrderWrapper>)JSON.deserialize(purchaseData, List<PurchaseOrderWrapper>.class);
      List<PurchaseOrderWrapper> poItemsForOrder =new List<PurchaseOrderWrapper>();
       Purchase_Order_Item__c poic;
        List<Purchase_Order_Item__c> lineItemsSelected=new List<Purchase_Order_Item__c>();

       for(PurchaseOrderWrapper po : items)
        {
            if(po.IsSupplier == false || po.Quantity == 0)
            {
                poic = new Purchase_Order_Item__c();
                
                if(po.IsSelected)
                {
                  poic.Id= po.Id;
                  lineItemsSelected.add(poic);
                    if(po.Quantity > 0)
                    {
                      poItemsForOrder.add(po);
                    }
                }
            }
        }
        delete lineItemsSelected;
        CreatePO(poItemsForOrder, 'Created');

        List<Purchase_Order__c> listId = new List<Purchase_Order__c>();
        List<Purchase_Order__c> poCounts = [SELECT Id, RecCount__c FROM Purchase_Order__c ];
        for (Purchase_Order__c u : poCounts) {
           if(u.RecCount__c ==0)
           {
             listId.add(u);
           }
            
        }
        delete listId;
        

        return 'success';
    }

    @AuraEnabled
    public static string savePurchaseRequisition(string purchaseData){
    	system.debug(purchaseData);
    	List<PurchaseOrderWrapper> items = (List<PurchaseOrderWrapper>)JSON.deserialize(purchaseData, List<PurchaseOrderWrapper>.class);
    	CreatePO(items, 'Requested');
       return 'success';

    }

    @AuraEnabled
    public static string savePOFromSalesOrder(string purchaseData)
    {
      system.debug(purchaseData);
      List<PurchaseOrderWrapper> items = (List<PurchaseOrderWrapper>)JSON.deserialize(purchaseData, List<PurchaseOrderWrapper>.class);
      CreatePO(items, 'Requested');
       return 'success';
    }

private static void CreatePO(List<PurchaseOrderWrapper> items, string status)
{
    List<Purchase_Order_Item__c> PoItems= new List<Purchase_Order_Item__c>();

        Set<String> suppliers= new Set<String>();
        for(PurchaseOrderWrapper pw : items)
        {
          system.debug('IN SUPP LOOP:' + pw.Supplier);
            suppliers.add(pw.Supplier);
        }

        Purchase_Order_Item__c pi ;
        List<SObject> bulkOrder= new List<SObject>();
        for(String s : suppliers)
        {
            system.debug('supplier:' + s);
            String str = string.valueof(Math.abs(Crypto.getRandomLong()));
            String randomNumber = s + '_' + str.substring(0, 10);
            bulkOrder.add(new Purchase_Order__c(Status__c=status,Supplier__c=s,RefrenceId__c=randomNumber));
            for(PurchaseOrderWrapper w : items)
            {
                if(w.Supplier == s)
                {
                    pi= new Purchase_Order_Item__c();
                    
                    pi.Quantity__c= w.Quantity;
                    pi.Rate__c=w.Rate;
                    pi.SKU__c= w.SKU;
                    pi.Total__c= w.Total;
                    pi.Yarn_State__c= w.Yarnstate;
                    pi.Remarks__c= w.Remarks;
                    pi.Grade__c= w.SelectedGrade;
                    pi.Colour_Name__c= w.ColourName;
                    pi.Purchase_Order__r=new Purchase_Order__c(RefrenceId__c=randomNumber); //po.Id;
                    //PoItems.add(pi);
                    bulkOrder.add(pi);
                    system.debug(pi);
                }
            }
        }
        system.debug(bulkOrder);

        insert bulkOrder;

}

    public with sharing class PurchaseOrderWrapper{
    	 @AuraEnabled
    	public Decimal Quantity{get;set;}

    	 @AuraEnabled
    	public string SKU {get;set;}

    	 @AuraEnabled
    	public string YarnState{get;set;}

    	 @AuraEnabled
    	public string Supplier{get;set;}

    	 @AuraEnabled
    	public string Remarks {get;set;}

    	 @AuraEnabled
    	public Decimal Total {get;set;}

    	 @AuraEnabled
    	public Decimal Rate{get;set;}

         @AuraEnabled
        public String SupplierName{get;set;}

         @AuraEnabled
        public String POId{get;set;}

         @AuraEnabled
        public String PONo{get;set;}

          @AuraEnabled
        public String SKUName{get;set;}

          @AuraEnabled
        public String UOM{get;set;}

         @AuraEnabled
        public String Id{get;set;}

        @AuraEnabled
        public Boolean IsSupplier{get;set;}

         @AuraEnabled
        public Boolean IsSelected{get;set;}

        @AuraEnabled
        public string SelectedGrade{get;set;}

        @AuraEnabled
        public string ColourName{get;set;}
    }
}