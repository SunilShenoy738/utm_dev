public class ProductionPlanController {

    @AuraEnabled
    public static List<OrderItemProcessWrapper> getOrderDetails(String orderId){
        List<OrderItem> lst= [SELECT Id,Product2.Id,Product2.Name,Order.OrderNumber,Quantity,Unit_Of_Measurement__c,
        EndDate,Remarks__c,Order.Id FROM OrderItem
       WHERE Order.Id = : orderId]; // WHERE Status__c ='Created'
        List<OrderItemProcessWrapper> lstItemProcess = new List<OrderItemProcessWrapper>();
        for(OrderItem i : lst)
        {
        	OrderItemProcessWrapper item= new OrderItemProcessWrapper();
        	item.OrderItemId= i.Id;
            item.ProductId= i.Product2.Id;
        	item.SKU=i.Product2.Name;
        	item.OrderNumber=i.Order.OrderNumber;
        	item.OrderId= i.Order.Id;
        	item.Quantity= Integer.valueOf(i.Quantity);
        	item.UnitOfMeasurement= i.Unit_Of_Measurement__c;
        	item.DeliveryDate= i.EndDate;
        	item.Remarks= i.Remarks__c;
        	item.RemarksNew='';
        	lstItemProcess.add(item);

        }
        return lstItemProcess;
    }

    @AuraEnabled
    public static List<OrderItemProcessWrapper> GetOrderItemProcesses(string orderItemData )
    {
    	//OrderItemProcessWrapper oi
    	System.debug(orderItemData);
    	List<OrderItemProcessWrapper> obj = (List<OrderItemProcessWrapper>)JSON.deserialize(orderItemData, List<OrderItemProcessWrapper>.class);
    	OrderItemProcessWrapper oi=obj[0];
    	System.debug(oi);
        Map<String,List<SKU_Process__c>> skuProcessMap = new Map<String,List<SKU_Process__c>>();
        
        
        System.debug ('Product Id' + oi.ProductId);
        Product2 sku = getSKUDetail(Id.valueOf(oi.ProductId));
        String weavingOutputName;
        System.debug('SKURecordType Name: '+sku.RecordType.Name);

        if(sku.RecordType.Name == Constants.RT_PRODUCT2_VALUE_ADDED)
        {
            System.debug('-----ValueAdd-------- Start');
            skuProcessMap = getSKUProcessMap(skuProcessMap, sku.SKU_Processes__r);
            System.debug('ValueAdded skuProcessMap: '+skuProcessMap);
            Product2 baseSKU = getSKUDetail(sku.Base_Fabric__c);
            System.debug('BaseSKU: '+baseSKU);
            weavingOutputName = baseSKU.Name;
            System.debug('weavingOutputName: '+weavingOutputName);
            skuProcessMap = getSKUProcessMap(skuProcessMap, baseSKU.SKU_Processes__r);
            System.debug('BaseSku skuProcessMap: '+skuProcessMap);
            List<SKU_Process__c> skuProcessList = getBillOfMaterialsSKUProcess(sku.Id);
            System.debug('BOM skuProcessList: '+skuProcessList);
            skuProcessMap = getSKUProcessMap(skuProcessMap, skuProcessList);
            System.debug('BOM skuProcessMap: '+skuProcessMap);
            System.debug('-----ValueAdd-------- End');
        }
        else if(sku.RecordType.Name == Constants.RT_PRODUCT2_FABRIC)
        {
            System.debug('-----FAbric-------- Start');
            weavingOutputName = sku.Name;
            System.debug('weavingOutputName: '+weavingOutputName);
            skuProcessMap = getSKUProcessMap(skuProcessMap, sku.SKU_Processes__r);
            System.debug('Fabric skuProcessMap: '+skuProcessMap);
            List<SKU_Process__c> skuProcessList = getBillOfMaterialsSKUProcess(sku.Id);
            System.debug('BOM skuProcessList: '+skuProcessList);
            skuProcessMap = getSKUProcessMap(skuProcessMap, skuProcessList);
            System.debug('BOM skuProcessMap: '+skuProcessMap);
            System.debug('-----FAbric-------- End');
        }
        
        List<OrderItemProcessWrapper> lst= new List<OrderItemProcessWrapper>();
        List<Vendor_Process__c> process= [SELECT Vendor__c,Vendor__r.Name FROM Vendor_Process__c];
        List<SKUSupplier> lstProcess= new  List<SKUSupplier>();

        for(Vendor_Process__c p : process)
        {
            SKUSupplier vp= new SKUSupplier();
            vp.Id=p.Vendor__c;
            vp.Name=p.Vendor__r.Name;
            lstProcess.add(vp);
        }

        for(String processName : skuProcessMap.keySet())
        {
            OrderItemProcessWrapper item= new OrderItemProcessWrapper();
            item.SKU=oi.SKU;
            item.ProductId=oi.ProductId;
            item.OrderItemId=oi.OrderItemId;
            item.OrderId=oi.OrderId;
            item.Remarks=oi.Remarks;
            item.Quantity=oi.Quantity;
            item.AssignQty=oi.Quantity;
            item.RemarksNew=oi.RemarksNew;
            item.CustomerSKUProcess=processName;
            item.SKUMasterProcess=processName;
            System.debug('processName: '+processName);
            if(processName == Constants.DEPARTMENT_WEAVING)
            {
                item.Input='Yarn';
                item.Output=weavingOutputName;
                item.ProcessNumber=1;
            }
            if(processName == Constants.DEPARTMENT_PRINTING)
            {
                item.Input=skuProcessMap.get(processName)[0].Input_Stock_SKU__r.Name;
                item.Output=skuProcessMap.get(processName)[0].Output_Stock_SKU__r.Name;
                item.ProcessNumber=2;
            }
            if(processName == Constants.DEPARTMENT_EMBROIDERY)
            {
                item.Input=skuProcessMap.get(processName)[0].Input_Stock_SKU__r.Name;
                item.Output=skuProcessMap.get(processName)[0].Output_Stock_SKU__r.Name;
                item.ProcessNumber=3;
            }

            
            item.IsSelected= true;
            item.Rate=0;
            item.DeliveryDate=System.Today();
            item.Supplier= lstProcess;
            lst.add(item);
        }
        

        return lst;

        
        
    	/*List<OrderItemProcessWrapper> lst= new List<OrderItemProcessWrapper>();

    	List<Vendor_Process__c> process= [SELECT Vendor__c,Vendor__r.Name FROM Vendor_Process__c];
    	
    	List<SKUSupplier> lstProcess= new  List<SKUSupplier>();
    	for(Vendor_Process__c p : process)
    	{
    		SKUSupplier vp= new SKUSupplier();
    		vp.Id=p.Vendor__c;
    		vp.Name=p.Vendor__r.Name;
    		lstProcess.add(vp);
    	}

    	OrderItemProcessWrapper item1= new OrderItemProcessWrapper();
    	item1.SKU=oi.SKU;
    	item1.ProductId=oi.ProductId;
    	item1.OrderItemId=oi.OrderItemId;
    	item1.OrderId=oi.OrderId;
    	item1.Remarks=oi.Remarks;
    	item1.Quantity=oi.Quantity;
    	item1.AssignQty=oi.Quantity;
    	item1.RemarksNew=oi.RemarksNew;
    	item1.CustomerSKUProcess='Weaving';
    	item1.SKUMasterProcess='Weaving';
        item1.Input='Yarn';
        item1.Output='Weaving';
    	item1.ProcessNumber=1;
    	item1.IsSelected= true;
    	item1.Rate=0;
    	item1.DeliveryDate=System.Today();
    	item1.Supplier= lstProcess;
    	lst.add(item1);

		item1= new OrderItemProcessWrapper();
    	item1.SKU=oi.SKU;
    	item1.ProductId=oi.ProductId;
    	item1.OrderItemId=oi.OrderItemId;
    	item1.OrderId=oi.OrderId;
    	item1.Remarks=oi.Remarks;
    	item1.Quantity=oi.Quantity;
    	item1.AssignQty=oi.Quantity;
    	item1.RemarksNew=oi.RemarksNew;
    	item1.CustomerSKUProcess='Printing';
    	item1.SKUMasterProcess='Printing';
        item1.Input='Weaving';
        item1.Output='Weaving + Printing';
    	item1.ProcessNumber=2;
    	item1.IsSelected= true;
    	item1.Rate=0;
    	item1.DeliveryDate=System.Today();
    	item1.Supplier= lstProcess;
    	lst.add(item1);

    	item1= new OrderItemProcessWrapper();
    	item1.SKU=oi.SKU;
    	item1.ProductId=oi.ProductId;
    	item1.OrderItemId=oi.OrderItemId;
    	item1.OrderId=oi.OrderId;
    	item1.Remarks=oi.Remarks;
    	item1.Quantity=oi.Quantity;
    	item1.AssignQty=oi.Quantity;
    	item1.RemarksNew=oi.RemarksNew;
    	item1.CustomerSKUProcess='Hand Embroidery';
    	item1.SKUMasterProcess='Hand Embroidery';
        item1.Input='Weaving + Printing';
        item1.Output='Weaving + Printing + Embroidery';
    	item1.ProcessNumber=3;
    	item1.IsSelected= true;
    	item1.Rate=0;
    	item1.DeliveryDate=System.Today();
    	item1.Supplier= lstProcess;
    	lst.add(item1);

    	return lst;*/
        return null;

    }

    public static Product2  getSKUDetail(Id skuId)
    {
        Product2 sku = [Select Id, RecordType.Name, Name, Base_Fabric__c, (Select Id, hasIO__c, Input_Stock_SKU__c, Input_Stock_SKU__r.Name, Output_Stock_SKU__c, Output_Stock_SKU__r.Name, Parent_SKU_Process__c, Process__c, Process__r.Name, Process__r.Department__c, Step__c From SKU_Processes__r Where Parent_SKU_Process__c = null ) From Product2 Where Id =: skuId];
        return sku;
    }

    public static List<Product2> getSKUsDetails(Set<Id> skuIds)
    {
        List<Product2> skuList = [Select Id, RecordType.Name, Name, Base_Fabric__c, (Select Id, hasIO__c, Input_Stock_SKU__c, Input_Stock_SKU__r.Name, Output_Stock_SKU__c, Output_Stock_SKU__r.Name, Parent_SKU_Process__c, Process__c, Process__r.Name, Process__r.Department__c, Step__c From SKU_Processes__r Where Parent_SKU_Process__c = null ) From Product2 Where Id =: skuIds];
        return skuList;
    }

    public static List<SKU_Process__c> getBillOfMaterialsSKUProcess(Id skuId)
    {
        Set<Id> yarnIds = new Set<Id>();
        List<SKU_Process__c> skuProcessList = new List<SKU_Process__c>();
        
        for(Bill_of_Material__c bom : [Select Id, Fabric__c,Yarn__c,Yarn__r.Name From Bill_of_Material__c Where Fabric__c =: skuId])
        {
            yarnIds.add(bom.Yarn__c);
        }
        
        for(Product2 sku : getSKUsDetails(yarnIds))
        {
            if(sku.SKU_Processes__r.size() > 0)
            {
                skuProcessList.addAll(sku.SKU_Processes__r);
            }
        }
        return skuProcessList;
    }

    public static Map<String,List<SKU_Process__c>> getSKUProcessMap(Map<String,List<SKU_Process__c>> skuProcessMap, List<SKU_Process__c> skuProcessList)
    {
        for(SKU_Process__c skuProcess : skuProcessList)
        {
            List<SKU_Process__c> tempList = new List<SKU_Process__c>();
            String str = (skuProcess.Process__r.Department__c == Constants.DEPARTMENT_WEAVING?Constants.DEPARTMENT_WEAVING:(skuProcess.Process__r.Department__c == Constants.DEPARTMENT_PRINTING?Constants.DEPARTMENT_PRINTING:Constants.DEPARTMENT_EMBROIDERY));
            if(skuProcessMap.containsKey(str))
            {
                tempList = skuProcessMap.get(skuProcess.Process__r.Name);
            }
            tempList.add(skuProcess);
            skuProcessMap.put(skuProcess.Process__r.Name,tempList);
        }
        return skuProcessMap;
    }

     @AuraEnabled
    public static string saveJobs(string orderItemData, string itemId)
    {
    	 List<OrderItemProcessWrapper> obj = (List<OrderItemProcessWrapper>)JSON.deserialize(orderItemData, List<OrderItemProcessWrapper>.class);
    	List<Job__c> jobs= new List<Job__c>();
    	

    	 for(OrderItemProcessWrapper oi : obj )
    	 {
    	 	Job__c job = new Job__c();
    	 	job.Customer_SKU_Process__c= oi.CustomerSKUProcess;
    	 	job.Delivery_Date__c= oi.DeliveryDate;
    	 	job.Order__c= oi.OrderId;
    	 	job.Order_SKU__c= oi.OrderItemId;
    	 	job.Quantity__c= oi.Quantity;
    	 	job.Remarks__c= oi.RemarksNew;
    	 	job.Sequence__c= oi.ProcessNumber;
    	 	job.SKU__c= oi.ProductId;
    	 	job.SKU_Process__c= oi.SKUMasterProcess;
    	 	job.Unit_Of_Measurement__c= oi.UnitOfMeasurement;
    	 	job.Status__c='In Progress';
    	 	job.Rate__c=oi.Rate;
    	 	job.Supplier__c= oi.SelectedSupplier;
    	 	job.AssignQty__c= oi.AssignQty;
    	 	job.JobOption__c= oi.JobOption;
    	 	job.Unit_Of_Measurement__c= oi.UnitOfMeasurement;

    	 	jobs.add(job);
    	 }
    	 List<Job__c> ordersToDelete= [SELECT Id FROM Job__c WHERE Order_SKU__c = : itemId ];
    	 delete ordersToDelete;
    	 if(obj.size() > 0)
    	 {
    	 OrderItem oi= new OrderItem();
    	 oi.Id=obj[0].OrderItemId;
    	 oi.Status__c='Processed';
    	 update oi;
    	}
    	 insert jobs;
    	 
    	 return 'success';


    }

     @AuraEnabled
    public static List<OrderItemProcessWrapper> getOrderJobDetails(string orderId){
        List<Job__c> lst= [SELECT Id,SKU__c,SKU__r.Name,Order__c,Order_SKU__c,Order__r.OrderNumber,Quantity__c,Unit_Of_Measurement__c,
        Delivery_Date__c,Remarks__c,Order_SKU__r.Remarks__c FROM Job__c
        WHERE Status__c ='In Progress' AND Order__c = : orderId];
        List<OrderItemProcessWrapper> lstItemProcess = new List<OrderItemProcessWrapper>();
        for(Job__c i : lst)
        {
        	OrderItemProcessWrapper item= new OrderItemProcessWrapper();
        	item.JobId= i.Id;
        	item.OrderItemId= i.Order_SKU__c;
        	item.ProductId= i.SKU__c;
        	item.SKU=i.SKU__r.Name;
        	item.OrderNumber=i.Order__r.OrderNumber;
        	item.OrderId= i.Order__c;
        	item.Quantity= Integer.valueOf(i.Quantity__c);
        	item.UnitOfMeasurement= i.Unit_Of_Measurement__c;
        	item.DeliveryDate= i.Delivery_Date__c;
        	item.Remarks= i.Order_SKU__r.Remarks__c;
        	item.RemarksNew=i.Remarks__c;
        	lstItemProcess.add(item);

        }
        return lstItemProcess;
    }


    @AuraEnabled
    public static List<OrderItemProcessWrapper> getJobsByDepartment(string department)
    {
        List<Job__c> lst= new List<Job__c>();
        lst=[SELECT Id,Name,SKU__c, SKU__r.Name,Order_SKU__r.Remarks__c,Quantity__c,Unit_Of_Measurement__c,Delivery_Date__c,
        Order__c,Order__r.OrderNumber,Remarks__c,Order_SKU__c, SKU_Process__c FROM Job__c  WHERE SKU_Process__c = :department];
        //WHERE Status__c='In Progress' AND
        List<OrderItemProcessWrapper> items = new List<OrderItemProcessWrapper>();
        for(Job__c i : lst)
        {
            OrderItemProcessWrapper item= new OrderItemProcessWrapper();
            item.JobId= i.Id;
            item.JobNumber=i.Name;
            item.OrderItemId= i.Order_SKU__c;
            item.ProductId= i.SKU__c;
            item.SKU=i.SKU__r.Name;
            item.OrderNumber=i.Order__r.OrderNumber;
            item.OrderId= i.Order__c;
            item.Quantity= Integer.valueOf(i.Quantity__c);
            item.UnitOfMeasurement= i.Unit_Of_Measurement__c;
            item.DeliveryDate= i.Delivery_Date__c;
            item.Remarks= i.Order_SKU__r.Remarks__c;
            item.RemarksNew=i.Remarks__c;
            items.add(item);

        }
        return items;
    }

     @AuraEnabled
    public static List<OrderItemProcessWrapper> GetJobProcesses(string orderItemData )
    {
        //OrderItemProcessWrapper oi
        System.debug(orderItemData);
         List<OrderItemProcessWrapper> obj = (List<OrderItemProcessWrapper>)JSON.deserialize(orderItemData, List<OrderItemProcessWrapper>.class);
        OrderItemProcessWrapper oi=obj[0];
        System.debug(oi);
        List<OrderItemProcessWrapper> lst= new List<OrderItemProcessWrapper>();

        List<Vendor_Process__c> process= [SELECT Vendor__c,Vendor__r.Name FROM Vendor_Process__c];
        
        List<SKUSupplier> lstProcess= new  List<SKUSupplier>();
        for(Vendor_Process__c p : process)
        {
            SKUSupplier vp= new SKUSupplier();
            vp.Id=p.Vendor__c;
            vp.Name=p.Vendor__r.Name;
            lstProcess.add(vp);
        }

        OrderItemProcessWrapper item1= new OrderItemProcessWrapper();
        item1.SKU=oi.SKU;
        item1.JobId= oi.JobId;
        item1.ProductId=oi.ProductId;
        item1.OrderItemId=oi.OrderItemId;
        item1.OrderId=oi.OrderId;
        item1.Remarks=oi.Remarks;
        item1.Quantity=oi.Quantity;
        item1.AssignQty=oi.Quantity;
        item1.RemarksNew=oi.RemarksNew;
        item1.CustomerSKUProcess='Coning';
        item1.SKUMasterProcess='Coning';
        //item1.Input='Yarn';
        //item1.Output='Weaving';
        item1.ProcessNumber=1;
        item1.IsSelected= true;
        item1.Rate=0;
        item1.DeliveryDate=System.Today();
        item1.Supplier= lstProcess;
        item1.MillRemarks= '';
        lst.add(item1);

        item1= new OrderItemProcessWrapper();
        item1.SKU=oi.SKU;
        item1.JobId= oi.JobId;
        item1.ProductId=oi.ProductId;
        item1.OrderItemId=oi.OrderItemId;
        item1.OrderId=oi.OrderId;
        item1.Remarks=oi.Remarks;
        item1.Quantity=oi.Quantity;
        item1.AssignQty=oi.Quantity;
        item1.RemarksNew=oi.RemarksNew;
        item1.CustomerSKUProcess='Dyeing';
        item1.SKUMasterProcess='Dyeing';
        //item1.Input='Weaving';
        //item1.Output='Weaving + Printing';
        item1.ProcessNumber=2;
        item1.IsSelected= true;
        item1.Rate=0;
        item1.DeliveryDate=System.Today();
        item1.Supplier= lstProcess;
        item1.MillRemarks= '';
        lst.add(item1);

        item1= new OrderItemProcessWrapper();
        item1.SKU=oi.SKU;
        item1.JobId= oi.JobId;
        item1.ProductId=oi.ProductId;
        item1.OrderItemId=oi.OrderItemId;
        item1.OrderId=oi.OrderId;
        item1.Remarks=oi.Remarks;
        item1.Quantity=oi.Quantity;
        item1.AssignQty=oi.Quantity;
        item1.RemarksNew=oi.RemarksNew;
        item1.CustomerSKUProcess='Warping';
        item1.SKUMasterProcess='Warping';
        //item1.Input='Weaving + Printing';
        //item1.Output='Weaving + Printing + Embroidery';
        item1.ProcessNumber=3;
        item1.IsSelected= true;
        item1.Rate=0;
        item1.DeliveryDate=System.Today();
        item1.Supplier= lstProcess;
        item1.MillRemarks= '';
        lst.add(item1);

        return lst;

    }

  @AuraEnabled
    public static string saveJobOperations(string jobData, string jobId)
    {
         List<OrderItemProcessWrapper> obj = (List<OrderItemProcessWrapper>)JSON.deserialize(jobData, List<OrderItemProcessWrapper>.class);
        List<Job_Process__c> jobs= new List<Job_Process__c>();
        

         for(OrderItemProcessWrapper oi : obj )
         {
            Job_Process__c job = new Job_Process__c();
            job.Delivery_Date__c= oi.DeliveryDate;
            job.Remarks__c= oi.Remarks;
            job.Sequence__c= oi.ProcessNumber;
            job.Job__c= oi.JobId;
            jobs.add(job);
         }
         List<Job_Process__c> jobsToDelete= [SELECT Id FROM Job_Process__c WHERE Job__c = : jobId ];
         delete jobsToDelete;
         if(obj.size() > 0)
         {
         Job__c jc= new Job__c();
         jc.Id=jobId;
         jc.Status__c='In Operation';
         update jc;
        }
         insert jobs;
         
         return 'success';


    }

  @AuraEnabled
    public static List<OrderItemProcessWrapper> getJobOperationDetails(){
        List<Job_Process__c> lst= [SELECT Id,Name,Remarks__c,Delivery_Date__c,Sequence__c, Job__r.Quantity__c,Job__r.SKU__r.Name,
      Job__r.Id, Job__r.Order__r.OrderNumber,Job__r.Unit_Of_Measurement__c,Job__r.Remarks__c  FROM Job_Process__c];
        List<OrderItemProcessWrapper> lstItemProcess = new List<OrderItemProcessWrapper>();
        for(Job_Process__c i : lst)
        {
            OrderItemProcessWrapper item= new OrderItemProcessWrapper();
            item.JobId= i.Job__r.Id;
            item.SKU=i.Job__r.SKU__r.Name;
            item.OrderNumber=i.Job__r.Order__r.OrderNumber;
            item.Quantity= Integer.valueOf(i.Job__r.Quantity__c);
            item.UnitOfMeasurement= i.Job__r.Unit_Of_Measurement__c;
            item.DeliveryDate= i.Delivery_Date__c;
            item.Remarks= i.Remarks__c;
            item.RemarksNew=i.Job__r.Remarks__c;
            lstItemProcess.add(item);

        }
        return lstItemProcess;
    }

    @AuraEnabled
    public static List<OrderItemProcessWrapper> getRecievedWorkOrder(string department){
        List<Job_Process__c> lst= [SELECT Id,Name,Remarks__c,Delivery_Date__c,Sequence__c, Job__r.Quantity__c,Job__r.SKU__r.Name,
      Job__r.Id, Job__r.Order__r.OrderNumber,Job__r.Unit_Of_Measurement__c,Job__r.Remarks__c,Job__r.Name  FROM Job_Process__c
      WHERE Job__r.SKU_Process__c = :department and Status__c ='Created'];
        List<OrderItemProcessWrapper> lstItemProcess = new List<OrderItemProcessWrapper>();
        for(Job_Process__c i : lst)
        {
            OrderItemProcessWrapper item= new OrderItemProcessWrapper();
            item.JobId= i.Id;
             item.JobNumber= i.Name;
            item.SKU=i.Job__r.SKU__r.Name;
            item.OrderNumber=i.Job__r.Order__r.OrderNumber;
            item.Quantity= Integer.valueOf(i.Job__r.Quantity__c);
            item.UnitOfMeasurement= i.Job__r.Unit_Of_Measurement__c;
            item.DeliveryDate= i.Delivery_Date__c;
            item.Remarks= i.Remarks__c;
            item.RemarksNew=i.Job__r.Remarks__c;
            lstItemProcess.add(item);

        }
        return lstItemProcess;
    }

      @AuraEnabled
    public static List<OrderItemProcessWrapper> getIssuedWorkOrder(string department){
        List<Job_Process__c> lst= [SELECT Id,Name,Remarks__c,Delivery_Date__c,Sequence__c, Job__r.Quantity__c,Job__r.SKU__r.Name,
      Job__r.Id, Job__r.Order__r.OrderNumber,Job__r.Unit_Of_Measurement__c,Job__r.Remarks__c  FROM Job_Process__c
      WHERE Job__r.SKU_Process__c = :department and Status__c='Processed'];
        List<OrderItemProcessWrapper> lstItemProcess = new List<OrderItemProcessWrapper>();
        for(Job_Process__c i : lst)
        {
            OrderItemProcessWrapper item= new OrderItemProcessWrapper();
            item.JobId= i.Job__r.Id;
            item.SKU=i.Job__r.SKU__r.Name;
            item.OrderNumber=i.Job__r.Order__r.OrderNumber;
            item.Quantity= Integer.valueOf(i.Job__r.Quantity__c);
            item.UnitOfMeasurement= i.Job__r.Unit_Of_Measurement__c;
            item.DeliveryDate= i.Delivery_Date__c;
            item.Remarks= i.Job__r.Remarks__c;
            item.MillRemarks=i.Remarks__c;
            lstItemProcess.add(item);

        }
        return lstItemProcess;
    }

    @AuraEnabled
    public static void CreateWorkOrder(string jobProcessId, string comments)
    {
        Job_Process__c jp= new Job_Process__c();
        jp.Id= jobProcessId;
        jp.Remarks__c= comments;
        jp.Status__c= 'Processed';
        update jp;
    }



}