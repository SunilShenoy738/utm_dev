public class OpportunityService {
    public static Map<Id, Id> getOpportunityAccountId (Set<id> idset) {
        Map<Id, Id> res = new Map<Id, Id> ();
        for (Opportunity oppObj : [Select Id, Account.Id from Opportunity where id in : idset]) {
            res.put (oppObj.Id, oppObj.Account.Id);
        }
        return res;
    }
}