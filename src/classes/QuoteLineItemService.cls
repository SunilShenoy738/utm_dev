public class QuoteLineItemService {

    //get list of QuoteLineItems from Opportuntiy Id
    public static List<QuoteLineItem> getQliFromOpportunityIds(Set<Id> oppIds) {
        return [Select Id, QuoteId, Quote.OpportunityId, Product2Id, Quantity, 
                UnitPrice, Agent_Commission_Percentage__c, Agent_Commission_in_Rs__c, 
                Cost_Type__c, Freight__c, Net_Profit__c, Rate__c, Rate_Type__c, Total__c 
                From QuoteLineItem Where Quote.OpportunityId IN : oppIds and Quote.IsSyncing = true];
    }
     
     //Added by Baibhav
    public  static Map<id,list<QuoteLineItem>> getMapQuoteLineItemByQuoteId(Set<Id> quoteIds)
    {
        List<Quote> quotList = [Select id,Name,(Select Id, UnitPrice, Quantity, PriceBookEntryId, Product2Id, Agent_Commission_Percentage__c, Agent_Commission_in_Rs__c, 
                    Cost_Type__c, Freight__c, Net_Profit__c, Rate__c, Rate_Type__c, Total__c, Dont_Sync__c
                    from QuoteLineItems) From Quote Where id =:quoteIds];

        Map<id,list<QuoteLineItem>> quotlinMap = new Map<id,list<QuoteLineItem>>();
        for(Quote qu: quotList)
        {
            quotlinMap.put(qu.id,qu.QuoteLineItems);
        }
        return quotlinMap;  
    }

}