public class PurchaseOrderService {
	
	public static List<Purchase_Order__c> getPOsWithLineItemsByAccountId(Id accId) {
        System.debug('Querying data');
        return [Select Id, Name, CreatedDate, 
                (Select Id, Name, Purchase_Order__c, Grade__c, Twist_Type__c, Colour_Name__c, Quantity__c, Unit_Of_Measurement__c, Purchase_Order__r.Name, Rate__c, SKU__r.Name, SKU__r.RecordTypeId, SKU__r.RecordType.Name, SKU__r.ProductCode, SKU__r.Twist_Type__c, SKU__r.Grade__c, SKU__r.Colour_Name__c, SKU__r.ColourName__c, Received_Quantity__c, Remaining_Quantity__c, Yarn_State__c, Total__c
                 from Purchase_Order_Items__r where Status__c != :Constants.ORDER_ITEM_STATUS_COMPLETED) 
                from Purchase_Order__c where Supplier__c = :accId And Status__c = :Constants.ORDER_STATUS_APPROVED];
    }

    //add company or inventory unit
	/*public static List<Purchase_Order__c> getPOsWithLineItemsByAccountId(Id accId, Id invtUnitId) {
        return [Select Id, Name, Order_Date__c, 
                (Select Id, Name, Order__c, Quantity_in_Pc__c, Quantity_in_Kg__c, Order__r.Name, Rate_Kg__c, SKU__r.Name, SKU__r.RecordTypeId, SKU__r.ProductCode, Type_of_Measurement_Unit__c, Actual_Quantity_in_Kg__c, Actual_Quantity_in_Pc__c 
                 from Order_Line_Items__r where Status__c != :Constants.ORDERITEM_STATUS_COMPLETED) 
                from Order__c where Account__c = :accId And Status__c = :Constants.CUSTOM_ORDER_STATUS_CONFIRMED And RecordType.Name = :Constants.RT_ORDER_PURCHASE_ORDER And Inventory_Unit__c = :invtUnitId];
    }*/
    
    /*public static List<Purchase_Order__c> getOrderAndItemsByIds(Set<Id> ordIds) {
        return [Select Id, Account__c, Inventory_Unit__c, Status__c, 
                (Select Id, SKU__c, Status__c, Quantity_in_Kg__c, Quantity_in_Pc__c, Actual_Quantity_in_Kg__c, Actual_Quantity_in_Pc__c, Type_of_Measurement_Unit__c, Wt_Pc__c, Inventory_Unit__c from Order_Line_Items__r) 
                from Order__c where Id in: ordIds];
    }*/
}