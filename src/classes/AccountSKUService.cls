public with sharing class AccountSKUService {
	public AccountSKUService() {
		
	}
	public static Map<Id, Account_SKU__c>  getAccountSKUByProductId (Set<Id> skuIds) {
		Map <Id, Account_SKU__c> res = new Map <Id, Account_SKU__c> () ;
		for (Account_SKU__c accSKUObj : [Select Id, SKU__c from Account_SKU__c where SKU__c in : skuIds ]) {
			res.put (accSKUObj.SKU__C, accSKUObj);
		}
		return res;
	} 
}