public with sharing class YarnController {
@AuraEnabled
  public static List<Product2> getWeftDetail(string weftId){
      List<Product2> product= [SELECT  Id, Name, Denier__r.Dinner__c ,Colour_Name__r.Name,Ply__c, Total_Physical_Weight__c, PPC__c, Size_Picks_Repeat__c, Cost_Per_Kg__c, Degum_Percentage__c, Yarn_State__c, Quality_Name__r.Name FROM Product2 where id = :weftId];
      return product;
  }
  @AuraEnabled
  public static String getOrderUOM(){
       List<SelectOption> options = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = OrderItem.Unit_Of_Measurement__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      string res=JSON.serialize(ple);
      return res;
  }

  
   @AuraEnabled
  public static List<Composition__c> getCompositeCount(string skuId)
  {
      return [SELECT Id,Yarn__r.Name,Composition_Percentage__c FROM Composition__c WHERE SKU__c = :skuId];
  }
   @AuraEnabled
  public static void saveCompositeYarn(string lstCompositeYarns)
  {
      List<Composition__c> objComp = (List<Composition__c>)JSON.deserialize(lstCompositeYarns, List<Composition__c>.class);
     string recId= [SELECT Id FROM RecordType WHERE SobjectType='Composition__c' and Name='Yarn' LIMIT 1].Id;
     for(Composition__c c : objComp)
     {
      c.RecordTypeId= recId;
     }
      insert objComp;
      
  }
  
   @AuraEnabled
  public static Integer getBomCount(string fabricId){
      List<AggregateResult> results= [SELECT count(Id)  FROM Bill_of_Material__c where Fabric__c = :fabricId];
     return Integer.valueOf(results[0].get('expr0'));
      
  }
    @AuraEnabled
  public static List<Quality_Master__c> getQualityList(){
      List<Quality_Master__c> lstQuality= new List<Quality_Master__c>();
      lstQuality= [SELECT  Id,Name FROM Quality_Master__c ];
      return lstQuality;
  }

  @AuraEnabled
  public static string getConstituentsofYarn(){
      List<SelectOption> options = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Composition__c.Constituents__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      string res=JSON.serialize(ple);
      return res;
  }
  
  @AuraEnabled
  public static String saveBOM(string lstWarp,string lstWeft, Product2 product) {

      List<RecordType> lstRecType= [SELECT Id,Name FROM RecordType WHERE SobjectType='Bill_of_Material__c'];
      Id WarpRecType;
      Id WeftRecType;
      if(lstRecType[0].Name.toUpperCase()=='WARP')
      {
          WarpRecType=lstRecType[0].Id;
          WeftRecType=lstRecType[1].Id;
      }
      else
      {
          WarpRecType=lstRecType[1].Id;
          WeftRecType=lstRecType[0].Id;
      }

       List<Bill_of_Material__c> objWarp = (List<Bill_of_Material__c>)JSON.deserialize(lstWarp, List<Bill_of_Material__c>.class);
      List<Bill_of_Material__c> objWeft = (List<Bill_of_Material__c>)JSON.deserialize(lstWeft, List<Bill_of_Material__c>.class);
      
      for(Bill_of_Material__c w : objWarp)
      {
          w.RecordTypeId=WarpRecType;
      }
      for(Bill_of_Material__c w : objWeft)
      {
          w.RecordTypeId=WeftRecType;
      }

      insert objWarp;
      insert objWeft;
      update product;
      return 'success';
  }

  @AuraEnabled
  public static List<SKU_Process__c> getRMCost(List<String> SKUs){
      
  List<SKU_Process__c> skuList= [SELECT SKU__c,Yarn_State__c,Cost_Per_Kg__c FROM SKU_Process__c WHERE SKU__c IN :SKUs];
   return skuList;
  }
  
  @AuraEnabled
  public static string deleteCompositeYarns(string yarnId){
      List<Composition__c> lstCom=[SELECT Id FROM Composition__c WHERE SKU__c = :yarnId];
      delete lstCom;
      return 'success';
  }
  
  @AuraEnabled
  public static List<Account_SKU__c> getProductsByRecType(string recId){
      Boolean isSampleSKU= false;
      String idPrefix = recId.substring(0,3);
      string accountId;
      if(idPrefix=='001')
      {
          accountId=recId;
          //Account
      }
      if(idPrefix=='006'){
          Opportunity opp= [SELECT Opportunity.AccountId FROM Opportunity WHERE Id= :recId];
          accountId= opp.AccountId;
          //Opportunity
      }
      if(idPrefix =='0Q0')
      {
          Quote quo= [SELECT Quote.OpportunityId FROM Quote WHERE Id= :recId];
           Opportunity opp= [SELECT Opportunity.AccountId FROM Opportunity WHERE Id= :quo.OpportunityId];
          accountId= opp.AccountId;
          //Quote
      }
      if(idPrefix == '801')
      {
           Order ord = [SELECT AccountId, RecordTypeId FROM Order WHERE Id =: recId];
           accountId= ord.AccountId;
            List<RecordType> lstRecType= [SELECT Id,Name FROM RecordType WHERE SobjectType='Order' AND Name='Sample Order'];

           if(lstRecType[0].Id == ord.RecordTypeId)
           isSampleSKU= true;
           //Order
      }
      List<Product2> prod= new List<Product2>();
      /*
      List<Account_SKU__c> lstAccSku=[SELECT SKU__c,Agent_Commission_Percentage__c,
      SKU__r.Id,SKU__r.Name,SKU__r.Express_Freight__c,SKU__r.Custom_Freight__c,SKU__r.Standard_Freight__c,
      SKU__r.Custom_Adjustment__c,SKU__r.Express_Adjustment__c,SKU__r.Standard_Adjustment__c,
      SKU__r.Custom_Net_Profit__c,SKU__r.Express_Net_Profit__c,SKU__r.Standard_Net_Profit__c,
      SKU__r.Base_Fabric_Custom_FOB_Cost__c,SKU__r.Base_Fabric_Express_FOB_Cost__c,SKU__r.Base_Fabric_Standard_FOB_Cost__c 
      FROM Account_SKU__c WHERE Account__c = :accountId];
      */

      List<Account_SKU__c> lstAccSku=[SELECT SKU__c,SKU__r.RecordType.Name,Agent_Commission_Percentage__c,
      SKU__r.Id,SKU__r.Name,SKU__r.Express_Freight__c,SKU__r.Custom_Freight__c,SKU__r.Standard_Freight__c,
      SKU__r.Custom_Adjustment__c,SKU__r.Express_Adjustment__c,SKU__r.Standard_Adjustment__c,
      SKU__r.Custom_Net_Profit__c,SKU__r.Express_Net_Profit__c,SKU__r.Standard_Net_Profit__c,
      SKU__r.Base_Fabric_Custom_FOB_Cost__c,SKU__r.Base_Fabric_Express_FOB_Cost__c,SKU__r.Base_Fabric_Standard_FOB_Cost__c ,
      SKU__r.Express_Total_CIF_Cost__c,SKU__r.Express_Total_FOB_Cost__c,SKU__r.Standard_Total_FOB_Cost__c,
      SKU__r.Standard_Total_CIF_Cost__c,SKU__r.Custom_Total_CIF_Cost__c ,SKU__r.Custom_Total_FOB_Cost__c,                                
      SKU__r.Express_FOB_Cost__c, SKU__r.Express_CIF_Cost__c, 
      SKU__r.Standard_FOB_Cost__c, SKU__r.Standard_CIF_Cost__c, 
      SKU__r.Custom_FOB_Cost__c, SKU__r.Custom_CIF_Cost__c

      FROM Account_SKU__c WHERE Account__c = :accountId];
      /*List<string>lstSku = new List<string>();
      for(Account_SKU__c a :lstAccSku){lstSku.add(a.SKU__c);}
      prod=[SELECT Id,Name,Express_Freight__c,Custom_Freight__c,Standard_Freight__c,
      Custom_Adjustment__c,Express_Adjustment__c,Standard_Adjustment__c,
      Custom_Net_Profit__c,Express_Net_Profit__c,Standard_Net_Profit__c,
      Base_Fabric_Custom_FOB_Cost__c,Base_Fabric_Express_FOB_Cost__c,Base_Fabric_Standard_FOB_Cost__c 
      FROM Product2
      WHERE Id IN :lstSku ];
      return prod;*/
      if(isSampleSKU)
      return new List<Account_SKU__c>();
      else
      return lstAccSku;
       
  }
   @AuraEnabled
  public static List<Product2> getSKUDetails(string weftId){
      List<Product2> product= [SELECT  Id,Name,RecordType.Name,Express_Freight__c,Custom_Freight__c,Standard_Freight__c,
      Custom_Adjustment__c,Express_Adjustment__c,Standard_Adjustment__c,
      Custom_Net_Profit__c,Express_Net_Profit__c,Standard_Net_Profit__c,
      Base_Fabric_Custom_FOB_Cost__c,Base_Fabric_Express_FOB_Cost__c,Base_Fabric_Standard_FOB_Cost__c ,
      Express_FOB_Cost__c,Express_CIF_Cost__c,Standard_FOB_Cost__c, Standard_CIF_Cost__c, 
      Custom_FOB_Cost__c,Custom_CIF_Cost__c,Express_Total_CIF_Cost__c,Express_Total_FOB_Cost__c,Standard_Total_FOB_Cost__c,
      Standard_Total_CIF_Cost__c,Custom_Total_CIF_Cost__c ,Custom_Total_FOB_Cost__c



      FROM Product2 where id = :weftId];
      return product;
  }

  @AuraEnabled
  public static string saveSKUs(string recId, String skuString){
      List<SkuWrapper> skuList = (List<SkuWrapper>)JSON.deserialize(skuString, List<SkuWrapper>.class);
      //Added by Baibhav
       /* Set<id> qotIdSet = new Set<id>();
      Set<id> oppIdSet = new Set<id>();
      for(SkuWrapper skw:skuList)
      {
        String idprefix=skw.Id.substring(0,3);
        if(idprefix=='006')
        {
          oppIdSet.add((Id)skw.Id);
        }
        if(idprefix=='0Q0')
        {
          qotIdSet.add((Id)skw.Id);
        }
      }*/

  //    Map<id,list<OpportunityLineItem>> opplineMap = OpportunityLineItemService.getMapOpportunityLineItemByOpportunityId(oppIdSet);
  //    Map<id,list<QuoteLineItem>> quotlineMap = QuoteLineItemService.getMapQuoteLineItemByQuoteId(qotIdSet);

      list<OpportunityLineItem> updateOpplinList = new list<OpportunityLineItem>();
       list<OrderItem> updateOrderlinList = new list<OrderItem>();
      list<QuoteLineItem> updatequotlinList = new list<QuoteLineItem>();
      list<Account_SKU__c> insertaccSku = new list<Account_SKU__c>();
       
        String idprefix=recId.substring(0,3);
        id pb=(Id)recId;
        if(idprefix=='006')
        {
            Opportunity opp= [select id,Pricebook2Id,AccountId,CurrencyIsoCode  from Opportunity where id=:recId limit 1];

            for(SkuWrapper skw:skuList)
            {
               OpportunityLineItem olt = new OpportunityLineItem();

               if(skw.Id != null && skw.Id != '')
              {

                PricebookEntry pbe = [SELECT Id,Product2Id,CurrencyIsoCode,Pricebook2Id,UnitPrice  FROM PricebookEntry where Product2Id =:skw.Id];

               olt.ServiceDate = System.today();
               olt.PricebookEntryId = pbe.id;
               olt.OpportunityId=opp.id;
           
               if(skw.SelectedCost!=null)
               olt.Cost_Type__c = skw.SelectedCost;
               
               if(skw.Rate!=null)
               olt.Rate__c = skw.Rate;

               if(skw.SelectedRate!=null)
               olt.Rate_Type__c = skw.SelectedRate;
               
               if(skw.Freight!=null)
               olt.Freight__c = skw.Freight;

               if(skw.Adjustment!=null)
               olt.Adjustment__c = skw.Adjustment;

               if(skw.NetProfit!=null)
               olt.Net_Profit__c = skw.NetProfit;

               if(skw.Total!=null)
               olt.Total__c = skw.Total;

               if(skw.Qty!=null)
               olt.Quantity = skw.Qty;

               olt.UnitPrice  = pbe.UnitPrice;

               if(skw.CustomerProduct == false)
                {
                  Account_SKU__c accsku =  new Account_SKU__c();
                  accsku.Account__c = opp.AccountId;
                  accsku.SKU__c=skw.id;
                  insertaccSku.add(accsku);
                  
                }

               updateOpplinList.add(olt);
            }
         }
        }

        if(idprefix=='801')
        {
            Order ord= [select id,AccountId,Pricebook2Id,CurrencyIsoCode,RecordType.Name, EffectiveDate  from Order where id=:recId];

            for(SkuWrapper skw:skuList)
            {
               OrderItem olt = new OrderItem();

               if(skw.Id != null && skw.Id != '')
              {

                PricebookEntry pbe = [SELECT Id,Product2Id,UnitPrice  FROM PricebookEntry where Product2Id =:skw.Id];

               olt.ServiceDate = ord.EffectiveDate;
               olt.PricebookEntryId = pbe.id;
               olt.orderId=ord.id;
               if(skw.SelectedCost!=null)
               olt.Cost_Type__c = skw.SelectedCost;
               
               if(skw.Rate!=null)
               olt.Rate__c = skw.Rate;

               if(skw.SelectedRate!=null)
               olt.Rate_Type__c = skw.SelectedRate;
               
               if(skw.Freight!=null)
               olt.Freight__c = skw.Freight;

               if(skw.Adjustment!=null)
               olt.Adjustment__c = skw.Adjustment;

               if(skw.NetProfit!=null)
               olt.Net_Profit__c = skw.NetProfit;

               if(skw.Total!=null)
               olt.Total__c = skw.Total;

               if(skw.Qty!=null)
               olt.Quantity = skw.Qty;

               if(skw.Comments!=null)
               olt.Description = skw.Comments;

               

               olt.UnitPrice  = pbe.UnitPrice;

               if(skw.CustomerProduct == false && ord.RecordType.Name != Constants.RT_ORDER_STOCK_ORDER)
                {
                  Account_SKU__c accsku =  new Account_SKU__c();
                  accsku.Account__c = ord.AccountId;
                  accsku.SKU__c=skw.id;
                  insertaccSku.add(accsku);
                  
                }

               updateOrderlinList.add(olt);  
            }
         }
        }
        if(idprefix=='0Q0')
        {
            Quote opp= [select id,AccountId,Pricebook2Id,CurrencyIsoCode from Quote where id=:recId];

            for(SkuWrapper skw:skuList)
            {
               QuoteLineItem olt = new QuoteLineItem();

               if(skw.Id != null && skw.Id != '')
              {

                PricebookEntry pbe = [SELECT Id,Product2Id,UnitPrice  FROM PricebookEntry where Product2Id =:skw.Id];

               olt.ServiceDate = System.today();
               olt.PricebookEntryId = pbe.id;
                olt.QuoteId=opp.id;
               if(skw.SelectedCost!=null)
               olt.Cost_Type__c = skw.SelectedCost;
               
               if(skw.Rate!=null)
               olt.Rate__c = skw.Rate;

              if(skw.SelectedRate!=null)
               olt.Rate_Type__c = skw.SelectedRate;
               
               if(skw.Freight!=null)
               olt.Freight__c = skw.Freight;

               if(skw.Adjustment!=null)
               olt.Adjustment__c = skw.Adjustment;

               if(skw.NetProfit!=null)
               olt.Net_Profit__c = skw.NetProfit;

               if(skw.Total!=null)
               olt.Total__c = skw.Total;

               if(skw.Qty!=null)
               olt.Quantity = skw.Qty;

              olt.UnitPrice  = pbe.UnitPrice;

               if(skw.CustomerProduct == false)
                {
                  Account_SKU__c accsku =  new Account_SKU__c();
                  accsku.Account__c = opp.AccountId;
                  accsku.SKU__c=skw.id;
                  insertaccSku.add(accsku);
                  
                }

               updatequotlinList.add(olt); 
            }
         }
        }
      upsert insertaccSku;  
      if(updateOpplinList.size() > 0)
      {
          insert updateOpplinList;
      }
      if(updatequotlinList.size() > 0)
      {
          insert updatequotlinList;
      }
      if(updateOrderlinList.size() > 0)
      {
          insert updateOrderlinList;
      }
      //End
      system.debug(skuList);
      return 'success';
  }

 
}