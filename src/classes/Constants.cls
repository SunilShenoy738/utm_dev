public class Constants
{
    //Lead Object Constants Starts Here--------------------------------------------------------------------
    public static String LEAD_CONVERTED_STATUS = 'Converted';
    //Lead Object Constants Ends Here----------------------------------------------------------------------

    //Product2 Object Constants Starts Here----------------------------------------------------------------
    public static String RT_PRODUCT2_YARN = 'Yarn';
	public static String RT_PRODUCT2_FABRIC = 'Fabric';
	public static String RT_PRODUCT2_VALUE_ADDED = 'Value Added';
	public static String RT_PRODUCT2_SEMI_FINISHED = 'Semi-Finished';
    public static String RT_PRODUCT2_TWISTED_YARN = 'Twisted Yarn';
    public static String PRODUCT_FAMILY_JACQUARD = 'Jacquard';
    public static String PRODUCT_FAMILY_DOBBY = 'Dobby';
    public static String PRODUCT_FAMILY_EMBROIDERY = 'Embroidery';
    public static String PRODUCT_FAMILY_JACQUARD_CODE = 'JJRD';
    public static String PRODUCT_FAMILY_DOBBY_CODE = 'DOBB';
    public static String PRODUCT_FAMILY_EMBROIDERY_CODE = 'ED'; 
    
    public static String PRODUCT_PATTERN_TYPE_NEW = 'New';
    public static String PRODUCT_PATTERN_TYPE_EXISTING = 'Existing';
    public static String PRODUCT_TWIST_TYPE_UNKNOWN = 'Unknown';
    public static String PRODUCT_TWIST_TYPE_UNKNOWN_TWI = 'TWI'; 
    public static String PRODUCT_YARN_TYPE_TWISTED = 'Twisted';
    public static String PRODUCT_YARN_TYPE_UNTWISTED = 'Untwisted';
    //Product2 Object Constanta Ends Here----------------------------------------------------------------------

    //Bill of Material Object Constants Starts Here----------------------------------------------------------------
    public static String RT_BOM_WEFT = 'Weft';
	public static String RT_BOM_WARP = 'Warp';
	//Bill of Material Object Constanta Ends Here-----------------------------------------------------------------

	//Composition Object Constants Starts Here----------------------------------------------------------------
    public static String RT_COMPOSITION_SKU = 'SKU';
	public static String RT_COMPOSITION_QUALITY = 'Quality';
	//Composition Object Constanta Ends Here----------------------------------------------------------------------

    //Department GLobal Picklist Starts Here-----------------------------------------------------------------
    public static String DEPARTMENT_WEAVING = 'Weaving';
    public static String DEPARTMENT_PRINTING = 'Printing';
    public static String DEPARTMENT_EMBROIDERY = 'Embroidery';
    //Department GLobal Picklist Ends Here-----------------------------------------------------------------


    //Process Object Constants Starts Here--------------------------------------------------------------------
    public static String PROCESS_PT_VALUE_ADD = 'Value Add';
    public static String PROCESS_PT_FABRIC = 'Fabric';
    public static String PROCESS_PT_YARN = 'Yarn';
    public static String PROCESS_NAME_DYEING = 'Dyeing';
    public static String PROCESS_NAME_TWISTING = 'Twisting';
    //Process Object Constanta Ends Here----------------------------------------------------------------------

    //Quote Object Constants Starts Here--------------------------------------------------------------------
    public static String QUOTE_STATUS_DRAFT = 'Draft';
    public static String QUOTE_STATUS_ACCEPTED = 'Accepted';
    public static String QUOTE_STATUS_REJECTED = 'Rejected';
    //Quote Object Constanta Ends Here----------------------------------------------------------------------

    //Order Object Constants Starts Here----------------------------------------------------------------
    public static String RT_ORDER_SALES_ORDER = 'Sales Order';
    public static String RT_ORDER_STOCK_ORDER = 'Stock Order';
    public static String RT_ORDER_SAMPLE_ORDER = 'Sample Order';
    public static String RT_ORDER_PURCHASE_ORDER = 'Purchase Order';
    public static String ORDER_STATUS_DRAFT = 'Draft';
    public static String ORDER_STATUS_CREATED = 'Created';
    public static String ORDER_STATUS_CLOSED = 'Closed';
    public static String ORDER_STATUS_REQUESTED = 'Requested';
    public static String ORDER_STATUS_APPROVED = 'Approved';
    public static String ORDER_STATUS_REJECTED = 'Rejected';
    //Order Object Constanta Ends Here----------------------------------------------------------------------

    //Order Line Item Object Constants Starts Here----------------------------------------------------------------
    public static String ORDER_ITEM_STATUS_OPEN = 'Open';
    public static String ORDER_ITEM_STATUS_COMPLETED = 'Completed';
    //Order Line Item Object Constants Ends Here----------------------------------------------------------------------

    //Opportunity Object Constants Starts Here----------------------------------------------------------------
    public static String OPPORTUNTIY_STAGE_CLOSED_WON = 'Closed Won';
    //Opportunity Object Constanta Ends Here----------------------------------------------------------------------

    //Expense Object Constants Starts Here--------------------------------------------------------------------
    public static String EXPENSE_APPROVAL_STATUS_APPROVED = 'Approved';
    //Expense Object Constants Ends Here----------------------------------------------------------------------
    
}