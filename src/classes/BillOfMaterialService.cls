public class BillOfMaterialService {
	
	public static List<Bill_Of_Material__c> getBomBySKUId(Id skuId)
	{
		return [Select Id,Name,Yarn__c,Yarn__r.Name,Total_RM_Cost_Kg__c,Costing_Wt_Mtr__c,Cost_Per_Meter__c From Bill_Of_Material__c Where Fabric__c =:skuId];
	}

	public static Id recordTypeByName(String rtName) {
        return Schema.SObjectType.Bill_Of_Material__c.getRecordTypeInfosByName().get(rtName).getRecordTypeId();
    }
}