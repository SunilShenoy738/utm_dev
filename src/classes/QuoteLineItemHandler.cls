public with sharing class QuoteLineItemHandler {
	
	public static void onBeforeInsert (List<QuoteLineItem> newList) {
		updateMinShippmentData (newList);		
	}

	public static void updateMinShippmentData (List<QuoteLineItem> newList) {
		System.debug('Quote Line Item :' + newList);
		Set<Id> quoteIds = new Set<Id> ();
		for (QuoteLineItem item : newList) {
			quoteIds.add (item.QuoteId);
		}

		if (quoteIds.size() > 0) {
			Map <Id, Quote> quoteData = new Map <Id, Quote> ();
			
			quoteData = QuoteService.getQuotesMap (quoteIds);
			System.debug('Quote Data :' + quoteData);
			for (QuoteLineItem item : newList) {
				if (quoteData.containsKey (item.QuoteId)) {
					item.Minimum_Quantity_Per_Shipment__c = quoteData.get (item.QuoteId).Minimum_Quantity_Per_Shipment__c;
				}
			}
		}
		
	}
}