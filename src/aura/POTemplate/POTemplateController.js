({
    getSupplierListCtrl : function(cmp) {
         var action = cmp.get("c.getSupplierList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.supplierList',yarns);
            }
        });
        $A.enqueueAction(action);
    }
})