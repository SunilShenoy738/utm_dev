({
  
     InitHelper: function(component, event, helper) {
        var action = component.get("c.getJobsByDepartment");
        action.setParams({department : "Printing" });
        console.log("AAAA");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
               var lst=data.getReturnValue();
               component.set("v.orderItems",lst);
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
     InitJobsHelper: function(component, event, helper) {
     
        var action = component.get("c.getJobOperationDetails");
        //action.setParams({orderId : component.get("v.recordId") });
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
            
               var lst=data.getReturnValue();
               console.log("JOBS***");
               console.log(lst);
               component.set("v.PlannedOrderItems",lst);
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
    getItemDetailHelper: function(component, event, helper) {
      var action = component.get("c.GetJobProcesses");
     var lst=  component.get("v.orderItems");
     var index= parseInt(component.get("v.selectedItem"),10);
     console.log("index:" + index);
     console.log(lst[index]);
      action.setParams({orderItemData : "[" + JSON.stringify(lst[index]) + "]" });
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
               var d=data.getReturnValue();
               console.log(d);
               component.set("v.ItemProcess",d);
                
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
    },
    saveJobHelper: function(component, event, helper){
      var lst = component.get("v.ItemProcess");
       var action = component.get("c.saveJobOperations");
       var lstSelected= new Array();
       var jId=lst[0].JobId;
       for(var i in lst)
       {
        if(lst[i].IsSelected)
        {
          var item=new Object();
          item.DeliveryDate= lst[i].DeliveryDate;
          item.Remarks= lst[i].Remarks;
          item.ProcessNumber= lst[i].ProcessNumber;
          item.JobId= jId;
          lstSelected.push(item);
        }
       }
      action.setParams({jobData : JSON.stringify(lstSelected), jobId:jId});
       action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
              var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Process Saved","message": "Mill Plan Save successfully","type":"success"});
            resultsToast.fire();
             component.set("v.isOpen", false); 
              window.parent.location ="/";   
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
      
    },

    
    
    
})