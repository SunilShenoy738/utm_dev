({
  

  GetUOMHelper:function(component, event, helper){
      var action = component.get("c.getOrderUOM");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                component.set("v.UOMList",JSON.parse(data.getReturnValue()));
             } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
  },
     InitHelper: function(component, event, helper) {
        var action = component.get("c.getProductsByRecType");
        action.setParams({recId :component.get("v.recordId")});
        var lstYarns = component.get("v.SkuList");

        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
               var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++)
                {
                    lst[i].Id= lst[i].SKU__r.Id;
                    lst[i].Name=lst[i].SKU__r.Name;


                   lst[i].SelectedCost='Standard';
                    lst[i].SelectedRate='FOB';
                    lst[i].SelectedUOM='Kg';
                    lst[i].AgentPercentage=0;
                    lst[i].AgentAmount=0;
                    lst[i].RatePerYard=0;
                    lst[i].Width           =0;
                    lst[i].Rate            =0;
                    lst[i].Freight         =0;
                    lst[i].Adjustment      =0;
                    lst[i].NetProfit       =0;
                    lst[i].Total           =0;
                    lst[i].Qty             =1;
                    lst[i].CustomerProduct = true;
                    lst[i].Comments='';
                    lstYarns.push(lst[i]);
                }
                console.log('**DDDD***');
               component.set("v.SkuList",lstYarns);
               this.setCostingHelper(component,event,helper);
                
                
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
    doCalculationHelper : function(component, event, helper){
    var lst = component.get("v.SkuList");
     
      for(var i in lst)
      {
        var grossTotal= parseFloat(lst[i].Rate) + parseFloat(lst[i].Freight) + parseFloat(lst[i].Adjustment) ;
        var np= grossTotal * parseFloat(lst[i].NetProfit) / 100;
        lst[i].Total= grossTotal + np;

      }
       component.set("v.SkuList",lst);
    },
    setCostingHelper: function(component, event, helper){
      //var selVal= event.getSource().get("v.value");
     // var i=parseInt(event.getSource().get("v.class"),10);
      var lst = component.get("v.SkuList");
     for(var i=0;i<lst.length;i++)
     {



     var selVal= lst[i].SelectedCost;
     var selRateType=lst[i].SelectedRate;

      if(lst[i].CustomerProduct)
      {
        if(selVal=="Standard")
        {

          if(selRateType == "CIF")
          {
            lst[i].Rate       =   (lst[i].SKU__r.Standard_CIF_Cost__c==undefined)? 0 : lst[i].SKU__r.Standard_CIF_Cost__c;
            lst[i].Freight    =   (lst[i].SKU__r.Standard_Freight__c == undefined) ? 0 : lst[i].SKU__r.Standard_Freight__c;
       
          }
          else
          {
            lst[i].Rate       =   (lst[i].SKU__r.Standard_FOB_Cost__c==undefined)? 0 : lst[i].SKU__r.Standard_FOB_Cost__c;
            lst[i].Freight    =   0;
          }

             lst[i].Adjustment =   (lst[i].SKU__r.Standard_Adjustment__c == undefined) ? 0 : lst[i].SKU__r.Standard_Adjustment__c;
             lst[i].NetProfit  =   (lst[i].SKU__r.Standard_Net_Profit__c == undefined) ? 0 : lst[i].SKU__r.Standard_Net_Profit__c;
            
        }
        if(selVal=="Custom")
        {
            if(selRateType == "CIF")
            {
              lst[i].Rate       =  (lst[i].SKU__r.Custom_CIF_Cost__c ==undefined) ? 0: lst[i].SKU__r.Custom_CIF_Cost__c;
              lst[i].Freight    =  (lst[i].SKU__r.Custom_FOB_Cost__c == undefined) ? 0 : lst[i].SKU__r.Custom_FOB_Cost__c

            }
            else
            {
                lst[i].Rate       =  (lst[i].SKU__r.Base_Fabric_Custom_FOB_Cost__c ==undefined) ? 0: lst[i].SKU__r.Base_Fabric_Custom_FOB_Cost__c;
                lst[i].Freight    = 0;
            }

            lst[i].NetProfit  =  (lst[i].SKU__r.Custom_Net_Profit__c ==undefined) ? 0 : lst[i].SKU__r.Custom_Net_Profit__c;
            lst[i].Adjustment =  (lst[i].SKU__r.Custom_Adjustment__c == undefined) ? 0 : lst[i].SKU__r.Custom_Adjustment__c;
          }
        if(selVal=="Express")
        {
            if(selRateType == "CIF")
            {
              lst[i].Freight    =  (lst[i].SKU__r.Express_CIF_Cost__c == undefined) ? 0 : lst[i].SKU__r.Express_CIF_Cost__c;
            }
            else
            {
              lst[i].Freight    =  0; //(lst[i].SKU__r.Express_Freight__c == undefined) ? 0 : lst[i].SKU__r.Express_Freight__c;
            }

          lst[i].Adjustment =  (lst[i].SKU__r.Express_Adjustment__c == undefined) ? 0 : lst[i].SKU__r.Express_Adjustment__c;
          lst[i].NetProfit  =  (lst[i].SKU__r.Express_Net_Profit__c ==undefined) ? 0 : lst[i].SKU__r.Express_Net_Profit__c;
          lst[i].Rate       =  (lst[i].SKU__r.Express_FOB_Cost__c == undefined) ? 0 : lst[i].SKU__r.Express_FOB_Cost__c;
        }
        lst[i].Total= parseFloat(lst[i].Rate) + parseFloat(lst[i].Freight) + parseFloat(lst[i].Adjustment) + parseFloat(lst[i].NetProfit);
      }
      else
      {
        if(selVal=="Standard")
        {
          if(selRateType == "CIF")
          {
            lst[i].Rate       =   (lst[i].Standard_CIF_Cost__c==undefined)? 0 : lst[i].Standard_CIF_Cost__c;
            lst[i].Freight    =   (lst[i].Standard_Freight__c == undefined) ? 0 : lst[i].Standard_Freight__c;
         
          }
          else
          {
            lst[i].Rate       =   (lst[i].Standard_FOB_Cost__c==undefined)? 0 : lst[i].Standard_FOB_Cost__c;
            lst[i].Freight    =   0;
         
          }

          lst[i].Adjustment =   (lst[i].Standard_Adjustment__c == undefined) ? 0 : lst[i].Standard_Adjustment__c;
          lst[i].NetProfit  =   (lst[i].Standard_Net_Profit__c == undefined) ? 0 : lst[i].Standard_Net_Profit__c;
            
        }
        if(selVal=="Custom")
        {
          if(selRateType == "CIF")
          {
            lst[i].Rate       =  (lst[i].Custom_CIF_Cost__c ==undefined) ? 0: lst[i].Custom_CIF_Cost__c;
            lst[i].Freight    =  (lst[i].Custom_FOB_Cost__c == undefined) ? 0 : lst[i].Custom_FOB_Cost__c
          }
          else
          {
            lst[i].Rate       =  (lst[i].Base_Fabric_Custom_FOB_Cost__c ==undefined) ? 0: lst[i].Base_Fabric_Custom_FOB_Cost__c;
            lst[i].Freight    =  0;
          }

          lst[i].NetProfit  =  (lst[i].Custom_Net_Profit__c ==undefined) ? 0 : lst[i].Custom_Net_Profit__c;
          lst[i].Adjustment =  (lst[i].Custom_Adjustment__c == undefined) ? 0 : lst[i].Custom_Adjustment__c;
          
        }
        if(selVal=="Express")
        {
          if(selRateType == "CIF")
          {
            lst[i].Freight    =  (lst[i].Express_CIF_Cost__c == undefined) ? 0 : lst[i].Express_CIF_Cost__c;
          }
          else
          {
            lst[i].Freight    = 0; //(lst[i].Express_Freight__c == undefined) ? 0 : lst[i].Express_Freight__c;
          }

          lst[i].Adjustment =  (lst[i].Express_Adjustment__c == undefined) ? 0 : lst[i].Express_Adjustment__c;
          lst[i].NetProfit  =  (lst[i].Express_Net_Profit__c ==undefined) ? 0 : lst[i].Express_Net_Profit__c;
          lst[i].Rate       =  (lst[i].Express_FOB_Cost__c == undefined) ? 0 : lst[i].Express_FOB_Cost__c;
        }
        var grossTotal= parseFloat(lst[i].Rate) + parseFloat(lst[i].Freight) + parseFloat(lst[i].Adjustment) ;
        var np= grossTotal * parseFloat(lst[i].NetProfit) / 100;
        lst[i].Total= grossTotal + np;
      }

    }
    component.set("v.SkuList",lst);
      
    },
    addYarnHelper: function(component, event, helper){
      var lstYarns = component.get("v.SkuList");
      var action = component.get("c.getSKUDetails");
      var wId= component.get("v.selectedYarn");
      action.setParams({weftId : wId});
      action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++)
                {
                    lst[i].SelectedCost='Standard';
                    lst[i].SelectedRate='FOB';
                    lst[i].SelectedUOM='Kg';
                    lst[i].AgentPercentage=0;
                    lst[i].AgentAmount=0;
                    lst[i].RatePerYard=0;
                    lst[i].Width=0;
                    lst[i].Rate=0;
                    lst[i].Freight=0;
                    lst[i].Adjustment=0;
                    lst[i].NetProfit=0;
                    lst[i].Total=0;
                    lst[i].Qty=1;
                    lst[i].CustomerProduct= false;
                    lst[i].Comments='';
                    lstYarns.push(lst[i]);
                }

                component.set("v.SkuList",lstYarns);
                console.log('**ALL YARNS**')
                console.log(lstYarns);
               this.setCostingHelper(component,event,helper);

             } else if (data.getState() === "ERROR") { $A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
    },
    removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.SkuList");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.SkuList",lst);
    },
    saveYarnHelper : function(component, event, helper) {
       var action = component.get("c.saveSKUs");
      var lst = component.get("v.SkuList");
      var lstSku = new Array();

      for(var i in lst)
      {
        lstSku.push({Id:lst[i].Id,SelectedCost:lst[i].SelectedCost,Rate:lst[i].Rate,Freight:lst[i].Freight,
          Adjustment:lst[i].Adjustment,NetProfit:lst[i].NetProfit,Total:lst[i].Total,
          Qty:lst[i].Qty,CustomerProduct:lst[i].CustomerProduct,Comments:lst[i].Comments,
          SelectedRate:lst[i].SelectedRate,SelectedUOM:lst[i].SelectedUOM,Width:lst[i].Width,
          AgentPercentage:lst[i].AgentPercentage,AgentAmount: lst[i].AgentAmount,RatePerYard:lst[i].RatePerYard});

      }

     
      console.log(JSON.stringify(lstSku));
      action.setParams({recId: component.get("v.recordId") , skuString :JSON.stringify(lstSku)});
      action.setCallback(this, function(data) {
        if (data.getState() === "SUCCESS") {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "SKU Saved","message": "SKU Save successfully","type":"success"});
            resultsToast.fire();
            setTimeout(function(){ window.parent.location = '/' + component.get("v.recordId"); }, 2000);
         } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        
        $A.enqueueAction(action);
         
        

       
     }
    
    
    
})