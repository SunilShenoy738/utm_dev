({

getAllFabricHelper: function(component, event, helper) {

        var action = component.get("c.getAllFabric");
         action.setParams({productId : component.get('v.recordId')});
        
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d= data.getReturnValue();
                d.DesignSource=JSON.parse(d.DesignSource);
                d.StateOfYarn=JSON.parse(d.StateOfYarn);
                d.Reed= JSON.parse(d.Reed);
                d.Beams= JSON.parse(d.Beams);
                d.Warping= JSON.parse(d.Warping);
                d.BeamPosition= JSON.parse(d.BeamPosition);
                 d.Loom= JSON.parse(d.Loom);
                 component.set("v.lstAllObjects",data.getReturnValue());
                   console.log("***ALL DATA***");
                   console.log(d);
                   helper.checkWeftStates(component, event,helper);
                   this.getRMCostHelper(component,event,helper);
                   this.doAllCalculation(component,event,helper);
             } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },

    deleteBOMHelper : function(component, event, helper) {

        var action = component.get("c.deleteBomItems");
         action.setParams({fabricId : component.get('v.recordId')});
        
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({"title": "Bom Deleted","message": "Bill of Matterial Deleted.","type":"success"});
                resultsToast.fire();
                setTimeout(function(){ window.parent.location = '/' + component.get("v.recordId"); }, 2000);

             } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getRMCostHelper: function(component,event,helper){
        var action = component.get("c.getRMCost");
        var lst = component.get("v.RMSKU");
        var oldSku= new Array();
        var newSku= new Array();
        for(var i in lst)
        {
           oldSku.push(lst[i].SKU__c);            
        }
         //var lstWraps = component.get("v.NewWarpYarns");
         var lstAll= component.get("v.lstAllObjects");
         var lstwefts= lstAll.WeftList;
         var lstWraps= lstAll.WarpList;
        //a = ['abc','xyz','pqrxyz'].includes('pqrxyz');
         for(var i in lstWraps)
         {
           if(oldSku.includes(lstWraps[i].ProductId)==false && newSku.includes(lstWraps[i].ProductId)==false)
           {
                newSku.push(lstWraps[i].ProductId);
           }
         }
         for(var i in lstwefts)
         {
           if(oldSku.includes(lstwefts[i].ProductId)==false && newSku.includes(lstwefts[i].ProductId)==false)
           {
                newSku.push(lstwefts[i].ProductId);
           }
         }
         var action = component.get("c.getRMCost");
         action.setParams({SKUs:newSku})

          action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var res=data.getReturnValue();
               for(var i in res){lst.push(res[i]); }
                component.set("v.RMSKU",lst);
              
             } else if (data.getState() === "ERROR") {$A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);

         console.log('OLD SKU:' + oldSku.join());
         console.log('NEW SKU:' + newSku.join());
        console.log('ALL COST DATA');
        console.log(lst);
        
    },
    addToWarpHelper: function(component, event, helper){
      //var lstWraps = component.get("v.NewWarpYarns");
      var lstAll= component.get("v.lstAllObjects");
      var lstWraps= lstAll.WarpList;

      var action = component.get("c.getWarpDetail");
      var wId= component.get("v.selectedWarp");
      console.log(wId);
      action.setParams({warpId : wId});
      action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var res=data.getReturnValue();
                for(var i=0;i< res.length;i++){
                  lstWraps.push(res[i]);  
                }
                lstAll.WarpList=lstWraps;
                component.set("v.lstAllObjects",lstAll);
                console.log('**ALL WARP**')
                console.log(lstWraps);
                this.getRMCostHelper(component,event,helper);
                this.doAllCalculation(component, event, helper);
                //this.calculateWarpTotal(component, event, helper);
             } else if (data.getState() === "ERROR") {$A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
    },
    addToWeftHelper: function(component, event, helper){
         var lstAll= component.get("v.lstAllObjects");

      var lstYarns = lstAll.WeftList;
      var newAddWeft=component.get("v.newAddedWeft");
      var action = component.get("c.searchWeft");
      var wId= component.get("v.selectedWeft");
      var indexNo=(lstYarns==undefined)?0 : lstYarns.length;
     
      action.setParams({weftId : wId});
      action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++)
                {
                    lstYarns.push(lst[i]);
                    indexNo=indexNo + 1;
                    newAddWeft.push(indexNo);
                }

                lstAll.WeftList=lstYarns;
                component.set("v.lstAllObjects",lstAll);
                component.set("v.newAddedWeft",newAddWeft);
                
                console.log('**ALL WEFT**')
                console.log(lstYarns);

                this.getRMCostHelper(component,event,helper);
                
                this.doAllCalculation(component, event, helper);
                helper.checkWeftStates(component, event,helper);
             } else if (data.getState() === "ERROR") { $A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
         

    },
    doAllCalculation: function(component, event, helper){
        //Calculate WEFT
        this.calculateWeftPicks(component,event,helper);

        //CALCULATE 
        this.calculateWarpTotal(component,event,helper);
       
    },
   checkWeftStates : function(component, event, helper){
    //lst[i].RecordType.Name
    var arr= component.get("v.newAddedWeft");
    var lstAll= component.get("v.lstAllObjects");
   
    var lst = lstAll.WeftList;
    var lstRMSku = component.get("v.RMSKU");
    for(var i in arr){
        var int=parseInt(arr[i],10)-1;
       var className="slds-checkbox_" + int;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && 
                (component.find("dpc")[cnt].get("v.label") == "Dyed" || component.find("dpc")[cnt].get("v.label") == "Twisted")){
               
                component.find("dpc")[cnt].set("v.value",true);

                if(lst[int].RecordTypeName=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted")
                {
                    component.find("dpc")[cnt].set("v.disabled",true);
                }
               



            }
          }

      }
     // component.set("v.NewYarns",lst);

   },
    calculateWarpTotal:function(component, event, helper){
       // this.getRMCostHelper(component,event,helper);
         var total=0;
         var totalWarpWeight=0;
         var totalWarpCosting=0;
         var totalWtCr=0;
         var totalCostCr=0;
         var totalEnds=0;

        var lstAll= component.get("v.lstAllObjects");

         var lst= lstAll.WarpList;
         var lstRMSku = component.get("v.RMSKU");

         var crimpage=  ((lstAll.CalculatedHeight - lstAll.ActualHeight ) * 100 ) / lstAll.ActualHeight;
         console.log('crimpage:' + crimpage)
         crimpage= parseFloat(crimpage.toFixed(2));
         for(var i in lst){
            var ends =parseInt(lst[i].Noofends,10);
            totalEnds= totalEnds + ends;
            var dinner=parseInt(lst[i].Dinner,10);
            var fabricWidth=lstAll.ActualWidth;
            var warpWastage=lstAll.WarpWastagePercentage;
              
            var weight = parseFloat((ends * dinner /9000).toFixed(2));
            var wastage= (weight*warpWastage/100) + weight;
            wastage= parseFloat(wastage.toFixed(2));
            totalWarpWeight= totalWarpWeight + weight;
            totalWarpCosting= totalWarpCosting + wastage;

            //var selectId= 'warpSelection';
            
            var totalWtCrimpage=(weight*crimpage/100) + weight;
            totalWtCrimpage= parseFloat(totalWtCrimpage.toFixed(2));
            var totalCostCrimpage= (totalWtCrimpage * warpWastage/100) + totalWtCrimpage;
            totalCostCrimpage= parseFloat(totalCostCrimpage.toFixed(2));

            totalWtCr= totalWtCr + totalWtCrimpage;
            totalCostCr= totalCostCr + totalCostCrimpage;
            lst[i].selectedWarpState=lst[i].YarnState;
            var arrStates= lst[i].YarnState; //lst[i].selectedWarpState.split(';');
            var rTot=0;
            for(var j in lstRMSku)
            {
                if(lstRMSku[j].SKU__c==lst[i].ProductId)
                {
                    if(arrStates.includes(lstRMSku[j].Yarn_State__c))
                    {
                        //AS PER JIRA UTM-260 ISSUE has been changes
                       //  if(!(lst[i].Yarn__r.RecordType.Name=="Twisted Yarn" && lstRMSku[j].Yarn_State__c == "Twisted"))
                       // {
                            rTot= rTot + ((lstRMSku[j].Cost_Per_Kg__c==undefined)? 0 : lstRMSku[j].Cost_Per_Kg__c);
                       // }
                       
                    }
                }
            }
            var costperKg=0;
            if(lst[i].TotalRMCostKg!=undefined && lst[i].TotalRMCostKg !=null)
            {
                 costperKg=  lst[i].TotalRMCostKg;
            }
            
            lst[i].TotalRMCostKg=rTot+ parseFloat(costperKg);

            
            lst[i].TotalWeightAfterCrimpage=totalWtCrimpage;
            lst[i].TotalCostAfterCrimpage=totalCostCrimpage;
            lst[i].TotalPhysicalWeight=weight;
            lst[i].CostingWeight=wastage;
            
            
        }
            component.set("v.totalWarpWeight",totalWarpWeight.toFixed(2));
            component.set("v.totalWarpCosting",totalWarpCosting.toFixed(2));
            component.set("v.totalWtCr",totalWtCr.toFixed(2));
            component.set("v.totalCostCr",totalCostCr.toFixed(2));
            lstAll.CrimpagePercentage= crimpage;
            
            lstAll.WarpList=lst;

            var fWidth= parseFloat(lstAll.ActualHorizontalRepeat)*parseFloat(totalEnds)/ parseFloat(lstAll.HooksRepeat);
            var t2= component.get("v.totalWeftWeight");
            var t = parseFloat(totalWarpWeight) + parseFloat(t2);
            lstAll.TotalFabricWeight = t.toFixed(2);
            lstAll.FinishedWidth=fWidth.toFixed(2);

            component.set("v.lstAllObjects",lstAll);
    },
    calculateWeftPicks:function(component, event, helper){
         //this.getRMCostHelper(component,event,helper);

         var total=0;
         var totalWeftWeight=0;
         var totalWeftCosting=0;
         var lstAll= component.get("v.lstAllObjects");
         
         var lst= lstAll.WeftList;
         var lstRMSku = component.get("v.RMSKU");
         var picksTotal= 0;
         var finalPPC=0;

         for(var t in lst)
         {
            var rowPicks = parseInt(lst[t].Picks,10);
            picksTotal= picksTotal + rowPicks;
         }

         finalPPC= picksTotal / lstAll.ActualHeight ;
         lstAll.FinalPPC= parseFloat(finalPPC.toFixed(2));
         for(var i in lst)
         {
            var picks = parseInt(lst[i].Picks,10);
            var ppcRow=(picks/ picksTotal) * finalPPC
             lst[i].PPCPerCol = ppcRow;
             
             var dinner= parseInt(lst[i].Dinner,10); 
             var weftWastage=lstAll.WeftWastageCms;
             var weight = (ppcRow * dinner * lstAll.ReedWidthCms /9000);
             weight= parseFloat(weight.toFixed(2));
             var wastage= (ppcRow * dinner * weftWastage /9000) + weight;
             wastage=parseFloat(wastage.toFixed(2));
             
             total= parseInt(total,10) + picks; 
             totalWeftWeight= totalWeftWeight + weight;
             totalWeftCosting= totalWeftCosting + wastage;
               
             
           var className="slds-checkbox_" + i;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
            {
                if(StateVal =='')
                {
                    if(!(lst[i].RecordTypeName=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                    StateVal=component.find("dpc")[cnt].get("v.label") ;
                }
                else
                {
                   if(!(lst[i].RecordTypeName=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                   StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label") 
                }
            }
          }

          lst[i].selectedWeftState=StateVal;

            var arrStates=StateVal.split(';');
            var rTot=0;
            for(var j in lstRMSku)
            {
                if(lstRMSku[j].SKU__c==lst[i].ProductId)
                {
                    if(arrStates.includes(lstRMSku[j].YarnState))
                     {
                        //if(!(lst[i].RecordType.Name=="Twisted Yarn" && lstRMSku[j].Yarn_State__c == "Twisted")){
                            rTot= rTot + ((lstRMSku[j].Cost_Per_Kg__c==undefined)? 0 : lstRMSku[j].Cost_Per_Kg__c);
                        //}
                     }
                      
                }
            }
            if(lst[i].TotalRMCostKg == null || lst[i].TotalRMCostKg == undefined)
            {
                lst[i].TotalRMCostKg= 0;
            }
            lst[i].TotalRMCostKg=rTot+parseFloat(lst[i].TotalRMCostKg);

                console.log('SELECTION:' + lst[i].selectedWeftState);
                lst[i].TotalPhysicalWeight=weight.toFixed(2);
                lst[i].CostingWeight=wastage.toFixed(2);
                lst[i].PPCPerCol= lst[i].PPCPerCol.toFixed(2);
         }
            lstAll.CalculatedHeight= parseFloat((total/lstAll.PPC).toFixed(2));
            var crimpage=  ((lstAll.CalculatedHeight - lstAll.ActualHeight ) * 100 ) / lstAll.ActualHeight;
            lstAll.CrimpagePercentage= parseFloat(crimpage.toFixed(2));
            component.set("v.totalWeftPicks",total);
            component.set("v.totalWeftWeight",totalWeftWeight.toFixed(2));
            component.set("v.totalWeftCosting",totalWeftCosting.toFixed(2));
            //component.set("v.NewYarns",lst);
            lstAll.WeftList=lst;
            component.set("v.lstAllObjects",lstAll);
            
    
    },
    removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lstAll= component.get("v.lstAllObjects");

        var lst = lstAll.WeftList;
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.deletefrom","WEFT");
        lstAll.WeftList=lst;
        //component.set("v.NewYarns",lst);
        component.set("v.lstAllObjects",lstAll)
        this.doAllCalculation(component, event, helper);
    },
    removeWarpHelper : function(component, event, helper){
      /*  var index = event.getSource().get("v.name");
        var lst = component.get("v.NewWarpYarns");
        if (index > -1) { lst.splice(index, 1); }

        component.set("v.deletefrom","WARP");
        component.set("v.NewWarpYarns",lst);
         this.doAllCalculation(component, event, helper);*/
         var index = event.getSource().get("v.name");
        var lstAll= component.get("v.lstAllObjects");

        var lst = lstAll.WarpList;
        if (index > -1) { lst.splice(index, 1); }

        component.set("v.deletefrom","WARP");
        lstAll.WarpList=new Array();
        //component.set("v.NewYarns",lst);
        component.set("v.lstAllObjects",lstAll)
        this.doAllCalculation(component, event, helper);
    },
    validateFormHelper : function(component, event,helper) {
        var validContact = true;
        var weftval= component.find("txtWeftWastage").get("v.value");
        if(weftval == null || weftval== undefined || weftval=="")
            validContact= false;
        else
        {
            if(isNaN(weftval))
                validContact= false;
        }
        if(!validContact)
        {
             var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Validation Error","message": "Please enter valid Weft Wastage!","type":"error"});
            resultsToast.fire();
        }
        
        /*
         var allValid = component.find('vForm').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        */
       return validContact;
    },
    saveBOMHelper : function(component, event, helper) {
       var action = component.get("c.SaveBillOfMatterial");
       
       var bomWarp = new Array();
       var bomWeft = new Array();

       


        var lstAll= component.get("v.lstAllObjects");
 
        
        var per=0;
        var p1=parseFloat(lstAll.PhysicalFabricWeight);
        var p2=parseFloat(lstAll.TotalFabricWeight);
        per=(p2-p1)/p1 * 100;
        if(per > 10 || per < -10)
        {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Validation Error","message": "Fabric Weight difference is more than 10%!","type":"error"});
            resultsToast.fire();
        }
        else
        {

        var lst=lstAll.WeftList;
        var lstWarps= lstAll.WarpList;
       
        var p= new Object();
        p.ActualHeight= lstAll.ActualHeight;
        p.ActualHorizontalRepeat= lstAll.ActualHorizontalRepeat;
        p.ActualVerticalRepeat= lstAll.ActualVerticalRepeat;
        p.ActualWidth= lstAll.ActualWidth;
        p.CalculatedHeight= lstAll.CalculatedHeight;
        p.CalculatedVerticalRepeat= lstAll.CalculatedVerticalRepeat;

        p.DesignStudioName= lstAll.DesignStudioName;
        p.DevelopmentRef= lstAll.DevelopmentRef;
        p.EPI= lstAll.EPI;
        p.FinalPPC= lstAll.FinalPPC;
        p.FinishedWidth= lstAll.FinishedWidth;
        p.HooksShafts= lstAll.HooksShafts;
        p.HooksRepeat= lstAll.HooksRepeat;
        p.PicksRepeat= lstAll.PicksRepeat;
        p.PPC= lstAll.PPC;
        p.ProductId= lstAll.ProductId;
        p.ReedWidthCms= lstAll.ReedWidthCms;
        p.Remarks= lstAll.Remarks;
        p.SelectedDesignSource= lstAll.SelectedDesignSource;
        p.SelectedWeaveType= lstAll.SelectedWeaveType;
        p.SelectWeave= lstAll.SelectWeave;
        p.TotalFabricWeight= lstAll.TotalFabricWeight;
        p.WarpWastagePercentage= lstAll.WarpWastagePercentage;
        p.WeftWastageCms= lstAll.WeftWastageCms;

        p.WeftWestage= lstAll.WeftWestage;
        p.WovenPPC= lstAll.WovenPPC;
        p.CrimpagePercentage= lstAll.CrimpagePercentage;
        p.WarpList= lstAll.WarpList;
        p.WeftList= lstAll.WeftList;
        p.PhysicalFabricWeight= lstAll.PhysicalFabricWeight;
        p.SelectedLoom=lstAll.SelectedLoom;


        action.setParams({bom : JSON.stringify(p)});

       action.setCallback(this, function(a) {
          if (a.getState() === "SUCCESS") {
             
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Fabric Saved","message": "Warp/Weft added successfully for Fabric.","type":"success"});
            resultsToast.fire();
            //$A.get('e.force:refreshView').fire();
            setTimeout(function(){ window.parent.location = '/' + component.get("v.recordId"); }, 3000);
             
          } 
          else if (a.getState() === "ERROR") {
            $A.log("Errors", a.getError()); 
                var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Error","message": a.getError()});
            resultsToast.fire();
        }
        });
        
       $A.enqueueAction(action);

        }
       
     }
    
    
    
})