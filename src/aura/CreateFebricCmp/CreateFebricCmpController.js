({
    getAllFabricCtrl: function(component, event, helper) {
       helper.getAllFabricHelper(component, event, helper);
    },
    deleteBOMCtrl: function(component, event, helper) {
       helper.deleteBOMHelper(component, event, helper);
    },
    WeftValidateCheckbox: function(component, event, helper){
       var i= event.target;
       /*
         var className="slds-checkbox_" + i;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true){
                if(StateVal =='')
                    StateVal=component.find("dpc")[cnt].get("v.label") ;
                else
                   StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label") 
            }
          }



       */
    },
    handleWeftPicksChange: function(component, event, helper) {
       helper.doAllCalculation(component,event,helper);
    },
    handleWeftSearch : function(component, event, helper) {
        component.set("v.selectedWeft", event.getParam("sObjectId"));
	},
    handleWarpSearch : function(component, event, helper) {
        component.set("v.selectedWarp", event.getParam("sObjectId"));
	},
    handleWarpPerChange: function(component, event, helper) {
        helper.doAllCalculation(component, event, helper);
    },
    addToWarpCtrl:function(component, event, helper) {
        var res =helper.validateFormHelper(component,event,helper);
        if(res)
        {
            helper.addToWarpHelper(component, event,helper);
        }
    },
    handleCheckBoxChange : function(component,event,helper){
        var className=event.getSource("v.name").get("v.class");
        var arrCheckedValues=new Array();
        for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
        {
             if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value"))
             {
                arrCheckedValues.push(component.find("dpc")[cnt].get("v.label"));
                //event.getSource("v.name").get("v.value")
                //arrCheckedValues.push(event.getSource("v.name").get("v.label"));
             }

        }
       
       if(arrCheckedValues.includes("Dyed") && arrCheckedValues.includes("Kora Dyed"))
        {
           var resultsToast = $A.get("e.force:showToast");
           resultsToast.setParams({"title": "Error","message": "Can not Select Dyed and Kora Dyed","type":"error"});
           resultsToast.fire();

          
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
            {
                 if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.label")=="Kora Dyed")
                 {
                     component.find("dpc")[cnt].set("v.value",false);
                 }

            }
        }
        else
        {
              helper.doAllCalculation(component,event,helper);
        }
        

    },
    addToWeftCtrl:function(component, event, helper) {
    
        var res = helper.validateFormHelper(component,event,helper);
        if(res)
        {
            helper.addToWeftHelper(component, event,helper);

        }
  },
    removeYarnCtrl:function(component, event, helper) {
        helper.removeYarnHelper(component, event,helper);
    },
    removeWarpCtrl:function(component, event, helper) {
        helper.removeWarpHelper(component, event,helper);
    },
      
    saveBOMCtrl:function(component, event, helper) {
        var res =helper.validateFormHelper(component,event,helper);
        if(res)
        {
             helper.saveBOMHelper(component, event,helper);
        }
       
    }
     
})