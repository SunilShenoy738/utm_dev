({ 
   getOrderDetailsHelper: function(component, event, helper) {
      var action = component.get("c.getOrderDetails");
      action.setParams({orderId : component.get("v.recordId") });
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                component.set("v.orderDetails",data.getReturnValue());
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
   getOrderSKUHelper: function(component, event, helper) {
         var lstYarns = component.get("v.NewYarns");
        var action = component.get("c.getOrderSKU");
        action.setParams({orderId : component.get("v.recordId") });

        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
               var lst=data.getReturnValue();
               console.log('LOAD ORDER');

                for(var i=0;i< lst.length;i++){
                    lst[i].Product2Id= lst[i].Product2.Id;
                    lst[i].RecordType= lst[i].Product2.RecordType.Name
                    lst[i].Name=lst[i].Product2.Name;
                    lst[i].selectedWeftState='';
                    lst[i].Quantity=0;
                    lst[i].Supplier__c='';
                    lst[i].Remarks__c='';
                    lst[i].Total__c=0;
                    lst[i].UnitPrice=0;
                    lst[i].UOM='';
                    
                    lstYarns.push(lst[i]);
                }
                 console.log(lstYarns);
                 component.set("v.NewYarns",lstYarns);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getUOMHelper: function(component, event, helper) {
      var action = component.get("c.getUOM");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                console.log("***UOM****");
                console.log(d);
                component.set("v.UOMList",d);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getSKUStateHelper : function(component, event, helper) {
        
        var action = component.get("c.getStateofSKU");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                component.set("v.skuState",d);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    checkWarpStates : function(component, event, helper){
    //lst[i].RecordType.Name
    var arr= component.get("v.newAddedWeft");
    var lst = component.get("v.NewYarns");
    
    for(var i in arr){
        var int=parseInt(arr[i],10)-1;
       var className="slds-checkbox_" + int;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && 
                (component.find("dpc")[cnt].get("v.label") == "Dyed" || component.find("dpc")[cnt].get("v.label") == "Twisted")){
               
                component.find("dpc")[cnt].set("v.value",true);

                if(lst[int].RecordType=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted")
                {
                    component.find("dpc")[cnt].set("v.disabled",true);
                }
               



            }
          }

      }
     // component.set("v.NewYarns",lst);

   },
   doAllCalculation : function(component, event, helper){
    var rate=0;
    var sData= component.get("v.supplierProcessData");
    var lst = component.get("v.NewYarns");
    
        
        for(var i in lst)
        {
            var isFound= false;
            for(var j in sData)
            {
                if(lst[i].Product2Id == sData[j].Id &&  lst[i].Supplier__c == sData[j].Supplier)
                {
                    isFound= true;
                    rate=sData[j].Rate * lst[i].Quantity;
                     lst[i].Total__c= rate.toFixed(2);
                     lst[i].UnitPrice= sData[j].Rate;
                    component.set("v.supplierProcessData",sData);
                    component.set("v.NewYarns",lst);
                }
            }
            if(!isFound)
            {
                var action = component.get("c.getSupplierSKU");
                action.setParams({yarnId : lst[i].Product2Id, SupplierId : lst[i].Supplier__c});
                        action.setCallback(this, function(data) 
                        {
                            if (data.getState() === "SUCCESS") 
                            {
                                var d=data.getReturnValue();
                                if(d.length >0)
                                {
                                    console.log(d[0]);
                                    rate=parseFloat(d[0].Rate__c == undefined ? 0 : d[0].Rate__c) * lst[i].Quantity;
                                     lst[i].Total__c= rate.toFixed(2);
                                     lst[i].UnitPrice= d[0].Rate__c;
                                    sData.push({Id:d[0].SKU__c,Rate:d[0].Rate__c,Supplier:d[0].Account__c});
                                    component.set("v.supplierProcessData",sData);
                                    component.set("v.NewYarns",lst);
                                }
                                
                            } 
                            else if (data.getState() === "ERROR") 
                            {
                                $A.log("Errors", data.getError());
                            }
            
                        });
                        
                        $A.enqueueAction(action);
            }
           
        }
                   
    },
   removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.NewYarns");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.NewYarns",lst);
    },
    saveWarpHelper : function(component, event, helper) {
        var action = component.get("c.savePOFromSalesOrder");
        
        var lst=component.get("v.NewYarns");
        var arr= new Array();
        for(var i in lst)
        {
          var className="slds-checkbox_" + i;
            var StateVal='';
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
            {
                if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
                {
                        if(StateVal =='')
                        {
                            if(!(lst[i].RecordType=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal=component.find("dpc")[cnt].get("v.label") ;
                            }
                        }
                        else
                        {
                            if(!(lst[i].RecordType=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label");
                            }
                        }
                        
                }
            }
           

            arr.push({SKU:lst[i].Product2Id,YarnState:StateVal,Quantity:lst[i].Quantity,
                Supplier:lst[i].Supplier__c,Remarks:lst[i].Remarks__c,Total:lst[i].Total__c,Rate:lst[i].UnitPrice,UOM:lst[i].UOM});
        }
        console.log(JSON.stringify(arr));
        action.setParams({purchaseData : JSON.stringify(arr)});
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({"title": "Purchase Saved","message": "Purchase Requisition saved successfully","type":"success"});
                resultsToast.fire();
                setTimeout(function(){ window.parent.location = '/one/one.app#/sObject/Purchase_Order__c/list?filterName=Recent'  }, 3000);
                //$A.get('e.force:refreshView').fire();
                
            } 
            else if (a.getState() === "ERROR") {
                console.log(a.getError());
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    }
    
    
    
})