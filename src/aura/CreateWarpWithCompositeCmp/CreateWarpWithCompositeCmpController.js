({
     handleYarnSearch : function(component, event, helper) {
        component.set("v.selectedYarn", event.getParam("sObjectId"));
	},
    addToYarnCtrl:function(component, event, helper) {
    	helper.addToYarnHelper(component, event,helper);
    },
    getWrapStateCtrl: function(component,event,helper){
        helper.getWrapStateHelper(component,event,helper);
    },
    handleCheckBoxChange : function(component,event,helper){
        var className=event.getSource("v.name").get("v.class");
        var arrCheckedValues=new Array();
        for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
        {
             if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value"))
             {
                arrCheckedValues.push(component.find("dpc")[cnt].get("v.label"));
                //event.getSource("v.name").get("v.value")
                //arrCheckedValues.push(event.getSource("v.name").get("v.label"));
             }

        }
       console.log(arrCheckedValues);
       if(arrCheckedValues.includes("Dyed") && arrCheckedValues.includes("Kora Dyed"))
        {
           var resultsToast = $A.get("e.force:showToast");
           resultsToast.setParams({"title": "Error","message": "Can not Select Dyed and Kora Dyed","type":"error"});
           resultsToast.fire();

          
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
            {
                 if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.label")=="Kora Dyed")
                 {
                     component.find("dpc")[cnt].set("v.value",false);
                 }

            }
        }
        

    },
    getAssetListCtrl : function(cmp) {
         var action = cmp.get("c.getAssetList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.assetList',yarns);
            }
        });
        $A.enqueueAction(action);
    },
   removeYarnCtrl:function(component, event, helper) {
        helper.removeYarnHelper(component, event,helper);
    },
    saveWarpCtrl:function(component, event, helper) {
        helper.saveWarpHelper(component, event,helper);
    }
     
})