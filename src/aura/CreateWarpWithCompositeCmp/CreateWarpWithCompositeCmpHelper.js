({ 
    addToYarnHelper: function(component, event, helper){
        var lstYarns = component.get("v.NewYarns");
        var action = component.get("c.getYarnDetail");
        var wId= component.get("v.selectedYarn");
          var newAddWeft=component.get("v.newAddedWeft");
          var indexNo=(lstYarns==undefined)?0 : lstYarns.length;
        action.setParams({yarnId : wId});
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++){
                    lst[i].selectedWeftState='';
                    //lst[i].Reed_Width__c=0;
                    lst[i].No_of_ends__c=0;
                    //lst[i].EPI__c=0;
                    lst[i].Beam__c='';
                    lst[i].Percentage_Wasted__c='';
                    
                    
                    indexNo=indexNo + 1;
                    newAddWeft.push(indexNo);
                    lstYarns.push(lst[i]);
                }
                
                component.set("v.NewYarns",lstYarns);
                 component.set("v.newAddedWeft",newAddWeft);
                console.log('**ALL WEFT**')
                console.log(lstYarns);
                helper.checkWarpStates(component,event,helper);
                
            } else if (data.getState() === "ERROR") { $A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
    },
    getWrapStateHelper : function(component, event, helper) {
        
        var action = component.get("c.getStateofWarp");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                component.set("v.WarpStates",d);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    checkWarpStates : function(component, event, helper){
    //lst[i].RecordType.Name
    var arr= component.get("v.newAddedWeft");
    var lst = component.get("v.NewYarns");
    
    for(var i in arr){
        var int=parseInt(arr[i],10)-1;
       var className="slds-checkbox_" + int;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && 
                (component.find("dpc")[cnt].get("v.label") == "Dyed" || component.find("dpc")[cnt].get("v.label") == "Twisted")){
               
                component.find("dpc")[cnt].set("v.value",true);

                if(lst[int].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted")
                {
                    component.find("dpc")[cnt].set("v.disabled",true);
                }
               



            }
          }

      }
     // component.set("v.NewYarns",lst);

   },
    removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.NewYarns");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.NewYarns",lst);
    },
    saveWarpHelper : function(component, event, helper) {
        var action = component.get("c.saveWarp");
        var wnm = component.find("wrapName").get("v.value");
        //var wper=component.find("perWested").get("v.value");

        var lst=component.get("v.NewYarns");
        var menuItems;  
         var lstWarpSku = new Array();
        var chkLstLength=0;
        for(var i=0;i<lst.length;i++){
            
            var className="slds-checkbox_" + i;
            var StateVal='';
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
                if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
                {
                        if(StateVal =='')
                        {
                            if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal=component.find("dpc")[cnt].get("v.label") ;
                            }
                        }
                        else
                        {
                            if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label");
                            }
                        }
                        
                }
            }
            lstWarpSku.push({Yarn__c:lst[i].Id,Beam__c:lst[i].Beam__c,EPI__c:lst[i].EPI__c,
                             No_of_ends__c:lst[i].No_of_ends__c,Reed_Width__c:lst[i].Reed_Width__c,
                             Percentage_Wasted__c:lst[i].Percentage_Wasted__c,
                             Yarn_State__c:StateVal});
            
            }
        console.log("**LIST**");
        console.log(lstWarpSku);
        action.setParams({wrapSkuList : JSON.stringify(lstWarpSku),wrapName :wnm,epi:component.find("epi").get("v.value"),reedWidth:component.find("reedWidth").get("v.value")});
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({"title": "Warp Saved","message": "Warp saved successfully","type":"success"});
                resultsToast.fire();
                setTimeout(function(){ window.parent.location = '/' + a.getReturnValue(); }, 3000);
                //$A.get('e.force:refreshView').fire();
                
            } 
            else if (a.getState() === "ERROR") {
                console.log(a.getError());
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    }
    
    
    
})