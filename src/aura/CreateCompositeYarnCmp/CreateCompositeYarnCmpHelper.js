({
    deleteCompositeHelper: function(component, event, helper) {
        var action = component.get("c.deleteCompositeYarns");
        action.setParams({yarnId :component.get("v.recordId")});
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
                var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Grindles Yarn Delete","message": "Grindles Yarn Deleted successfully","type":"success"});
            resultsToast.fire();
            //$A.get('e.force:refreshView').fire();
            setTimeout(function(){ window.parent.location = '/' + component.get("v.recordId"); }, 2000);
                
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
    ValidateHelper: function(component, event, helper) {
        var action = component.get("c.getCompositeCount");
        action.setParams({skuId :component.get("v.recordId")});
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
                component.set("v.lstCompositeSKU",data.getReturnValue());
                if(data.getReturnValue().length > 0){
                    component.set("v.ShowMessage",true);
                    component.set("v.HideMessage",false);
                }
                else
                {
                    component.set("v.ShowMessage",false);
                    component.set("v.HideMessage",true);
                }
                
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
    
    addYarnHelper: function(component, event, helper){
      var lstYarns = component.get("v.NewYarns");
      var action = component.get("c.getWeftDetail");
      var wId= component.get("v.selectedYarn");
      action.setParams({weftId : wId});
      action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++){lst[i].Cmp=0;
                lstYarns.push(lst[i]);
            }

                component.set("v.NewYarns",lstYarns);
                console.log('**ALL YARNS**')
                console.log(lstYarns);
             } else if (data.getState() === "ERROR") { $A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
    },
    removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.NewYarns");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.NewYarns",lst);
    },
    saveYarnHelper : function(component, event, helper) {
         var lst = component.get("v.NewYarns");
         var lstComp = new Array();//=component.get("v.CompositeYarns");
         var total=0;
         for(var i in lst)
         {
          total= parseFloat(total) +  parseFloat(lst[i].Cmp);
          lstComp.push({Composition_Percentage__c:lst[i].Cmp,SKU__c:component.get("v.recordId"),Yarn__c:lst[i].Id});
         }
         if(total != 100.00)
         {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Error","message": "Total COMPOSITION % should be 100% while its:" + total + "%","type":"error"});
            resultsToast.fire();
         }
         else
         {
            console.log(lstComp);
            var action = component.get("c.saveCompositeYarn");
             action.setParams({lstCompositeYarns :JSON.stringify(lstComp)});
            action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
             var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Grindles Yarn Saved","message": "Grindles Yarn Save successfully","type":"success"});
            resultsToast.fire();
            //$A.get('e.force:refreshView').fire();
            setTimeout(function(){ window.parent.location = '/' + component.get("v.recordId"); }, 3000);
         } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
         }

       
     }
    
    
    
})