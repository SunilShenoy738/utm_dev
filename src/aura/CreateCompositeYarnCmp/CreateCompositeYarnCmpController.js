({

    ValidateCtrl : function(component, event, helper){
        helper.ValidateHelper(component,event,helper);
    },
    handleYarnpSearch : function(component, event, helper) {
        component.set("v.selectedYarn", event.getParam("sObjectId"));
    },
    addToYarnCtrl:function(component, event, helper) {
            helper.addYarnHelper(component, event,helper);
  },
    removeYarnCtrl:function(component, event, helper) {
        helper.removeYarnHelper(component, event,helper);
},      
    saveYarnCtrl:function(component, event, helper) {
             helper.saveYarnHelper(component, event,helper);
    },
deleteCompositeCtrl:function(component, event, helper) {
             helper.deleteCompositeHelper(component, event,helper);
    }
     
})