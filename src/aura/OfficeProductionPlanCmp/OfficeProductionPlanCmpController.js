({
       
    InitCtrl : function(component, event, helper){ helper.InitHelper(component,event,helper); },
    IntJobsCtrl:function(component, event, helper){helper.InitJobsHelper(component, event, helper)},
    closeModel: function(component, event, helper) { component.set("v.isOpen", false); },
    saveJobCtrl: function(component, event, helper){helper.saveJobHelper(component,event,helper);},
    OpenPopupCtrl: function(component,event, helper){
      var index = event.target.getAttribute("data-selected-Index");
      component.set("v.selectedItem",index);
      var lst=  component.get("v.orderItems");
      component.set("v.title", "SKU - " + lst[index].SKU);
      helper.getItemDetailHelper(component,event,helper);
      component.set("v.isOpen", true);

    }
     
})