({
  
     InitHelper: function(component, event, helper) {
        var action = component.get("c.getOrderDetails");
        action.setParams({orderId : component.get("v.recordId") });
        console.log("AAAA");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
               var lst=data.getReturnValue();
               component.set("v.orderItems",lst);
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
     InitJobsHelper: function(component, event, helper) {
     
        var action = component.get("c.getOrderJobDetails");
        action.setParams({orderId : component.get("v.recordId") });
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
            
               var lst=data.getReturnValue();
               console.log("JOBS***");
               console.log(lst);
               component.set("v.PlannedOrderItems",lst);
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
        
    },
    getItemDetailHelper: function(component, event, helper) {
      var action = component.get("c.GetOrderItemProcesses");
     var lst=  component.get("v.orderItems");
     var index= parseInt(component.get("v.selectedItem"),10);
     console.log("index:" + index);
     console.log(lst[index]);
      action.setParams({orderItemData : "[" + JSON.stringify(lst[index]) + "]" });
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
               var d=data.getReturnValue();
               console.log(d);
               component.set("v.ItemProcess",d);
                
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
    },
    saveJobHelper: function(component, event, helper){
      var lst = component.get("v.ItemProcess");
       var action = component.get("c.saveJobs");
       var lstSelected= new Array();
       var orderItemId=lst[0].OrderItemId;
       for(var i in lst)
       {
        if(lst[i].IsSelected)
        {
          var item=new Object();
          item.OrderItemId=lst[i].OrderItemId;
          item.SKU= lst[i].SKU;
          item.OrderId= lst[i].OrderId;
          item.Quantity= lst[i].Quantity;
          item.UnitOfMeasurement= lst[i].UnitOfMeasurement;
          item.DeliveryDate= lst[i].DeliveryDate;
          item.Remarks= lst[i].Remarks;
          item.RemarksNew= lst[i].RemarksNew;
          item.CustomerSKUProcess= lst[i].CustomerSKUProcess;
          item.SKUMasterProcess= lst[i].SKUMasterProcess;
          item.ProcessNumber= lst[i].ProcessNumber;
          item.ProductId= lst[i].ProductId;
          item.Rate= lst[i].Rate;
          item.SelectedSupplier=lst[i].SelectedSupplier;
          item.AssignQty= lst[i].AssignQty;
          item.JobOption="Purchase";
          item.UnitOfMeasurement= lst[i].UnitOfMeasurement;

          lstSelected.push(item);
        }
       }
      action.setParams({orderItemData : JSON.stringify(lstSelected), itemId:orderItemId});
       action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") { 
              var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Process Saved","message": "Office Plan Save successfully","type":"success"});
            resultsToast.fire();
             component.set("v.isOpen", false);
                 $A.get("e.force:closeQuickAction").fire();
            } 
            else if (data.getState() === "ERROR") {$A.log("Errors", data.getError());  }
        });
        $A.enqueueAction(action);
      
    },

    
    
    
})