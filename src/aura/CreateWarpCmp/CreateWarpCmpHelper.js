({ 
    getWrapStateHelper : function(component, event, helper) {

        var action = component.get("c.getStateofWarp");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                component.set("v.WarpStates",d);
             } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getWrapTemplateHelper : function(component, event, helper) {
        var action = component.get("c.getWrapTemplate");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                component.set("v.NewYarns", data.getReturnValue());
                var lst = component.get("v.NewYarns");
                lst.push({'Beam__c':'','No_of_ends__c':'','Warp_Master__c':'','Yarn__c':''});
                console.log(lst[0]);
                component.set("v.NewYarns",lst);
                
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
    },
    addYarnHelper : function(component, event, helper){
        var lst = component.get("v.NewYarns");
        lst.push({'Beam__c':'','No_of_ends__c':'','Warp_Master__c':'','Yarn__c':'','Yarn_State__c':''});
        component.set("v.NewYarns",lst);
    },
    removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.NewYarns");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.NewYarns",lst);
    },
    saveWarpHelper : function(component, event, helper) {
        var action = component.get("c.saveWarp");
       
        var wnm = component.find("wrapName").get("v.value");
        var wper=component.find("perWested").get("v.value");
        var lst=JSON.parse(JSON.stringify(component.get("v.NewYarns")));
         var menuItems;  
        
           var chkLstLength=0;
        for(var i=0;i<lst.length;i++){
            if(component.find("ddlAsset")[i] !=undefined)
            {
                lst[i].Beam__c=component.find("ddlAsset")[i].get("v.value");
                lst[i].No_of_ends__c=component.find("noofends")[i].get("v.value");
                lst[i].Yarn__c=component.find("ddlSku")[i].get("v.value");
                //menuItems=component.find("dpc")[i].get("v.value");
                lst[i].Reed_Width__c=component.find("reedWidth")[i].get("v.value");
                lst[i].EPI__c=component.find("EPI")[i].get("v.value");
           }
           else
            {
                lst[i].Beam__c=component.find("ddlAsset").get("v.value");
                lst[i].No_of_ends__c=component.find("noofends").get("v.value");
                lst[i].Yarn__c=component.find("ddlSku").get("v.value");
               //menuItems=component.find("dpc").get("v.value");
               lst[i].Reed_Width__c=component.find("reedWidth").get("v.value");
               lst[i].EPI__c=component.find("EPI").get("v.value");
               
           }
            var className="slds-checkbox_" + i;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true){
                if(StateVal =='')
                    StateVal=component.find("dpc")[cnt].get("v.label") ;
                else
                   StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label") 
            }
          }
           


            lst[i].Yarn_State__c=StateVal;
           console.log(lst[i]);

        }
         
        action.setParams({wrapSkuList : JSON.stringify(lst),wrapName :wnm,per:wper});
        
        action.setCallback(this, function(a) {
      if (a.getState() === "SUCCESS") {
       // var toastEvent = $A.get("e.force:showToast");
	   // toastEvent.setParams({"title": "Success!","message": "The record has been updated successfully." });
	     
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({"title": "Warp Saved","message": "Warp saved successfully","type":"success"});
            resultsToast.fire();
            $A.get('e.force:refreshView').fire();

      } 
      else if (a.getState() === "ERROR") {
               console.log(a.getError());
                $A.log("Errors", a.getError());
            }
        });
        
       $A.enqueueAction(action);
     }
    
    
    
})