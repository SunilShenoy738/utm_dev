({
    getWrapTemplateCtrl : function(component, event, helper) {
       helper.getWrapTemplateHelper(component);
    },
    getWrapStateCtrl: function(component,event,helper){
        helper.getWrapStateHelper(component,event,helper);
    },
    getYarnListCtrl : function(cmp) {
         var action = cmp.get("c.getYarnList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.yarnList',yarns);
            }
        });
        $A.enqueueAction(action);
    },
    getAssetListCtrl : function(cmp) {
         var action = cmp.get("c.getAssetList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.assetList',yarns);
            }
        });
        $A.enqueueAction(action);
    },
   addYarnCtrl:function(component, event, helper) {
        helper.addYarnHelper(component, event,helper);
    },
    removeYarnCtrl:function(component, event, helper) {
        helper.removeYarnHelper(component, event,helper);
    },
    saveWarpCtrl:function(component, event, helper) {
        helper.saveWarpHelper(component, event,helper);
    }
     
})