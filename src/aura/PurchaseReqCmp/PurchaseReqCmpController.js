({
     handleYarnSearch : function(component, event, helper) {
        component.set("v.selectedYarn", event.getParam("sObjectId"));
	},
     getGradeCtrl : function(component, event, helper) {
        helper.getGradesHelper(component, event, helper);
    },
    
    addToYarnCtrl:function(component, event, helper) {
    	helper.addToYarnHelper(component, event,helper);
    },
    getSKUStateCtrl: function(component,event,helper){
        helper.getSKUStateHelper(component,event,helper);
    },
    getUOMCtrl: function(component,event,helper){
        helper.getUOMHelper(component,event,helper);
    },
    handleOnChangeCtrl : function(component, event, helper){
        helper.doAllCalculation(component,event,helper);
    },
    handleCheckBoxChange : function(component,event,helper){
        var className=event.getSource("v.name").get("v.class");
        var arrCheckedValues=new Array();
        for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
        {
             if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value"))
             {
                arrCheckedValues.push(component.find("dpc")[cnt].get("v.label"));
                //event.getSource("v.name").get("v.value")
                //arrCheckedValues.push(event.getSource("v.name").get("v.label"));
             }

        }
      
       if(arrCheckedValues.includes("Dyed") && arrCheckedValues.includes("Kora Dyed"))
        {
           var resultsToast = $A.get("e.force:showToast");
           resultsToast.setParams({"title": "Error","message": "Can not Select Dyed and Kora Dyed","type":"error"});
           resultsToast.fire();

          
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
            {
                 if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.label")=="Kora Dyed")
                 {
                     component.find("dpc")[cnt].set("v.value",false);
                 }

            }
        }
       else
        {
                component.set("v.selectedStates",arrCheckedValues);
              helper.doAllCalculation(component,event,helper);
        }
       
        

    },
    getSupplierListCtrl : function(cmp) {
         var action = cmp.get("c.getSupplierList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.supplierList',yarns);
            }
        });
        $A.enqueueAction(action);
    },
   removeYarnCtrl:function(component, event, helper) {
        helper.removeYarnHelper(component, event,helper);
    },
    saveWarpCtrl:function(component, event, helper) {
        helper.saveWarpHelper(component, event,helper);
    }
     
})