({ 
    addToYarnHelper: function(component, event, helper){
        var lstYarns = component.get("v.NewYarns");
        var action = component.get("c.getYarnDetail");
        var wId= component.get("v.selectedYarn");
          var newAddWeft=component.get("v.newAddedWeft");
          var indexNo=(lstYarns==undefined)?0 : lstYarns.length;
        action.setParams({yarnId : wId});
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                console.log(data.getReturnValue());
                var lst=data.getReturnValue();
                for(var i=0;i< lst.length;i++){
                    lst[i].Product2Id= lst[i].Id;
                    lst[i].selectedWeftState='';
                    lst[i].Quantity=0;
                    lst[i].Supplier__c='';
                    lst[i].Remarks__c='';
                    lst[i].Total__c=0;
                    lst[i].UnitPrice=0;
                    lst[i].UOM='';
                    lst[i].SelectedGrade='';
                    lst[i].ColourName='';
                    indexNo=indexNo + 1;
                    newAddWeft.push(indexNo);
                    lstYarns.push(lst[i]);
                }
                
                 component.set("v.NewYarns",lstYarns);
                 component.set("v.newAddedWeft",newAddWeft);
                console.log('**ALL WEFT**')
                console.log(lstYarns);
                helper.checkWarpStates(component,event,helper);
                
            } else if (data.getState() === "ERROR") { $A.log("Errors", data.getError()); }
        });
        $A.enqueueAction(action);
    },
     getGradesHelper : function(component, event, helper) {

        var action = component.get("c.getGrades");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                console.log("JSON");
                console.log(d);
                component.set("v.GradeList",d);
             } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getUOMHelper: function(component, event, helper) {
        
        var action = component.get("c.getUOM");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                console.log("***UOM****");
                console.log(d);
                component.set("v.UOMList",d);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getSKUStateHelper : function(component, event, helper) {
        
        var action = component.get("c.getStateofSKU");
        action.setCallback(this, function(data) {
            if (data.getState() === "SUCCESS") {
                var d=JSON.parse(data.getReturnValue());
                component.set("v.skuState",d);
            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    checkWarpStates : function(component, event, helper){
    //lst[i].RecordType.Name
    var arr= component.get("v.newAddedWeft");
    var lst = component.get("v.NewYarns");
    
    for(var i in arr)
    {
        var int=parseInt(arr[i],10)-1;
       var className="slds-checkbox_" + int;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && 
                (component.find("dpc")[cnt].get("v.label") == "Dyed" || component.find("dpc")[cnt].get("v.label") == "Twisted")){
               
                component.find("dpc")[cnt].set("v.value",true);

                if(lst[int].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted")
                {
                    component.find("dpc")[cnt].set("v.disabled",true);
                }
               



            }
          }

      }
     
        var arrCheckedValues=new Array();
        arrCheckedValues.push("Dyed");
        arrCheckedValues.push("Twisted");
        component.set("v.selectedStates",arrCheckedValues);
     // component.set("v.NewYarns",lst);

   },
   doAllCalculation : function(component, event, helper){
    var rate=0;
    var sData= component.get("v.supplierProcessData");
    var lst = component.get("v.NewYarns");
    
        
        for(var i in lst)
        {
            var isFound= false;
            var selectedVal='';
             var className="slds-checkbox_" + i;
           var StateVal='';
          for(var cnt=0;cnt < component.find("dpc").length; cnt ++){
            if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
            {
                if(StateVal =='')
                {
                    if(!(lst[i].RecordTypeName=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                    StateVal=component.find("dpc")[cnt].get("v.label") ;
                }
                else
                {
                   if(!(lst[i].RecordTypeName=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                   StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label") 
                }
            }
          }

          selectedVal= StateVal;


            for(var j in sData)
            {
                if(lst[i].Product2Id == sData[j].Id &&  lst[i].Supplier__c == sData[j].Supplier && selectedVal!= null && selectedVal!= undefined)
                {
                    isFound= true;
                    rate=sData[j].Rate;
                    if(selectedVal.includes("Dyed"))
                        rate= parseFloat(rate) + parseFloat(sData[j].Dying_Rate__c);
                     if(selectedVal.includes("Twisted"))
                        rate= parseFloat(rate) + parseFloat(sData[j].Twisting_Rate__c);
                     if(selectedVal.includes("Kora Dyed"))
                        rate= parseFloat(rate) + parseFloat(sData[j].Kora_Dying_Rate__c);
                     if(selectedVal.includes("Heating Setting"))
                        rate= parseFloat(rate) + parseFloat(sData[j].Heating_Setting_Rate__c);

                    lst[i].UnitPrice= rate;//sData[j].Rate;

                    rate=rate * lst[i].Quantity;
                     lst[i].Total__c= rate.toFixed(2);
                     
                    component.set("v.supplierProcessData",sData);
                    component.set("v.NewYarns",lst);
                }
            }
            if(!isFound)
            {
                var action = component.get("c.getSupplierSKU");
                action.setParams({yarnId : lst[i].Product2Id, SupplierId : lst[i].Supplier__c});
                        action.setCallback(this, function(data) 
                        {
                            if (data.getState() === "SUCCESS") 
                            {
                                var d=data.getReturnValue();
                                if(d.length >0)
                                {
                                    console.log("Price Data");
                                    console.log(d[0]);
                                     sData.push({Id:d[0].SKU__c,Rate:d[0].Rate__c,Supplier:d[0].Account__c,
                                    Dying_Rate__c: d[0].Dying_Rate__c,Heating_Setting_Rate__c:d[0].Heating_Setting_Rate__c,
                                    Kora_Dying_Rate__c:d[0].Kora_Dying_Rate__c,Twisting_Rate__c:d[0].Twisting_Rate__c
                                    });

                                     rate=d[0].Rate__c;

                                     if(selectedVal.includes("Dyed"))
                                        rate= parseFloat(rate) + parseFloat(d[0].Dying_Rate__c);
                                    if(selectedVal.includes("Twisted"))
                                        rate= parseFloat(rate) + parseFloat(d[0].Twisting_Rate__c);
                                    if(selectedVal.includes("Kora Dyed"))
                                        rate= parseFloat(rate) + parseFloat(d[0].Kora_Dying_Rate__c);
                                    if(selectedVal.includes("Heating Setting"))
                                        rate= parseFloat(rate) + parseFloat(d[0].Heating_Setting_Rate__c);

                                    lst[i].UnitPrice= rate;//d[0].Rate__c;
                                    rate=rate * lst[i].Quantity;

                                     //rate=d[0].Rate__c * lst[i].Quantity;
                                     lst[i].Total__c= rate.toFixed(2);
                                     
                                   
                                    component.set("v.supplierProcessData",sData);
                                    component.set("v.NewYarns",lst);
                                }
                                
                            } 
                            else if (data.getState() === "ERROR") 
                            {
                                $A.log("Errors", data.getError());
                            }
            
                        });
                        
                        $A.enqueueAction(action);
            }
           
        }
       

        
               /*
                var className="slds-checkbox_" + i;
                var StateVal='';
                  for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
                  {
                    if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
                    {
                        if(StateVal =='')
                        {
                            if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            StateVal=component.find("dpc")[cnt].get("v.label") ;
                        }
                        else
                        {
                           if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                           StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label") 
                        }
                    }
                  }

                lst[i].selectedWeftState=StateVal;
                var arrStates=StateVal.split(';');
                 for(var q in arrStates)
                {
                    if(arrStates[q]== sData[j].State)
                    {
                        rate= rate + (sData[j].Rate * lst[i].Quantity);
                    }
                    else
                    {
                        var action = component.get("c.getStateofSKU");
                        action.setCallback(this, function(data) 
                        {
                            if (data.getState() === "SUCCESS") 
                            {
                                var d=data.getReturnValue();
                                sData.push({Id:d.SKU__c,Rate:Rate__c,Supplier:Account__c})
                                component.set("v.supplierProcessData",d);
                            } 
                            else if (data.getState() === "ERROR") 
                            {
                                $A.log("Errors", data.getError());
                            }
            
                        });

                        $A.enqueueAction(action);
                    }
                }
                */
               

            
             
    },
   removeYarnHelper : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var lst = component.get("v.NewYarns");
        if (index > -1) { lst.splice(index, 1); }
        component.set("v.NewYarns",lst);
    },
    saveWarpHelper : function(component, event, helper) {
        var action = component.get("c.savePurchaseRequisition");
        
        var lst=component.get("v.NewYarns");
        var arr= new Array();
        for(var i in lst)
        {
          var className="slds-checkbox_" + i;
            var StateVal='';
            for(var cnt=0;cnt < component.find("dpc").length; cnt ++)
            {
                if(component.find("dpc")[cnt].get("v.class") == className && component.find("dpc")[cnt].get("v.value") == true)
                {
                        if(StateVal =='')
                        {
                            if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal=component.find("dpc")[cnt].get("v.label") ;
                            }
                        }
                        else
                        {
                            if(!(lst[i].RecordType.Name=="Twisted Yarn" && component.find("dpc")[cnt].get("v.label") == "Twisted"))
                            {
                                StateVal= StateVal + ';' + component.find("dpc")[cnt].get("v.label");
                            }
                        }
                        
                }
            }
           

            arr.push({SKU:lst[i].Product2Id,YarnState:StateVal,Quantity:lst[i].Quantity,
                Supplier:lst[i].Supplier__c,Remarks:lst[i].Remarks__c,Total:lst[i].Total__c,
                Rate:lst[i].UnitPrice,UOM:lst[i].UOM, SelectedGrade: lst[i].SelectedGrade,
                ColourName: lst[i].ColourName});
        }
        action.setParams({purchaseData : JSON.stringify(arr)});
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({"title": "Purchase Saved","message": "Purchase Requisition saved successfully","type":"success"});
                resultsToast.fire();
                setTimeout(function(){ window.parent.location = '/one/one.app#/sObject/Purchase_Order__c/list?filterName=Recent'  }, 3000);
                //$A.get('e.force:refreshView').fire();
                
            } 
            else if (a.getState() === "ERROR") {
                console.log(a.getError());
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    }
    
    
    
})