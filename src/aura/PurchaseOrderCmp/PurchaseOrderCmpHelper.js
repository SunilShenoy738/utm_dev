({ 
    getDepartmentHelper: function(component, event, helper) {
        var action = component.get("c.getAllDepartment");
        action.setCallback(this, function(data) {
            var roles = new Array(), results;
            if (data.getState() === "SUCCESS") {
                results=data.getReturnValue();
                console.log(results);
                component.set("v.LstDepartments",results);

            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
        
    },
    getSKUStateHelper : function(component, event, helper) {
    try 
    {
    
    //var arr = new Array({Id: "", IsSelected: false, IsSupplier: false, POId: "", PONo: "",Quantity:0,Rate:0,Remarks:"",SKU:"",SKUName:"",Supplier:"",SupplierName:"",UOM:"",YarnState:""});
    //var lst =component.get("v.PoItems");
    //for(var i in lst) { lst.splice(i, 1); }
    component.set("v.PoItems",[]);
    helper.fetchData(component,event,helper)
   }
    catch(err){}
        
    },
    fetchData : function(component, event, helper){
         try 
    {
    
    //var arr = new Array({Id: "", IsSelected: false, IsSupplier: false, POId: "", PONo: "",Quantity:0,Rate:0,Remarks:"",SKU:"",SKUName:"",Supplier:"",SupplierName:"",UOM:"",YarnState:""});
    //var lst =component.get("v.PoItems");
    //for(var i in lst) { lst.splice(i, 1); }
   


    var txtPo = component.get("v.poNumber");
    var txtSup = component.get("v.supplierName");
    var txtDept = component.get("v.DepartmentName");

    if(txtPo == undefined) txtPo= "";
    if(txtSup == undefined) txtSup= "";
    if(txtDept == undefined) txtDept= "";

        var action = component.get("c.getReqPO");
        action.setParams({poNumber : txtPo,supplierNm: txtSup,department:txtDept});
        action.setCallback(this, function(data) {
           
            if (data.getState() === "SUCCESS") {
                console.log('***PO Items***');
                var d= data.getReturnValue();
                var poArray= new Array();
                for(var i in d)
                {
                    poArray.push(d[i]);
                }
                console.log(poArray);
                component.set("v.PoItems",poArray);

            } else if (data.getState() === "ERROR") {
                console.log(data.getError());
                $A.log("Errors", data.getError());
            }
            
        });
        $A.enqueueAction(action);
    }
    catch(err){}
    },
     handleOnChangeHelper : function(component, event, helper){
        //Eating Error because of lightning error - Nested of iteration with Select control issue. Need to validate later release with salesforce
        try
        {
        var selVal= event.getSource().get("v.value");
         var selLabel;
      //var icnt=parseInt(event.getSource().get("v.class"),10);
     
      var row=parseInt(event.getSource().get("v.class").replace("cls_",""),10);
     // var row=parseInt(event.getSource().get("v.label"),10);
      var lst = component.get("v.PoItems");

      var supLst=component.get("v.supplierList");
      for(var k in supLst)
      {
        if(supLst[k].Id==selVal)
            selLabel= supLst[k].Name;
      } 
      lst[row].Supplier=selVal;
      lst[row].SupplierName =selLabel;
      
      var arrSupp= new Array();
      var arrSortedArray= new Array();
      for(var l in lst)
      {
        if(lst[l].IsSupplier)
            arrSupp.push({IsSupplier:lst[l].IsSupplier,Supplier:lst[l].Supplier,SupplierName:lst[l].SupplierName});
      }
     
     for(var i in arrSupp)
     {
       arrSortedArray.push({Supplier:arrSupp[i].Supplier,IsSupplier:true,SupplierName:arrSupp[i].SupplierName});
         for(var j in lst)
        {
            if(lst[j].Supplier == arrSupp[i].Supplier && lst[j].IsSupplier == false)
            {

               arrSortedArray.push(
                {
                    Id:lst[j].Id,
                    IsSupplier:false,
                    IsSelected:lst[j].IsSelected,
                    Quantity:(lst[j].Quantity==undefined) ? 0 : lst[j].Quantity,
                    Rate:(lst[j].Rate == undefined) ? 0 :lst[j].Rate,
                    SKU:lst[j].SKU,
                    SKUName:lst[j].SKUName,
                    SupplierName:lst[j].SupplierName,
                    Supplier:lst[j].Supplier,
                    UOM:(lst[j].UOM==undefined) ? "" :lst[j].UOM,
                    YarnState:(lst[j].YarnState ==undefined) ? "": lst[j].YarnState,
                    POId:lst[j].POId,
                    PONo:lst[j].PONo,
                    SelectedGrade:lst[j].SelectedGrade,
                    ColourName:lst[j].ColourName,
                    Remarks: (lst[j].Remarks==undefined)?"" :lst[j].Remarks 
                }); 
            }
        }


     }
        component.set("v.PoItems",arrSortedArray); 
}
        catch(err){}
        
     
    },
    saveWarpHelper : function(component, event, helper) {
       var action = component.get("c.savePurchaseOrder");
        var lst = component.get("v.PoItems");

    action.setParams({purchaseData : JSON.stringify(lst)});
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({"title": "Purchase Saved","message": "Purchase Order saved successfully","type":"success"});
                resultsToast.fire();
                setTimeout(function(){ window.parent.location = '/one/one.app#/sObject/Purchase_Order__c/list?filterName=Recent' }, 3000);
                //$A.get('e.force:refreshView').fire();
                
            } 
            else if (a.getState() === "ERROR") {
                console.log(a.getError());
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    }
    
    
    
})