({
     getDepartmentCtrl : function(component, event, helper) {
         helper.getDepartmentHelper(component,event,helper);
	},
    SearchCtrl : function(component, event, helper) {
        try{
         helper.getSKUStateHelper(component,event,helper);
        }
        catch(err){}

    },
    getSKUStateCtrl: function(component,event,helper){
       // helper.getSKUStateHelper(component,event,helper);
    },
    handleOnChangeCtrl : function(component, event, helper){
     helper.handleOnChangeHelper(component, event, helper);  
     
    },
    UpdateCheckedCtrl: function(component, event, helper){
        try{
     var index = event.target.getAttribute("data-index");
      var lst = component.get("v.PoItems");
     if(event.target.checked)
        {
            lst[index].IsSelected= true;
        }
        else
        {
            lst[index].IsSelected= false;
        }
        component.set("v.PoItems",lst);
    }
    catch(err){}
     
    },
   

    getSupplierListCtrl : function(cmp) {
         var action = cmp.get("c.getSupplierList");
        action.setCallback(this, function(response) {
            if(cmp.isValid() && response.getState() === "SUCCESS") {
                var yarns = response.getReturnValue();
                cmp.set('v.supplierList',yarns);
            }
        });
        $A.enqueueAction(action);
    },
    saveWarpCtrl:function(component, event, helper) {
        helper.saveWarpHelper(component, event,helper);
    }
     
})