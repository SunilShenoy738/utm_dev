<apex:page standardController="Product2" extensions="AddCostingDetailExtension" sidebar="false">
    <script>
        function showSpinner()
        {
            document.getElementById('loadSpinner').style.display = "block";
        }
        
        function hideSpinner()
        {
            document.getElementById('loadSpinner').style.display = "none";
        }
    </script>
    <apex:stylesheet value="{!URLFOR($Resource.Slds221, 'assets/styles/salesforce-lightning-design-system.css')}" />
    
    <div class="slds">
    <div class="slds-box slds-is-relative">
    
    <apex:form >
        <center><apex:messages style="color:red; font-weight:bold; font-size: 20px"/></center>
      <div class="demo-only demo--inverse" style="height:6rem;display:none;" id="loadSpinner"> 
        <div class="slds-spinner_container" style="position:fixed;" >
            <div role="status" class="slds-spinner slds-spinner_medium">
              <span class="slds-assistive-text">Loading</span>
              <div class="slds-spinner__dot-a"></div>
              <div class="slds-spinner__dot-b"></div>
            </div>
         </div>
       </div>     
           
          
        <div class="slds-page-header slds-grid" role="banner" style="padding-top: 8px; padding-bottom: 8px;">
            <div class="slds-media slds-col slds-size--9-of-12">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">SKU Code: &nbsp;<apex:outputLink style="color:rgb(84, 105, 141);" value="/{!Product2.Id}">{!Product2.Name}</apex:outputLink></p>
                </div>
            </div>
            <div class="slds-media slds-col">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">Last Costing Done: &nbsp;<apex:outputText id="last_costing_done" style="color:rgb(84, 105, 141);" value="{0, date, MMMM d','  yyyy}">
                         <apex:param value="{!Product2.Last_Costing_Done__c}" />
                    </apex:outputText></p>
                </div>
            </div>
         </div>
         <br/>      
          
         <div class="slds-page-header" role="banner" style="padding-top: 8px; padding-bottom: 8px;">
            <div class="slds-media">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">Raw Material Cost (Including processes performed on Yarn)</p>
                </div>
            </div>
         </div>   
         <table class="slds-table slds-table_bordered slds-table_cell-buffer slds-table_col-bordered slds-no-row-hover">
             <tr class="slds-text-title_caps">
                 <th scope="col">Yarn Name</th>
                 <th scope="col">RM Rate/Kg</th>
                 <th scope="col">Costing Weight/Mtr</th>
                 <th scope="col">Cost Per Meter</th>
             </tr>
             <apex:repeat value="{!bomList}" var="bom">
                 <tr>
                     <td scope="row">{!bom.Yarn__r.Name}</td>
                     <td scope="row">{!bom.Total_RM_Cost_Kg__c}</td>
                     <td scope="row">{!bom.Costing_Wt_Mtr__c}</td>
                     <td scope="row">{!bom.Cost_Per_Meter__c}</td>
                 </tr>
             </apex:repeat>    
             <tr>
                 <td scope="row"></td>
                 <td scope="row"></td>
                 <th scope="row" class="slds-text-title_caps">Total</th>
                 <th scope="row" class="slds-text-title_caps">{!totalRMCost}</th>
             </tr> 
         </table>
         <br/>
         
         <div class="slds-page-header" role="banner" style="padding-top: 8px; padding-bottom: 8px;">
            <div class="slds-media">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">Fabric Finishes Applied</p>
                </div>
            </div>
         </div>
         <table class="slds-table slds-table_bordered slds-table_cell-buffer slds-table_col-bordered slds-no-row-hover">
             <tr class="slds-text-title_caps">    
                 <th>Finish</th>
                 <th>Vendor</th>
                 <th>Rate</th>
             </tr>
             <apex:repeat value="{!skuProcessList}" var="skuProcess">
                 <tr>
                     <td>{!skuProcess.Process__r.Name}</td>
                     <td>{!skuProcess.Vendor__c}</td>
                     <td>{!skuProcess.Cost_per_Mtr__c}</td>
                 </tr>
             </apex:repeat>    
             <tr>
                 <td></td>
                 <th class="slds-text-title_caps">Total</th>
                 <th class="slds-text-title_caps">{!totalFinishRate}</th>
             </tr>
         </table>
         <br/>
         
         <!--
         <div class="slds-page-header" role="banner" style="padding-top: 8px; padding-bottom: 8px;">
            <div class="slds-media">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">Type of Weaving: &nbsp;{!Product2.Weave_Type__c}</p>
                </div>
            </div>
         </div>
         <br/>
		-->
         
      <apex:outputPanel id="CostingOutputPanel">
         <div class="slds-page-header" role="banner" style="padding-top: 8px; padding-bottom: 8px;">
            <div class="slds-media">
                <div class="slds-media__body" >
                    <p class="slds-page-header__title slds-truncate slds-align-middle" style="color:rgb(84, 105, 141);font-size:.8rem;font-weight:bold">Miscellaneous Cost</p>
                </div>
            </div>
         </div>
         <table class="slds-table slds-table_bordered slds-table_cell-buffer slds-table_col-bordered slds-no-row-hover">
             <tr class="slds-text-title_caps">
                 <th></th>
                 <th>Express</th>
                 <th>Standard</th>
                 <th>Custom</th>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">Conversion Cost</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!Product2.Express_Conversion_Cost__c}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!Product2.Bulk_Conversion_Cost__c}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Custom_Conversion_Cost__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                            
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">Depreciation</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Express_Depreciation__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Standard_Depreciation__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Custom_Depreciation__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">Loan</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Express_Loan__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Standard_Loan__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Custom_Loan__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">PPC Surcharge</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Express_PPC_Surcharge__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Standard_PPC_Surcharge__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Custom_PPC_Surcharge__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">FOB Cost</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                              {!expressFOBCost}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                              {!standardFOBCost}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                             {!customFOBCost}
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">Freight</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!costingMaster.Freight__c}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!costingMaster.Freight__c}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!costingMaster.Freight__c}
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">Excess Freight</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Express_Freight__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Standard_Freight__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            <apex:inputField styleClass="slds-input" value="{!Product2.Custom_Freight__c}" style="width:100px;" onchange="calculateTotalCost();showSpinner();"/>
                          </div>
                      </div>
                 </td>
             </tr>
             <tr>
                 <th class="slds-text-title_caps">CIF Cost</th>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!expressCIFCost}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!standardCIFCost}
                          </div>
                      </div>
                 </td>
                 <td>
                     <div class="slds-form-element">
                         <div class="slds-form-element__control">
                            {!customCIFCost}
                          </div>
                      </div>
                 </td>
             </tr>
         </table>
         <br/>
         <center>
             <apex:commandButton action="{!updateCostingDetail}" rerender="last_costing_done" value="Save" style="color:#ffffff;border-radius: 0.25rem; shape-margin: initial;background:#136dd9;margin-right:10px;width:50px;height:30px;font-weight: 100;border: 1px solid #d8dde6;" onClick="showSpinner()" onComplete="hideSpinner()"/>
             <apex:commandButton action="{!cancel}" value="Cancel" style="color:#ffffff;border-radius: 0.25rem; shape-margin: initial;background:#be3b2f;margin-right:10px;width:50px;height:30px;font-weight: 100;border: 1px solid #d8dde6;"/>
             
         </center> 
         
         <apex:actionFunction name="calculateTotalCost" action="{!calculateTotalCost}" reRender="CostingOutputPanel" onComplete="hideSpinner()"/> 
       </apex:outputPanel>     
    </apex:form> 
    
    </div>
     
   </div>      
</apex:page>