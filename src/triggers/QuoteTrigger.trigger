trigger QuoteTrigger on Quote (after update, before insert) {

	if(Trigger.isAfter)
	{
		if(Trigger.isUpdate)
		{
			QuoteTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
		}
	}
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            QuoteTriggerHandler.onBeforeInsert(Trigger.new);
        }
    }

}