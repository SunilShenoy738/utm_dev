trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert) {

	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			OpportunityLineItemTriggerHandler.onBeforeInsert(Trigger.new);
		}
	}

}