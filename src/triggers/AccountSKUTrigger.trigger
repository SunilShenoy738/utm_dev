trigger AccountSKUTrigger on Account_SKU__c (before insert) {

	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			AccountSKUTriggerHandler.onBeforeInsert(Trigger.new);
		}
	}

}