trigger OrderTrigger on Order (After insert, before insert) {
	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			OrderTriggerHandler.onAfterInsert(trigger.new);
		}
	}
    
	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			OrderTriggerHandler.onBeforeInsert(trigger.new);
		}
	}

}