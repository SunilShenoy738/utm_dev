trigger CompositionTrigger on Composition__c (after insert, after update, after delete) {

	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			CompositionTriggerHandler.onAfterInsert(Trigger.new);
		}
		if(Trigger.isUpdate)
		{
			CompositionTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
		}
		if(Trigger.isDelete)
		{
			CompositionTriggerHandler.onAfterDelete(Trigger.old);
		}
	}

}