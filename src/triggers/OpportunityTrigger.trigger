trigger OpportunityTrigger on Opportunity (after insert,after update,before insert) {
	if(trigger.isAfter)
	{
		if(trigger.isInsert)
		{
			OpportunityTriggerHandler.onAfterInsert(Trigger.new);
		}
		if(trigger.isUpdate)
		{
			OpportunityTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
		}
	}
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            OpportunityTriggerHandler.onBeforeInsert(Trigger.new);
        }
    }
    

}