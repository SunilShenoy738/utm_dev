trigger ProductTrigger on Product2 (after insert,before insert, after update, before update) {

	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			ProductTriggerHandler.onAfterInsert(Trigger.new);
		}

		if(Trigger.isUpdate)
		{
			ProductTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
		}
	}

	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			ProductTriggerHandler.onBeforeInsert(Trigger.new);
		}
        
        if (Trigger.isUpdate) {
            ProductTriggerHandler.onBeforeUpdate (Trigger.new, Trigger.oldMap);
        }
	}


}