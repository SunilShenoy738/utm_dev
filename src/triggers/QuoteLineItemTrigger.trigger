trigger QuoteLineItemTrigger on QuoteLineItem (before insert) {
	if (Trigger.isBefore) {
		if (Trigger.isInsert) {
			QuoteLineItemHandler.onBeforeInsert (Trigger.new);
		}
	}
}