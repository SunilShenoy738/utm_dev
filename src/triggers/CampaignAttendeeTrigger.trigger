trigger CampaignAttendeeTrigger on Campaign_Attendee__c (before insert) {
    
    if(Trigger.IsBefore){        
        if(Trigger.IsInsert){
            
            CampaignAttendeeTriggerHandler.checkForDuplicate(Trigger.new);
        }
    }
}