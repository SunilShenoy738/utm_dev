trigger ExpenseTrigger on Expense__c (after update) {
    if (Trigger.isAfter) {
        
        if (Trigger.isUpdate) {
			ExpenseTriggerHandler.onAfterUpdate (Trigger.oldMap, Trigger.new);            
        }
    }
}