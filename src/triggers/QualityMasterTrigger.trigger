trigger QualityMasterTrigger on Quality_Master__c (before insert, before update) {

	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			QualityMasterTriggerHandler.onBeforeInsert(Trigger.new);
		}
		if(Trigger.isUpdate)
		{
			QualityMasterTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}

}