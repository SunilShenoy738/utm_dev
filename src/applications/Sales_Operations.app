<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Customize and automate the sales process, and analyze your data to make informed business decisions.</description>
    <label>Sales Operations</label>
    <tab>standard-Chatter</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Pricebook2</tab>
    <tab>standard-Product2</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>Address__c</tab>
    <tab>Wage_Master__c</tab>
</CustomApplication>
