<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Cost_Per_Kg</fullName>
        <field>Cost_Per_Kg__c</field>
        <formula>Vendor_Process__r.Cost_Per_Kg__c</formula>
        <name>Update Cost Per Kg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Cost_Per_Mtr</fullName>
        <field>Cost_per_Mtr__c</field>
        <formula>Vendor_Process__r.Cost_Per_Metre__c</formula>
        <name>Update Cost Per Mtr</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
