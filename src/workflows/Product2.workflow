<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculated_Denier</fullName>
        <field>Calculated_Denier__c</field>
        <formula>Denier__r.Dinner__c * VALUE(TEXT(Ply__c))</formula>
        <name>Calculated Denier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Calculated Denier</fullName>
        <actions>
            <name>Calculated_Denier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED( Denier__c ) , ISCHANGED( Ply__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
